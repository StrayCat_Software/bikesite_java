<%-- 
    Document   : newjsp
    Created on : 24-Nov-2014, 15:48:33
    Author     : wxu13keu
--%>

<%@page import="controller.ControllerOnWebstore"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="DatabaseTools.DBAccessor"%>

<%
    ControllerOnWebstore controller = new ControllerOnWebstore();
    String rows = controller.formatData();
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">
        <!--
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        -->
    </head>
    <body onload="onPageLoad()">
        <div id="pageContent">
             
            
            <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
            <script src="js/main.js"></script>
            <script src="js/LocationChecker.js"></script>
            <script src="js/AJAXTools.js"></script>

            <!--[if lt IE 8]>
                <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
            <![endif]-->

            <div class="menu">
                <a href="index.jsp">Home</a> |
                <a href="routes.html">Local Routes</a> |
                <a href="contact.html">Contact</a>

            </div>

            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <img id="topbarLogo" src="img/PawseysBicyclesLogo.png" alt="Pawsey's Bicycles"/>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <form class="navbar-form navbar-right" role="form">
                            <div class="form-group">
                                <input type="text" placeholder="Email" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="password" placeholder="Password" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-success">Sign in</button>
                        </form>
                    </div><!--/.navbar-collapse -->
                </div>
            </nav>





            <div id="competitionResults"></div>



            <!-- Main jumbotron for a primary marketing message or call to action -->
            <div class="jumbotron">
                <div class="container">
                    <h1>Welcome to Pawsey's Bicycles!</h1>
                    <p>We are your one stop shop for bikes in Norwich.</p>
                </div>
            </div>

            <div class="container">
                <!-- Example row of columns -->
                <div class="row">
                    <div class="col-md-4">
                        <h2>Web Store</h2>
                        <img id="webStoreImg" src="img/mens_hybrid_thumb.jpg" alt="Go to the store" height="100%" width="100%"/>
                        <p>We have many different models and specs of bike available in the shop and in our web store</p>
                        <p><a class="btn btn-default" href="#" role="button">Visit our web store &raquo;</a></p>
                    </div>
                    <div class="col-md-4">
                        <h2>Bicycle Repairs</h2>
                        <img id="repairImg" src="img/repair.jpg" alt="Book a repair" height="50%" width="100%"/>
                        <p>Our fully qualified and experienced team of mechanics can fix it! Walk in, or book your repair online.</p>
                        <p><a class="btn btn-default" href="#" role="button">Book a repair &raquo;</a></p>
                    </div>
                    <div class="col-md-4">
                        <h2>Where to find us</h2>
                        <div class="map">

                            <!-- API key= AIzaSyCZr7-xiPo-asPL1yTMZ7VoWZNIl5fcxKs -->
                            <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCZr7-xiPo-asPL1yTMZ7VoWZNIl5fcxKs&q=10+Castle+Meadow,Norwich,UK" height='200' width = '100%'>
                            </iframe>
                        </div>
                        <p>
                            Please see interactive map below for our location.
                        </p>
                    </div>
                </div>

                <div id="store">
                    <div class="jumbotron">
                        <div class="container">
                            <h2>Store</h2>
                        </div>
                        
                    </div>

                    <!--
                    <img id="bikelady" src="img/bikelady.jpg"  height="100%" width="100%"/>
                    -->
                    
                    <div class="row">
                        <%=rows%>
                    </div>
                </div>

                <hr>

                <footer>
                    <div class="footer"> 
                        <a href="mailto:info@anglian-bike-hire.co.uk">info@pawseys-bicycles.co.uk</a>
                    </div>
                </footer>
            </div> 
            <!-- /container -->        
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
            <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

            <script src="js/vendor/bootstrap.min.js"></script>

            <script src="js/main.js"></script>

            <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
            <script>
        (function (b, o, i, l, e, r) {
            b.GoogleAnalyticsObject = l;
            b[l] || (b[l] =
                    function () {
                        (b[l].q = b[l].q || []).push(arguments)
                    });
            b[l].l = +new Date;
            e = o.createElement(i);
            r = o.getElementsByTagName(i)[0];
            e.src = '//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e, r)
        }(window, document, 'script', 'ga'));
        ga('create', 'UA-XXXXX-X', 'auto');
        ga('send', 'pageview');
            </script>
        </div>
    </body>
</html>