package controller;

import DatabaseTools.DBAccessor;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Daniel Pawsey
 */
public class ControllerOnWebstore {

    /**
     * Queries the database for a single bike, and formats the result.
     *
     * @return The HTML formatted data.
     */
    public String formatData() {
        DBAccessor dba = new DBAccessor();
        String content = "";
        ResultSet rs = null;
        
        try {
            rs = dba.query("SELECT DISTINCT bike_type, model, description FROM biketype;");

            while (rs.next()) {
                content
                        += "\n<div class=\"col-md-4\">"
                        + "\n    <form action=\"BikeType.jsp\">"
                        + "\n            <h2>" + rs.getString("model") + "</h2>"
                        + "\n            <img class=\"" + rs.getString("bike_type") + "Img\" src=\"img/thumbnails/" + rs.getString("bike_type") + "_thumb.jpg\" height=\"100%\" width=\"100%\">"
//                        + "\n            <p>" + rs.getString("description") + "</p>"
                        + "\n            <button class=\"btn btn-success\" " /* + "onclick=\"readMore(\"" + rs.getString("bike_type") + "\")" + */ + ">Read more</button>"
                        + "\n            <input class=\"btn btn-success\" type=\"submit\" value=\"Purchase\"></input>"
                        + "\n    </form>"
                        + "\n</div>";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            //TODO exception handling
        }
        return content;
    }
}
