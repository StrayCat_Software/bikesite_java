<%-- 
    Document   : ManagerPage
    Created on : 09-Dec-2014, 22:14:11
    Author     : Dan
--%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="DatabaseTools.DBAccessor"%>
<%@page import="UserAccountTools.UserAccountFormGenerator"%>
<%
    /**
    * The code below is used to check if a user is signed in, and generate the 
    * appropriate buttons accordingly.
    */
    UserAccountFormGenerator uafg = new UserAccountFormGenerator();
    
    /**
     * This page automatically generates a database query, showing sales
     * information for the present day. The user can also enter a date into a
     * form, which will show sales for that day.
     */
    
    //Get the current date as a string
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    Date date = new Date();
    String searchDate = df.format(date);
    
    //Set up for the query result strings
    String booking_date = null;
    String booking_period = null;
    String customer_name = null;
    String customer_email = null;
    String booking_id = null;
    String bike_id = null;
    String bike_type = null;
    
    String onPage = "";
    
    DBAccessor dba = new DBAccessor();
    ResultSet rs = null;
    String sqlStatement = 
            "SELECT booking.booking_date, booking.booking_period, "
                +"customer.customer_name, customer.customer_email, "
                +"booking.booking_id, booked_bike.bike_id, bike.bike_type "
                +"FROM customer " 
                +"LEFT JOIN booking "
                    +"ON booking.customer_email = customer.customer_email "
                +"LEFT JOIN booked_bike "
                    +"ON booked_bike.booking_id = booking.booking_id "
                +"LEFT JOIN bike "
                    +"ON bike.bike_id = booked_bike.bike_id "
                +"WHERE booking_date = '" + searchDate + "' "
                +"ORDER BY booking.booking_date, "
                    +"booking.booking_period, customer.customer_name;";
    
    try{
        rs = dba.query(sqlStatement);
        
        while(rs.next()){
           
            booking_date = rs.getString("booking_date");
            booking_period = rs.getString("booking_period");
            customer_name = rs.getString("customer_name");
            customer_email = rs.getString("customer_email");
            booking_id = rs.getString("booking_id");
            bike_id = rs.getString("bike_id");
            bike_type = rs.getString("bike_type");
            
            
            
            onPage +=
                    "\n" + "Booking period: " + booking_period + " | Customer name: " 
                    + customer_name + " | Customer email: " + customer_email + " | Booking ID: " + booking_id + " | "
                    + bike_id + " | Bike ID: " + bike_type + "<br><br>";
        }
    } catch (SQLException ex){
        
    }

%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>

<html>

<head>
	<!-- Page Title -->
	<title>Booking Report for Today | Anglian Bike Hire</title>
	<link rel="stylesheet" media="only screen and (min-width: 601px)" href="css/desktop.css" />
	<link rel="stylesheet" media="only screen and (max-width: 600px)" href="css/mobile.css" />
</head>

<body>	
    <!-- Content DIV -->	
    <div class="content">		
        <!-- Header DIV --> 		
        <div class="header">	 		
            <div class="logo">
                <a href="index.html"><img src="resources/images/graphics/abh_logo_small.png" alt="Logo image" /></a>
            </div>
 		</div>
 		<div class="menu"> 			
                    <a href="index.jsp">Home</a> | 		
                    <a href="routes.html">Local Routes</a> | 		
                    <a href="contact.html">Contact</a>
		</div>
		
		<div class="menu_buttons">
                    <%=uafg.generateHTML(request, response, "index.jsp")%>
		</div>	
				
		<div class="pagecontent">
		
		<!-- Page specific content here-->			
                <h2>Daily rentals for <%=searchDate%></h2>
            
                <div>
                    <div>
                        <form action="ManagerDailyReportSelectDate.jsp">
                            <input type="hidden"
                                   name="customer_email">                                    
                            <input type="date"
                                        name="seachDate">    
                            <button type="submit">
                                Enter a date    
                            </button>                           
                        </form>
                    </div>
                    <div>
                        <div class="spacer"></div>
                            <div class="interactive_content">
                                <%=onPage%>
                            </div>
                        <div class="spacer"></div>
                    </div>
                    
                        
                    <!--Form to return to manager home page-->                        
                    <div>                    
                        <form action="ManagerPage.jsp">
                            <input type="hidden"
                                   name="customer_email"
                                   value="<%=request.getParameter("customer_email")%>">
                                
                            <button type="submit">    
                                Back to administrator homepage        
                            </button>    
                        </form>                        
                    </div>                     
                </div>
                <!-- Page specific content ends-->
                <div class="icons_line">    
                    <span class="icons">
                        <a href="https://twitter.com/anglianbikehire">	
                            <img src="resources/images/graphics/twitter.png" alt="Twitter image"></a>    
                    
                    </span>
                    <span class="icons">
                        <a href="https://www.facebook.com/anglianbikehireltd">
                            <img src="resources/images/graphics/facebook.png" alt="Facebook icon"></a>
                        </span>
                
                </div>
                </div>
                
                <!-- End of page content-->
		
                <!-- Footer DIV -->
                <div class="footer"> 
                    <a href="mailto:info@anglian-bike-hire.co.uk">info@anglian-bike-hire.co.uk</a>
                </div>
    </div> 
    <!-- End of content DIV -->
    </body>
</html>