<%-- 
    Document   : BookingForm
    Created on : 23-Nov-2014, 19:29:32
    Author     : Dan
--%>

<%@page import="UserAccountTools.UserAccountFormGenerator"%>
<%
UserAccountFormGenerator uafg = new UserAccountFormGenerator();
    
String model = request.getParameter("model");
String customer_email = request.getParameter("customer_email");
String emailLabel = "";
String emailInputType = "hidden";

//If the customer_email parameter retrieved from the HttpServletRequest object is
//null (i.e. the user hasn't signed in), they can add there email address here.
//Their email address being null causes the hidden field which would have taken
//their email as passed between the JSP forms and converts it into a text field,
//requesting their email
if (customer_email.equals("null") || customer_email.equals(null) || customer_email.equals("") || customer_email.isEmpty()){
    customer_email = "";
    emailLabel = "Email Address: ";
    emailInputType = "email";
}

//Send a parameter to the HttpServletRequest telling it that a booking has just
//been made. This is so that if the checkout page is called, it only attempts to
//add something into the database if it is called from this page.
String CallingPage = "BookingForm.jsp";
request.setAttribute("CallingPage", CallingPage);


%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
	<!-- Page Title -->
	<title>Book <%=model%> | Anglian Bike Hire</title>
	<link rel="stylesheet" media="only screen and (min-width: 601px)" href="css/desktop.css" />
	<link rel="stylesheet" media="only screen and (max-width: 600px)" href="css/mobile.css" />
    </head>
    <body>
	<!-- Content DIV -->
	<div class="content">
		<!-- Header DIV -->
 		<div class="header">
	 		<div class="logo">
                            <a href="index.html"><img src="resources/images/graphics/abh_logo_small.png" alt="Logo image" /></a>
 			</div>
 		</div>

 		<div class="menu">
 			<a href="index.jsp">Home</a> |
 			<a href="routes.html">Local Routes</a> |
 			<a href="contact.html">Contact</a>
		</div>
		
		<div class="menu_buttons">
                    <%=uafg.generateHTML(request, response, "index.jsp")%>
                </div>
                
                <div class="spacer"></div>
				
		<div class="pagecontent">
		
                    <div class="interactive_content">
        		<form action="ProcessBooking.jsp" method="GET">
            
                        <!--The hidden field adds the users email to the booking. If the user
                        is not signed in, then this becomes an email text entry field.-->
                        <%=emailLabel%>
                        <input type="<%=emailInputType%>" 
                               name="customer_email"
                               value="<%=customer_email%>"><br>
            
                        <!--The bike model is passed as a hidden variable, retrieved from the previous page-->
                        <input type="hidden" name="model" value="<%=model%>">
                        
                        Date:    
                        <input type="date"    
                               name ="booking_date"><br>    
      
                            Period:
                            <input type="radio" name="booking_period" value="ALL">All day
                            <input type="radio" name="booking_period" value="AM" checked>Morning
                            <input type="radio" name="booking_period" value="PM" checked>Afternoon<br>
            
                            <!--
                            How many?:
                            <input type="number"
                                   name="amount"><br>
                            -->
                            How many?:
                            <input type="number" 
                                   name="amount"
                                   min="0" 
                                   max="30" 
                                   step="1" 
                                   value="1"><br>
            
                            Note:    
                            <input type="text"    
                                   name="note"><br>
                
                            <button type ="submit">
                                Book
                            </button>
                
                            <input type="reset">
                        </form>
                    </div>
		
		<!-- Page specific content ends-->
                
                <div class="icons_line">
                    <span class="icons">
                        <a href="https://twitter.com/anglianbikehire">	
                            <img src="resources/images/graphics/twitter.png" alt="Twitter image"></a>
                    </span>
                    <span class="icons">
                        <a href="https://www.facebook.com/anglianbikehireltd">
                            <img src="resources/images/graphics/facebook.png" alt="Facebook icon"></a>
                    </span>
                </div>
                </div>
                
                <!-- End of page content-->
		
 		<!-- Footer DIV -->
 		<div class="footer"> 
 			<a href="mailto:info@anglian-bike-hire.co.uk">info@anglian-bike-hire.co.uk</a>
 		</div>
		
	</div> 
    <!-- End of content DIV -->
    </body>
</html>