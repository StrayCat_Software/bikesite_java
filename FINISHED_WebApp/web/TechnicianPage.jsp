<%-- 
    Document   : TechnicianPage
    Created on : 11-Dec-2014, 09:16:32
    Author     : wxu13keu
--%>
<%@page import="UserAccountTools.UserAccountFormGenerator"%>
<%
    UserAccountFormGenerator uafg = new UserAccountFormGenerator();
%>
    
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

<head>
	<!-- Page Title -->
	<title>Technician Home | Anglian Bike Hire</title>
	<link rel="stylesheet" media="only screen and (min-width: 601px)" href="css/desktop.css" />
	<link rel="stylesheet" media="only screen and (max-width: 600px)" href="css/mobile.css" />
</head>

<body>
	<!-- Content DIV -->
	<div class="content">
		<!-- Header DIV -->
 		<div class="header">
	 		<div class="logo">
                            <a href="index.html"><img src="resources/images/graphics/abh_logo_small.png" alt="Logo image" /></a>
 			</div>
 		</div>

 		<div class="menu">
 			<a href="index.html">Home</a> |
 			<a href="routes.html">Local Routes</a> |
 			<a href="contact.html">Contact</a>
		</div>
		
		<div class="menu_buttons">
                    <%=uafg.generateHTML(request, response, "index.jsp")%>
                    <a href="index.jsp">Home</a>
                    <a href="routes.html">Local Routes</a>
                    <a href="contact.html">Contact</a>
		</div>	
				
		<div class="pagecontent">
		
		<!-- Page specific content here-->			
                <h2>Welcome, technician</h2>
                <div>
                    <div class="interactive_content">
                        <!--by default, this will show sales for today, but the page includes a
                        form allowing the user to select which date to view-->
                        <form action="TechnicianReportDamage.jsp">
                            <input type="hidden"
                                   name="customer_email"
                                   value="<%=request.getParameter("customer_email")%>">
                            <button type="submit">
                                Report damage to a bike   
                            </button>
                        </form>

                        <form action="TechnicianSelectRepairs.jsp">
                            <input type="hidden"
                                   name="customer_email"
                                   value="<%=request.getParameter("customer_email")%>">
                            <button type="submit">
                                Review repairs   
                            </button>
                        </form>
                    </div>
                </div>
                        <div class="spacer"></div>
                 <!--Form to return to manager home page-->
                <div>
                    <form action="index.jsp">
                        <input type="hidden"
                               name="customer_email"
                               value="<%=request.getParameter("customer_email")%>">
                        <button type="submit">
                            Return to the site homepage    
                        </button>
                    </form>
                </div>
		
		<!-- Page specific content ends-->
                
                <div class="icons_line">
                    <span class="icons">
                        <a href="https://twitter.com/anglianbikehire">	
                            <img src="resources/images/graphics/twitter.png" alt="Twitter image"></a>
                    </span>
                    <span class="icons">
                        <a href="https://www.facebook.com/anglianbikehireltd">
                            <img src="resources/images/graphics/facebook.png" alt="Facebook icon"></a>
                    </span>
                </div>
                </div>
                
                <!-- End of page content-->
		
 		<!-- Footer DIV -->
 		<div class="footer"> 
 			<a href="mailto:info@anglian-bike-hire.co.uk">info@anglian-bike-hire.co.uk</a>
 		</div>
		
	</div> 
    <!-- End of content DIV -->
    </body>
</html>