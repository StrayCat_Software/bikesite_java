<%-- 
    Document   : ManagerMonthlyReport
    Created on : 11-Dec-2014, 09:53:09
    Author     : wxu13keu
--%>
<%@page import="UserAccountTools.UserAccountFormGenerator"%>
<%
    UserAccountFormGenerator uafg = new UserAccountFormGenerator();    
%>
<%@page import="java.sql.ResultSet"%>
<%@page import="DatabaseTools.DBAccessor"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

<head>
	<!-- Page Title -->
	<title>Sales Report | Anglian Bike Hire</title>
	<link rel="stylesheet" media="only screen and (min-width: 601px)" href="css/desktop.css" />
	<link rel="stylesheet" media="only screen and (max-width: 600px)" href="css/mobile.css" />
</head>

<body>
	<!-- Content DIV -->
	<div class="content">
		<!-- Header DIV -->
 		<div class="header">
	 		<div class="logo">
                            <a href="index.html"><img src="resources/images/graphics/abh_logo_small.png" alt="Logo image" /></a>
 			</div>
 		</div>

 		<div class="menu">
 			<a href="index.jsp">Home</a> |
 			<a href="routes.html">Local Routes</a> |
 			<a href="contact.html">Contact</a>
		</div>
		
		<div class="menu_buttons">
                    <%=uafg.generateHTML(request, response, "index.jsp")%>
		</div>	
				
		<div class="pagecontent">
		
                    <!-- Page specific content here-->			
                    <h2>Select dates for sales report</h2>
                    <div class="interactive_content">
                        <form action="ViewManagerMonthlyReport.jsp">
                            <input type="hidden"
                                   name="customer_email"
                                   value="<%=request.getParameter("customer_email")%>">
                            From:
                            <input type="date"
                                   name="minimumDate"><br>
                            Until:
                            <input type="date"
                                   name="maximumDate">
                            <button type="submit">
                                Search
                            </button>
                        </form>
                    </div>

                            <div class="spacer"></div>
                    <!--Form to return to manager home page-->
                    <div>
                        <form action="ManagerPage.jsp">
                            <input type="hidden"
                                   name="customer_email"
                                   value="<%=request.getParameter("customer_email")%>">
                            <button type="submit">
                                Back to administrator homepage    
                            </button>
                        </form>
                    </div>

                    <!-- Page specific content ends-->

                    <div class="icons_line">
                        <span class="icons">
                            <a href="https://twitter.com/anglianbikehire">	
                                <img src="resources/images/graphics/twitter.png" alt="Twitter image"></a>
                        </span>
                        <span class="icons">
                            <a href="https://www.facebook.com/anglianbikehireltd">
                                <img src="resources/images/graphics/facebook.png" alt="Facebook icon"></a>
                        </span>

                    </div>
               
                </div>
                
                <!-- End of page content-->
		
 		<!-- Footer DIV -->
 		<div class="footer"> 
 			<a href="mailto:info@anglian-bike-hire.co.uk">info@anglian-bike-hire.co.uk</a>
 		</div>
		
	</div> 
    <!-- End of content DIV -->
    </body>
</html>