<%-- 
    Document   : ManagerMonthlyReport
    Created on : 11-Dec-2014, 09:53:09
    Author     : wxu13keu
--%>
<%@page import="UserAccountTools.UserAccountFormGenerator"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="DatabaseTools.DBAccessor"%>
<%
    //sign in and out buttons
    UserAccountFormGenerator uafg = new UserAccountFormGenerator();
    
    DBAccessor dba = new DBAccessor();
    ResultSet rs = null;
    
    //Set up the variables for recieving the database output.
    String booking_period = null;
    int amount = 0;
    String bike_type = null;
    float day = 0;
    float half = 0;
    float rate = 0;
    
    //Set up the page output variable
    float monthlyIncome = 0;
    
    //Get the page input parameters
    String minimumDate = request.getParameter("minimumDate");
    String maximumDate = request.getParameter("maximumDate");
    
    String sqlStatement =
            "SELECT booking_period, amount, bike.bike_type, day, half "
            +"FROM booking "
            +"LEFT JOIN booked_bike "
                    +"ON booking.booking_id = booked_bike.booking_id "
            +"LEFT JOIN bike "
                    +"ON booked_bike.bike_id = bike.bike_id "
            +"LEFT JOIN rates "
                    +"ON bike.bike_type = rates.bike_type "
            +"WHERE booking_date > '" + minimumDate + "'"
                    +"AND booking_date < '" + maximumDate + "';";
    
    rs = dba.query(sqlStatement);


    while(rs.next()){

        booking_period = rs.getString("booking_period");
        amount = rs.getInt("amount");
        bike_type = rs.getString("bike_type");
        day = rs.getFloat("day");
        half = rs.getFloat("half");

        //If the booking_period is "ALL", then the day rate is used
        //for that booking, otherwise, the half rate is used for
        //that booking
        if(booking_period.equals("ALL")){
            rate = day;
        }
        else{
            rate = half;
        }

     monthlyIncome = monthlyIncome + rate;
    }

    
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
	<!-- Page Title -->
	<title>Monthly sales for <%=minimumDate%> to <%=maximumDate%> | Anglian Bike Hire</title>
	<link rel="stylesheet" media="only screen and (min-width: 601px)" href="css/desktop.css" />
	<link rel="stylesheet" media="only screen and (max-width: 600px)" href="css/mobile.css" />
</head>

<body>
	<!-- Content DIV -->
	<div class="content">
		<!-- Header DIV -->
 		<div class="header">
	 		<div class="logo">
                            <a href="index.html"><img src="resources/images/graphics/abh_logo_small.png" alt="Logo image" /></a>
 			</div>
 		</div>

 		<div class="menu">
 			<a href="index.jsp">Home</a> |
 			<a href="routes.html">Local Routes</a> |
 			<a href="contact.html">Contact</a>
		</div>
		
		<div class="menu_buttons">
                    <%=uafg.generateHTML(request, response, "index.jsp")%>
		</div>	
				
		<div class="pagecontent">
		
		<!-- Page specific content here-->			
                <h2>Monthly sales for <%=minimumDate%> to <%=maximumDate%></h2>
                <div class="interactive_content">
                    £<%=monthlyIncome%> was made between <%=minimumDate%> and <%=maximumDate%>.
                </div>

                <div class="spacer"></div>
                <!--Form to return to manager home page-->
                <div>
                    <form action="ManagerPage.jsp">
                        <input type="hidden"
                               name="customer_email"
                               value="<%=request.getParameter("customer_email")%>">
                        <button type="submit">
                            Back to administrator homepage    
                        </button>
                    </form>
                </div>
		
		<!-- Page specific content ends-->
                
                <div class="icons_line">
                    <span class="icons">
                        <a href="https://twitter.com/anglianbikehire">	
                            <img src="resources/images/graphics/twitter.png" alt="Twitter image"></a>
                    </span>
                    <span class="icons">
                        <a href="https://www.facebook.com/anglianbikehireltd">
                            <img src="resources/images/graphics/facebook.png" alt="Facebook icon"></a>
                    </span>
                </div>
                </div>
                
                <!-- End of page content-->
		
 		<!-- Footer DIV -->
 		<div class="footer"> 
 			<a href="mailto:info@anglian-bike-hire.co.uk">info@anglian-bike-hire.co.uk</a>
 		</div>
		
	</div> 
    <!-- End of content DIV -->
    </body>
</html>