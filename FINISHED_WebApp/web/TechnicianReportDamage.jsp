<%-- 
    Document   : TechnicianReportDamage
    Created on : 11-Dec-2014, 11:04:53
    Author     : wxu13keu
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="DatabaseTools.DBAccessor"%>
<%@page import="UserAccountTools.UserAccountFormGenerator"%>
<%
    //Generate the sign in/out buttons
    UserAccountFormGenerator uafg = new UserAccountFormGenerator();
    
    DBAccessor dba = new DBAccessor();
        ResultSet rs = null;
        String model = null;
    
        String output = "";
        
        //This creates a dynamically generated drop down list of bikes, ordered
        //by model
        
        //First query the bike table to get all distinct bike models
        try{
            String sqlStatement =
                    "SELECT DISTINCT model "
                    +"FROM bike";
        
            ArrayList ModelList = new ArrayList();
                
            rs = dba.query(sqlStatement);
            
            //Add all of the bike models to an ArrayList
            while(rs.next()){
                ModelList.add(rs.getString("model"));
            }

            //Add all of the models from the ArrayList to the drop down list
            for(int i = 0; i != ModelList.size(); i++){
                model = ModelList.get(i).toString();
                output += "\n<optgroup label=\"" + model + "\">";
                
                //Search the bike table for bike_id by model
                sqlStatement = 
                        "SELECT bike_id "
                        +"FROM bike "
                        +"WHERE model = '" + model + "';";
                
                rs = dba.query(sqlStatement);
                
                //Add each bike_id to the drop down list under their respective
                //models.
                while(rs.next()){
                    String bike_id = rs.getString("bike_id");
                    output +=
                            "\n    <option value=\"" + bike_id + "\">" + bike_id + "</option>";
                }
            }

        } catch (SQLException ex){
            output = "SQL Exception occured.";
        }
%>
    
<%@page contentType="text/html" pageEncoding="UTF-8"%>








<!DOCTYPE html>

<html>

<head>
	<!-- Page Title -->
	<title>Report Damage | Anglian Bike Hire</title>
	<link rel="stylesheet" media="only screen and (min-width: 601px)" href="css/desktop.css" />
	<link rel="stylesheet" media="only screen and (max-width: 600px)" href="css/mobile.css" />
</head>

<body>
	<!-- Content DIV -->
	<div class="content">
		<!-- Header DIV -->
 		<div class="header">
	 		<div class="logo">
                            <a href="index.html"><img src="resources/images/graphics/abh_logo_small.png" alt="Logo image" /></a>
 			</div>
 		</div>

 		<div class="menu">
 			<a href="index.jsp">Home</a> |
 			<a href="routes.html">Local Routes</a> |
 			<a href="contact.html">Contact</a>
		</div>
		
		<div class="menu_buttons">
                    <%=uafg.generateHTML(request, response, "index.jsp")%>
		</div>	
				
		<div class="pagecontent">
		
                    <!-- Page specific content here-->		
                    <h2>Report Damage to a Bike</h2>
                    
                    <div class="interactive_content">
                        <div>
                            Select a bike of model to report:

                            <form id="okButton" action="technicianProcessDamage.jsp">
                                <input type="hidden"
                                       name="customer_email"
                                       value="<%=request.getParameter("customer_email")%>">

                                <select form="okButton" name="bike_id">
                                    <%=output%>
                                </select>

                                Describe the problem:
                                <input type="textarea"
                                       name="damage">

                                <button type="submit">
                                    Ok
                                </button>

                            </form>
                        </div>
                    </div>
                        
                                <div class="spacer"></div>            
                                
                    <!--Form to return to manager home page-->    
                    <div>
                        <form action="TechnicianPage.jsp">
                            <input type="hidden"
                                   name="customer_email"
                                   value="<%=request.getParameter("customer_email")%>">
                            <button type="submit">
                                Back to technician homepage    
                            </button>
                        </form>
                    </div>
                    
                        <!-- Page specific content ends-->

                        <div class="icons_line">
                            <span class="icons">
                                <a href="https://twitter.com/anglianbikehire">	
                                    <img src="resources/images/graphics/twitter.png" alt="Twitter image"></a>
                            </span>
                            <span class="icons">
                                <a href="https://www.facebook.com/anglianbikehireltd">
                                    <img src="resources/images/graphics/facebook.png" alt="Facebook icon"></a>
                            </span>
                        </div>
                </div>
                
                <!-- End of page content-->
		
 		<!-- Footer DIV -->
 		<div class="footer"> 
 			<a href="mailto:info@anglian-bike-hire.co.uk">info@anglian-bike-hire.co.uk</a>
 		</div>
		
	</div> 
    <!-- End of content DIV -->
    </body>
</html>