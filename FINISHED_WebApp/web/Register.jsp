<%-- 
    Document   : Register
    Created on : 30-Nov-2014, 16:18:16
    Author     : Dan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>




<!DOCTYPE html>
<html>
    <head>
	<!-- Page Title -->
	<title>Register | Anglian Bike Hire</title>
	<link rel="stylesheet" media="only screen and (min-width: 601px)" href="css/desktop.css" />
	<link rel="stylesheet" media="only screen and (max-width: 600px)" href="css/mobile.css" />
    </head>
    <body>
	<!-- Content DIV -->
	<div class="content">
		<!-- Header DIV -->
 		<div class="header">
	 		<div class="logo">
                            <a href="index.html"><img src="resources/images/graphics/abh_logo_small.png" alt="Logo image" /></a>
 			</div>
 		</div>

 		<div class="menu">
 			<a href="index.jsp">Home</a> |
 			<a href="routes.html">Local Routes</a> |
 			<a href="contact.html">Contact</a>
		</div>
                
                <div class="spacer"></div>
				
		<div class="pagecontent">
		
                    <!--Page specific content-->
                    <h1>Register</h1>
        
                    <div class="interactive_content">
                        <form class="loginPanel" action="ProcessAddCustomer.jsp">

                            Email address: 
                            <input type="email"
                                   name="customer_email"><br>

                            Password:
                            <input type="password"
                                   name="password"><br>

                            Confirm password:
                            <input type="password"
                                   name="confirm_password">

                            <br>

                            First name:
                            <input type="text"
                                   name="first_name">

                            Last name:
                            <input type="text"
                                   name="last_name"><br>

                            <div>Please enter payment details below:</div><br>

                            Billing address:
                            <input type="text"
                                   name="billing_address"><br>

                            Card number:
                            <input type="numeric"
                                   name="cardno"><br>

                            Expiry date:
                            <input type="numeric"
                                   name="card_exp"><br>

                            Card type:
                            <input type="radio" name="card_type" value="V" checked>Visa
                            <input type="radio" name="card_type" value="MC">MasterCard
                            <input type="radio" name="card_type" value="AE">American Express<br>

                            <input type="submit"
                                   value="Register">

                        </form>
                    </div>
		
		<!-- Page specific content ends-->
                
                <div class="icons_line">
                    <span class="icons">
                        <a href="https://twitter.com/anglianbikehire">	
                            <img src="resources/images/graphics/twitter.png" alt="Twitter image"></a>
                    </span>
                    <span class="icons">
                        <a href="https://www.facebook.com/anglianbikehireltd">
                            <img src="resources/images/graphics/facebook.png" alt="Facebook icon"></a>
                    </span>
                </div>
                </div>
                
                <!-- End of page content-->
		
 		<!-- Footer DIV -->
 		<div class="footer"> 
 			<a href="mailto:info@anglian-bike-hire.co.uk">info@anglian-bike-hire.co.uk</a>
 		</div>
		
	</div> 
    <!-- End of content DIV -->
    </body>
</html>