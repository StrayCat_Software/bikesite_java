<%-- 
    Document   : ManagerPage
    Created on : 09-Dec-2014, 22:14:11
    Author     : Dan
--%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="DatabaseTools.DBAccessor"%>
<%@page import="UserAccountTools.UserAccountFormGenerator"%>
<%
    /**
    * The code below is used to check if a user is signed in, and generate the 
    * appropriate buttons accordingly.
    */
    UserAccountFormGenerator uafg = new UserAccountFormGenerator();

    String output = "";
    
    //Get date parameters
    String minimumDate = request.getParameter("minimumDate");
    String maximumDate = request.getParameter("maximumDate");
    
    //database query variable strings
    String bike_id = null;
    String service_start = null;
    String service_end = null;
    String note = null;
    
    DBAccessor dba = new DBAccessor();
    ResultSet rs = null;
    String sqlStatement = 
            "SELECT bike_id, service_start, service_end, note "
            +"FROM bikeservice "
            +"WHERE service_start > '" + minimumDate + "' "
                    +"AND service_end < '" + maximumDate + "';";
    
    try{
        rs = dba.query(sqlStatement);
        
        while(rs.next()){
            bike_id = rs.getString("bike_id");
            service_start = rs.getString("service_start");
            service_end = rs.getString("service_end");
            note = rs.getString("note");
            
            output += "Bike ID: " + bike_id + " | Service start date: " + service_start 
                    + " | Service end date: " + service_end 
                    + " | Note: " + note + "<br><br>";
        }
    } catch (SQLException ex){
        
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

<head>
	<!-- Page Title -->
	<title>View Repairs | Anglian Bike Hire</title>
	<link rel="stylesheet" media="only screen and (min-width: 601px)" href="css/desktop.css" />
	<link rel="stylesheet" media="only screen and (max-width: 600px)" href="css/mobile.css" />
</head>

<body>
	<!-- Content DIV -->
	<div class="content">
		<!-- Header DIV -->
 		<div class="header">
	 		<div class="logo">
                            <a href="index.html"><img src="resources/images/graphics/abh_logo_small.png" alt="Logo image" /></a>
 			</div>
 		</div>

 		<div class="menu">
 			<a href="index.jsp">Home</a> |
 			<a href="routes.html">Local Routes</a> |
 			<a href="contact.html">Contact</a>
		</div>
		
		<div class="menu_buttons">
                    <%=uafg.generateHTML(request, response, "index.jsp")%>
		</div>	
				
		<div class="pagecontent">
		
		<!-- Page specific content here-->
                <!--Form to return to technician home page-->
                <h2>Select dates for repairs report</h2>
                <div class="interactive_content">
                    <%--<%=sqlStatement%>--%>
                    <form action="TechnicianViewRepairs.jsp">
                        <input type="hidden"
                               name="customer_email"
                               value="<%=request.getParameter("customer_email")%>">
                        From:
                        <input type="date"
                               name="minimumDate"><br>
                        Until:
                        <input type="date"
                               name="maximumDate">
                        <button type="submit">
                            Search
                        </button>
                    </form>
                </div>
                        <div class="spacer"></div>
                <div class="interactive_content">
                    
                    <!--A list of repairs between two dates-->        
                    <div>
                        <%=output%>
                    </div>
                </div>
                    
                <div class="spacer"></div>
                <!--Form to return to technician home page-->
                
                <div>
                    <form action="TechnicianPage.jsp">
                        <input type="hidden"
                               name="customer_email"
                               value="<%=request.getParameter("customer_email")%>">
                        <button type="submit">
                            Back to technician homepage    
                        </button>
                    </form>
                </div>
		
		<!-- Page specific content ends-->
                
                <div class="icons_line">
                    <span class="icons">
                        <a href="https://twitter.com/anglianbikehire">	
                            <img src="resources/images/graphics/twitter.png" alt="Twitter image"></a>
                    </span>
                    <span class="icons">
                        <a href="https://www.facebook.com/anglianbikehireltd">
                            <img src="resources/images/graphics/facebook.png" alt="Facebook icon"></a>
                    </span>
                </div>
                </div>
                
                <!-- End of page content-->
		
 		<!-- Footer DIV -->
 		<div class="footer"> 
 			<a href="mailto:info@anglian-bike-hire.co.uk">info@anglian-bike-hire.co.uk</a>
 		</div>
		
	</div> 
    <!-- End of content DIV -->
    </body>
</html>