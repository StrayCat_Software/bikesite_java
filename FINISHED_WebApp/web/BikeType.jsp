<%-- 
    Document   : newjsp
    Created on : 24-Nov-2014, 15:48:33
    Author     : wxu13keu
--%>

<%@page import="UserAccountTools.UserAccountFormGenerator"%>
<%@page import="javax.servlet.http.HttpServletRequest"%>
<%@page import="DatabaseTools.DBAccessor"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>

<%
    /**
    * The code below is used to check if a user is signed in, and generate the 
    * appropriate buttons accordingly.
    */
    UserAccountFormGenerator uafg = new UserAccountFormGenerator();
    
    ResultSet rs = null;
    String model = "";
    String imagePath = "";
    String description = "";
    String page_bike_type = request.getParameter("bike_type");
    
    //This is the page that will be called by the bike buttons on this page. If
    //the user is signed in, that will be the booking form, otherwise, if there
    //is a null value for email in the HttpServletRequest object (i.e. the user
    //has not signed in), it will be the sign in form.
    String callPage = "BookingForm.jsp";
    
    String customer_email = null;

        customer_email = request.getParameter("customer_email");

        String query = "SELECT * FROM biketype WHERE bike_type = '" + page_bike_type + "';";
        String entries = "";

        try {
            DBAccessor dba = new DBAccessor();
            rs = dba.query(query);
            
            //The following will retrieve the model and description of each bike
            //from the result set, and add them to a <div> tag HTML page element
            //with the class label "bike_type", allowing these elements to be
            //styled as individual on-page elements, but in a uniform style.
            //Furthermore, the name will be presented as a hyperlink. This will
            //All be generated as a string, called as a JavaBean on the page.
            int i = 0;
            while(rs.next()){
                
                String bike_type = rs.getString("bike_type");
                model = rs.getString("model");
                imagePath = rs.getString("imagePath");
                description = rs.getString("description");
                
                //For each bike, a form and submit button are included, which 
                //serve both as a link to BookingForm.jsp, and a means of passing
                //the bike model to that form.
                entries += 
                        "\n<!--This is a single dynamically generated bike model of the previously selected bike type-->"
                        +"\n<div class=\"bike_info_fullsize\">"
                        +"\n    <h2>" + rs.getString("model") + "</h2>"
                        +"\n    <form action=\"" + callPage + "\">"
                        +"\n        <input type=\"hidden\" name=\"pageName\" value=\"HomePage.jsp\">"
                        +"\n        <input type=\"hidden\" name=\"model\" value=\"" + model + "\">"
                        +"\n       <button type=\"submit\" name=\"" + rs.getString("bike_type") + "\" value=\"" + rs.getString("bike_type") + "\">"
                        +"\n            <img src=\"" + rs.getString("imagePath") + "\">"
                        +"\n            <br>Click to book a " + rs.getString("model")
                        +"\n       </button>"
                        +"\n       <input type=\"hidden\" name=\"customer_email\" value=\"" + customer_email + "\">"
                        +"\n    </form>" 
                        +"\n    <div class=\"interactive_content\">" 
                        +"\n        " + rs.getString("description") 
                        +"\n   </div>"
                        +"\n</div>"
                        ;
                
                rs.next();
                i++;               
            }
        } catch (SQLException ex) {

        } catch (ClassNotFoundException ex) {

        }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>
	<!-- Page Title -->
	<title>Bike Details | Anglian Bike Hire</title>
	<link rel="stylesheet" media="only screen and (min-width: 601px)" href="css/desktop.css" />
	<link rel="stylesheet" media="only screen and (max-width: 600px)" href="css/mobile.css" />
</head>

<body>
	<!-- Content DIV -->
	<div class="content">
		<!-- Header DIV -->
 		<div class="header">
	 		<div class="logo">
                            <a href="index.html"><img src="resources/images/graphics/abh_logo_small.png" alt="Logo image" /></a>
 			</div>
 		</div>

 		<div class="menu">
 			<a href="index.jsp">Home</a> |
 			<a href="routes.html">Local Routes</a> |
 			<a href="contact.html">Contact</a>
		</div>
		
		<div class="menu_buttons">
                    <%=uafg.generateHTML(request, response, "index.jsp")%>
		</div>	
				
		<div class="pagecontent">
		
		<!-- Page specific content here-->			
                <div class="bike_info">
                    <%=entries%>
		</div>
		
		<!-- Page specific content ends-->
                
                <div class="icons_line">
                    <span class="icons">
                        <a href="https://twitter.com/anglianbikehire">	
                            <img src="resources/images/graphics/twitter.png" alt="Twitter image"></a>
                    </span>
                    <span class="icons">
                        <a href="https://www.facebook.com/anglianbikehireltd">
                            <img src="resources/images/graphics/facebook.png" alt="Facebook icon"></a>
                    </span>
                </div>
                </div>
                
                <!-- End of page content-->
		
 		<!-- Footer DIV -->
 		<div class="footer"> 
 			<a href="mailto:info@anglian-bike-hire.co.uk">info@anglian-bike-hire.co.uk</a>
 		</div>
		
	</div> 
    <!-- End of content DIV -->
    </body>
</html>