<%-- 
    Document   : technicianProcessDamage
    Created on : 11-Dec-2014, 13:09:18
    Author     : wxu13keu
--%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="UserAccountTools.UserAccountFormGenerator"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="DatabaseTools.DBAccessor"%>
<%
    //Generate the sign in buttons
    UserAccountFormGenerator uafg = new UserAccountFormGenerator();
    
    DBAccessor dba = new DBAccessor();
    ResultSet rs = null;
    String output = "";
    
    //Get todays date so as to list the service_start in the bikeservice table.
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    Date date = new Date();
    String serviceStartDate = df.format(date);
    
    //Update the database
    String sqlStatement =
            "UPDATE bike "
            +"SET damage = '" + request.getParameter("damage") + "' "
            +"WHERE bike_id = '" + request.getParameter("bike_id") + "';";
                    
    dba.update(sqlStatement);
    
    sqlStatement =
            "INSERT INTO bikeservice"
            +"(bike_id, service_start, note)"
            +"VALUES('" 
                + request.getParameter("bike_id") + "', '" 
                + serviceStartDate + "', '" 
                + request.getParameter("damage") + "');";
        
    dba.update(sqlStatement);
        
    //Query the database 
    sqlStatement =
            "SELECT bike.bike_id, model, damage "
            +"FROM bike "
            +"LEFT JOIN bikeservice "
                +"ON bike.bike_id = bikeservice.bike_id "
            +"WHERE bike.bike_id = '" + request.getParameter("bike_id") + "' "
                +"AND service_start = '" + serviceStartDate + "';";
    
    rs = dba.query(sqlStatement);
    
    //Get the information about the bike to display on the page
    while(rs.next()){
        String bike_id = rs.getString("bike_id");
        String model = rs.getString("model");
        String damage = rs.getString("damage");
        
        output +=
                "Bike ID: " + bike_id + "<br>"
                +"\nModel: " + model + "<br>"
                +"\nDamage reported / Cleaning needed: " + damage;
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

<head>
	<!-- Page Title -->
	<title>Damage Report Added | Anglian Bike Hire</title>
	<link rel="stylesheet" media="only screen and (min-width: 601px)" href="css/desktop.css" />
	<link rel="stylesheet" media="only screen and (max-width: 600px)" href="css/mobile.css" />
</head>

<body>
	<!-- Content DIV -->
	<div class="content">
		<!-- Header DIV -->
 		<div class="header">
	 		<div class="logo">
                            <a href="index.html"><img src="resources/images/graphics/abh_logo_small.png" alt="Logo image" /></a>
 			</div>
 		</div>

 		<div class="menu">
 			<a href="index.jsp">Home</a> |
 			<a href="routes.html">Local Routes</a> |
 			<a href="contact.html">Contact</a>
		</div>
		
		<div class="menu_buttons">
                    <%=uafg.generateHTML(request, response, "index.jsp")%>
		</div>	
				
		<div class="pagecontent">
		
                    <!-- Page specific content here-->			
                    <h2>Damage logged</h2>

                    <!--Display an aknowledgement of the damage report-->
                    
                    <div class="interactive_content">
                        <%=output%>
                    </div>

                    <div class="spacer"></div>
                    
                    <!--Form to return to technician home page-->
                    <div>
                        <form action="TechnicianPage.jsp">
                            <input type="hidden"
                                    name="customer_email"
                                    value="<%=request.getParameter("customer_email")%>">
                            <button type="submit">
                                Back to technician homepage    
                            </button>
                        </form>
                    </div>
                </div>
		
		<!-- Page specific content ends-->
                
                <div class="icons_line">
                    <span class="icons">
                        <a href="https://twitter.com/anglianbikehire">	
                            <img src="resources/images/graphics/twitter.png" alt="Twitter image"></a>
                    </span>
                    <span class="icons">
                        <a href="https://www.facebook.com/anglianbikehireltd">
                            <img src="resources/images/graphics/facebook.png" alt="Facebook icon"></a>
                    </span>
                </div>
                </div>
                
                <!-- End of page content-->
		
 		<!-- Footer DIV -->
 		<div class="footer"> 
 			<a href="mailto:info@anglian-bike-hire.co.uk">info@anglian-bike-hire.co.uk</a>
 		</div>
		
	</div> 
    <!-- End of content DIV -->
    </body>
</html>