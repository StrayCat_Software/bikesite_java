<%-- 
    Document   : ProcessPayment
    Created on : 10-Dec-2014, 12:51:21
    Author     : wxu13keu
--%>
<%@page import="UserAccountTools.UserAccountFormGenerator"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="DatabaseTools.DBAccessor"%>
<%
    UserAccountFormGenerator uafg = new UserAccountFormGenerator();
    
    //This JSP will add any card details added by the user to the database
    DBAccessor dba = new DBAccessor();
    ResultSet rs = null;
    
    String customer_email = request.getParameter("customer_email");
    String billing_address = request.getParameter("billing_address");
    String cardno = request.getParameter("cardno");
    String card_exp = request.getParameter("card_exp");
    String card_type = request.getParameter("card_type");
    
    //check inputs into database by attempting to retrieve them:
    String check_customer_email = null;
    String check_billing_address = null;
    String check_cardno = null;
    String check_card_exp = null;
    String check_card_type = null;
    
    String queryStatement1 = null;
    String queryStatement2 = null;
    String updateStatement = null;
    
    int i = 0;
    
    
    try{
    
        //First check if the customer already exists, so as to decide whetehr to
        //make a new customer, or update an existing one
        queryStatement1 = 
                "SELECT customer_email "
                +"FROM customer "
                +"WHERE customer_email = '" + customer_email + "';";
    
        rs = dba.query(queryStatement1);
        
        i = 0;
        while(rs.next()){
            i++;
        }
    
        //Insert the customer
        if(i == 0){
            updateStatement = 
                "INSERT INTO customer "
                +"(customer_email, billing_address, cardno, card_exp, card_type, staff)"
                +"VALUES('" + customer_email + "', '" + billing_address + "', '"
                + cardno + "', '" + card_exp + "', '" + card_type + "', 'CUSTOMER');";
        
            dba.update(updateStatement);
        
        //Update the customer    
        }else{
            updateStatement = 
                    "UPDATE customer "
                    +"SET billing_address = '" + billing_address + "' "
                    +"WHERE customer_email = '" + customer_email + "'; "
                    
                    +"UPDATE customer "
                    +"SET cardno = '" + cardno + "' "
                    +"WHERE customer_email = '" + customer_email + "'; "
        
                    +"UPDATE customer "
                    +"SET card_exp = '" + card_exp + "' "
                    +"WHERE customer_email = '" + customer_email + "'; "
                
                    +"UPDATE customer "
                    +"SET card_type = '" + card_type + "' "
                    +"WHERE customer_email = '" + customer_email + "'; ";
        
            dba.update(updateStatement);
        }

        //This JSP will query the database for the newly added customer details,
        //to confirm that they have been added to the database, and to display
        ///them.
        queryStatement2 =
                "SELECT customer_email, billing_address, cardno, card_exp, card_type "
                +"FROM customer "
                +"WHERE customer_email = '" + customer_email + "' "
                +"AND billing_address = '" + billing_address + "' "
                +"AND cardno = '" + cardno + "' "
                +"AND card_exp = '" + card_exp + "' "
                +"AND card_type = '" + card_type + "' "
                +"AND staff = 'CUSTOMER';";
        
        rs = dba.query(queryStatement2);
        
        while(rs.next()){
            check_customer_email = rs.getString("customer_email");
            check_billing_address = rs.getString("billing_address");
            check_cardno = rs.getString("cardno");
            check_card_exp = rs.getString("card_exp");
            check_card_type = rs.getString("card_type");
            
            //Make the card names readable to the user:
            if(check_card_type.equals("V")){
                check_card_type = "Visa";
            }
            if(check_card_type.equals("MC")){
                check_card_type = "MasterCard";
            }
        
        "UPDATE booking "
                +"SET paid = 't' "
                +"WHERE customer_email = '" + request.getParameter("customer_email") + "' "
                    +"AND booking_date = '" + request.getParameter("booking_date") + "' "
                    +"AND booking_period = '" + request.getParameter("booking_period") + "'; ";
        
        }
    } catch(SQLException ex){
        
    }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
	<!-- Page Title -->
	<title>Payment received | Anglian Bike Hire</title>
	<link rel="stylesheet" media="only screen and (min-width: 601px)" href="css/desktop.css" />
	<link rel="stylesheet" media="only screen and (max-width: 600px)" href="css/mobile.css" />
</head>

<body>
	<!-- Content DIV -->
	<div class="content">
		<!-- Header DIV -->
 		<div class="header">
	 		<div class="logo">
                            <a href="index.html"><img src="resources/images/graphics/abh_logo_small.png" alt="Logo image" /></a>
 			</div>
 		</div>

 		<div class="menu">
 			<a href="index.jsp">Home</a> |
 			<a href="routes.html">Local Routes</a> |
 			<a href="contact.html">Contact</a>
                        
		</div>
		
		<div class="menu_buttons">
                    <%=uafg.generateHTML(request, response, "index.jsp")%>
		</div>	
				
		<div class="pagecontent">
		
		<!-- Page specific content here-->
                <h3>Thank you for your custom</h3>
                
                <p>
                    We have received your payment. Your bike(s) will be available for
                    collection from us on the specified date and time.
                </p>
                
                
                <h3>Your payment details</h3>
                <div>
                    Email address:
                        <%=check_customer_email%><br>
            
                    Billing address: 
                        <%=check_billing_address%><br>
            
                    Card number: 
                        <%=check_cardno%><br>
            
                    Card expiration: 
                        <%=check_card_exp%><br>
            
                    Card type: 
                        <%=check_card_type%>
                </div>
        
                <div class="spacer"></div>
                
                <form action="SelectBookingDateAndTime.jsp">
                    <input type="hidden"
                           name="customer_email"
                           value="<%=request.getParameter("customer_email")%>">
                    <button type="submit">                  
                        View other bookings         
                    </button>
                </form>
        
                <form action="index.jsp">
                    <input type="hidden"
                           name="customer_email"
                           value="<%=request.getParameter("customer_email")%>">
                    <button type="submit">
                        Continue browsing
                    </button>
                </form>
		
		<!-- Page specific content ends-->
                
                <div class="icons_line">
                    <span class="icons">
                        <a href="https://twitter.com/anglianbikehire">	
                            <img src="resources/images/graphics/twitter.png" alt="Twitter image"></a>
                    </span>
                    <span class="icons">
                        <a href="https://www.facebook.com/anglianbikehireltd">
                            <img src="resources/images/graphics/facebook.png" alt="Facebook icon"></a>
                    </span>
                </div>
                </div>
                
                <!-- End of page content-->
		
 		<!-- Footer DIV -->
 		<div class="footer"> 
 			<a href="mailto:info@anglian-bike-hire.co.uk">info@anglian-bike-hire.co.uk</a>
 		</div>
	</div> 
    <!-- End of content DIV -->
    </body>
</html>