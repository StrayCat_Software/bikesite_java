<%-- 
    Document   : DeleteBooking
    Created on : 09-Dec-2014, 14:06:47
    Author     : wxu13keu
--%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="DatabaseTools.DBAccessor"%>
<%
    //This page is intended specifically for the handling of the deletion of a
    //bike, and then redirecting the user back to PaymentForm.jsp
    
    DBAccessor dba = new DBAccessor();
    ResultSet rs = null;
    String bike_id = request.getParameter("bike_id");
    String sqlStatement = null;
    int count = 0;
    String booking_id = null;
    String onPage = "Redirecting...";
    
    try{
        
        //First, get the booking id of the bike to be deleted
        sqlStatement = 
                "SELECT booking_id "
                +"FROM booked_bike "
                +"WHERE bike_id = '" + bike_id + "';";
        
        rs = dba.query(sqlStatement);
        
        while(rs.next()){
            booking_id = rs.getString("booking_id");
        }
        
        //get the number of bikes associated with the booking of the bike to be
        //deleted
        sqlStatement =
                "SELECT count(bike_id) AS count "
                +"FROM booked_bike "
                +"WHERE booking_id = " + booking_id + ";";
        
        rs = dba.query(sqlStatement);
        
        while(rs.next()){
            count = rs.getInt("count");
        }
        
        //If there is only one bike associated with the booking while that bike
        //is being deleted, delete the booking also.
        if(count == 1){
            sqlStatement =
                    "DELETE FROM booking "
                    +"WHERE booking_id = '" + booking_id + "';";
            
            dba.update(sqlStatement);
        }
        
        //Finally, and unconditionally, delete the bike
        sqlStatement = 
                "DELETE FROM booked_bike "
                +"WHERE bike_id = '" + bike_id + "';";
        
        dba.update(sqlStatement);
        
        //Pass booking and bike ID to HttpServletRequest.
        String booking_date = request.getParameter("booking_date");
        String booking_period = request.getParameter("booking_period");
        request.setAttribute("booking_date", booking_date);
        request.setAttribute("booking_period", booking_period);
        
        //Reload PaymentForm.jsp. 
        request.getRequestDispatcher("/PaymentForm.jsp").forward(request, response);
        
    } catch (SQLException ex){
        onPage = "SQL Exception occured. <br>SQL query was: <br><br>" + sqlStatement;
    }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Redirecting | Anglian Bike Hire</title>
    </head>
    <body>
        <h1><%=onPage%></h1>
        
    </body>
</html>
