<%-- 
    Document   : Sign-in_Register
    Created on : 30-Nov-2014, 15:12:12
    Author     : Dan
--%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="DatabaseTools.DBAccessor"%>

<%
    //The previous page's name is called, so that the on page form can redirect
    //back to it once the user has signed in
    String previousPage = request.getParameter("pageName");
    String errorMessage = "";
    
    //This checks to see if the failedAttempt variable with value "true" has been
    //passed from Servlet_SignIn. Finding this means that the log in has been failed
    //at least once, and that the error message telling the user that the log in has
    //failed should be displayed using a JavaBean.
    try{
        String failedAttempt = (String)request.getAttribute("failedAttempt");
        if(failedAttempt.equals("true")){
            errorMessage = "Incorrect email address or password.";
            previousPage = request.getParameter("previousPage");
        }
    } catch (NullPointerException ex){
        //Exception handling
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
	<!-- Page Title -->
	<title>Sign In | Anglian Bike Hire</title>
	<link rel="stylesheet" media="only screen and (min-width: 601px)" href="css/desktop.css" />
	<link rel="stylesheet" media="only screen and (max-width: 600px)" href="css/mobile.css" />
    </head>
    <body>
	<!-- Content DIV -->
	<div class="content">
		<!-- Header DIV -->
 		<div class="header">
	 		<div class="logo">
                            <a href="index.html"><img src="resources/images/graphics/abh_logo_small.png" alt="Logo image" /></a>
 			</div>
 		</div>

 		<div class="menu">
 			<a href="index.jsp">Home</a> |
 			<a href="routes.html">Local Routes</a> |
 			<a href="contact.html">Contact</a>
		</div>
                
                <div class="spacer"></div>
				
		<div class="pagecontent">
		
                    <h2>Sign in</h2>
                    
                    <div class="interactive_content">
        		
                        <form class="loginPanel" action="Servlet_SignIn">
                            Email address: 
                                <input type="email"
                                       accept=""name ="customer_email"><br>
            
                            Password:
                            <input type="password"
                                   accept=""name="password"><br>
            
                            <button type="submit">
                                Sign in
                            </button>
            
                            <input type="hidden"
                                    name="previousPage"
                                    value="<%=previousPage%>">
                        </form>
        
                        <form action="Register.jsp" method="GET">
               
                            <input id="Button_Register"
                                   type="submit"
                                   value="Register">
                        </form>
        
                        <!--This is where "Invalid username" message will appear-->
                        <div id="loginFailedText">
                                <%=errorMessage%>
                        </div>
                    </div>
		
		<!-- Page specific content ends-->
                
                <div class="icons_line">
                    <span class="icons">
                        <a href="https://twitter.com/anglianbikehire">	
                            <img src="resources/images/graphics/twitter.png" alt="Twitter image"></a>
                    </span>
                    <span class="icons">
                        <a href="https://www.facebook.com/anglianbikehireltd">
                            <img src="resources/images/graphics/facebook.png" alt="Facebook icon"></a>
                    </span>
                </div>
                </div>
                
                <!-- End of page content-->
		
 		<!-- Footer DIV -->
 		<div class="footer"> 
 			<a href="mailto:info@anglian-bike-hire.co.uk">info@anglian-bike-hire.co.uk</a>
 		</div>
		
	</div> 
    <!-- End of content DIV -->
    </body>
</html>