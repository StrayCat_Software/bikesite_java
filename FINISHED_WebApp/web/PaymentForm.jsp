<%-- 
    Document   : PaymentForm
    Created on : 08-Dec-2014, 11:53:46
    Author     : wxu13keu
--%>
<%@page import="UserAccountTools.UserAccountFormGenerator"%>
<%@page import="JSPTools.ViewBooking"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="DatabaseTools.DBAccessor"%>
<%
    //Generate the user account form object, in order to create the relevant
    //user account control buttons on the page.
    UserAccountFormGenerator uafg = new UserAccountFormGenerator();
  
    String onPage = "";
    
    
    
    //This JSP will check the customer table to see if the user has a card 
    //number and card_exp. If they do not, it will generate a form which will 
    //request their card details
    String generateForm = "";
    String cardno = "";
    String card_exp = "";
    String card_type = "";
    
    try{
        
    //Generate the list of booked bikes for this day, time and user using the
    //ViewBooking object, writen to the onPage string.
    ViewBooking vb = new ViewBooking();
    onPage = vb.generateHTML(request, response);
    
    //if onPage has a length of 0, there are no entries to display, thus the
    //user is redirected to SelectBookingDateAndTime.jsp.
    //in practical terms, this means that the user has deleted the last bike in
    //a booking, and thus is redirected away from a page empty of content.
    if(onPage.length() == 0){
        request.getRequestDispatcher("/SelectBookingDateAndTime.jsp").forward(request, response);
    }
    
        DBAccessor dba = new DBAccessor();
    
        String query = "SELECT cardno, card_exp, card_type FROM customer WHERE customer_email = '" 
                + request.getParameter("customer_email") + "';";
    
        ResultSet rs = dba.query(query);
    
        while(rs.next()){
            cardno = request.getParameter("cardno");
            card_exp = request.getParameter("card_exp");
            card_type = request.getParameter("card_type");
        }
        
        //if(cardno.equals(null) || card_exp.equals(null) || card_type.equals(null)){
            
            
            
            //onPage += generateForm;
            
    } catch(SQLException ex){
        
    } catch(ClassNotFoundException ex){
               
    } catch(NullPointerException ex){
        
    }
    
    generateForm += 
                    "Card number"
                    +"\n<input type=\"number\""
                    +"\n       name=\"cardno\"><br>"
                    +"\nCard expiry"
                    +"\n<input type=\"text\""
                    +"\n       name=\"card_exp\"><br>"
                    +"\nBilling address"
                    +"\n<input type=\"text\""
                    +"\n       name=\"billing_address\"><br>"
                    +"\nCard type"
                    +"\n<input type=\"radio\""
                    +"\n       name=\"card_type\""
                    +"\n       value=\"V\" "
                    +"\n       checked>Visa"
                    +"\n<input type=\"radio\"" 
                    +"\n       name=\"card_type\""
                    +"\n       value=\"MC\">MasterCard"
                    +"\n<input type=\"radio\"" 
                    +"\n       name=\"card_type\" "
                    +"\n       value=\"AE\">American Express<br>";
    
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
	<!-- Page Title -->
	<title>Payment | Anglian Bike Hire</title>
	<link rel="stylesheet" media="only screen and (min-width: 601px)" href="css/desktop.css" />
	<link rel="stylesheet" media="only screen and (max-width: 600px)" href="css/mobile.css" />
    </head>
    <body>
	<!-- Content DIV -->
	<div class="content">
		<!-- Header DIV -->
 		<div class="header">
	 		<div class="logo">
                            <a href="index.html"><img src="resources/images/graphics/abh_logo_small.png" alt="Logo image" /></a>
 			</div>
 		</div>

 		<div class="menu">
 			<a href="index.jsp">Home</a> |
 			<a href="routes.html">Local Routes</a> |
 			<a href="contact.html">Contact</a>
		</div>
		
		<div class="menu_buttons">
                    <%=uafg.generateHTML(request, response, "index.jsp")%>
                </div>
                
                <div class="spacer"></div>
				
		<div class="pagecontent">
		
                    <div>
            <h2>Your basket</h2>
            
            <%=onPage%>
        </div>
        <div>
            <h2>Payment details</h2>
            <form action="ProcessPayment.jsp" method="GET">
                <input type="hidden"
                       name="customer_email"
                       value="<%=request.getParameter("customer_email")%>">
                <%=generateForm%>
                <button type="submit">
                    Pay for this booking
                </button>
            </form>
        </div>
         
                <div class="spacer"></div>
         
        <div>
            <form action="index.jsp" method="GET">
                <input type="hidden"
                       name="customer_email"
                       value="<%=request.getParameter("customer_email")%>">
                <button type="submit">
                    Continue browsing
                </button>
            </form>
                
            <form action="SelectBookingDateAndTime.jsp">
                <input type="hidden"
                       name="customer_email"
                       value="<%=request.getParameter("customer_email")%>">
                <button type="submit">                  
                    View other bookings            
                </button>
            </form>
                
        </div>
		
		<!-- Page specific content ends-->
                
                <div class="icons_line">
                    <span class="icons">
                        <a href="https://twitter.com/anglianbikehire">	
                            <img src="resources/images/graphics/twitter.png" alt="Twitter image"></a>
                    </span>
                    <span class="icons">
                        <a href="https://www.facebook.com/anglianbikehireltd">
                            <img src="resources/images/graphics/facebook.png" alt="Facebook icon"></a>
                    </span>
                </div>
                </div>
                
                <!-- End of page content-->
		
 		<!-- Footer DIV -->
 		<div class="footer"> 
 			<a href="mailto:info@anglian-bike-hire.co.uk">info@anglian-bike-hire.co.uk</a>
 		</div>
		
	</div> 
    <!-- End of content DIV -->
    </body>
</html>