<%-- 
    Document   : SelectBookingDateAndTime
    Created on : 09-Dec-2014, 14:51:01
    Author     : wxu13keu
--%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="UserAccountTools.UserAccountFormGenerator"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="DatabaseTools.DBAccessor"%>
<%
    //This page allows the user to select the date and time that they want to make
    //a payment for, allowing them to handle multiple bookings.
    UserAccountFormGenerator uafg = new UserAccountFormGenerator();
    
    DBAccessor dba = new DBAccessor();
    ResultSet rs = null;
    String booking_date = null;
    String booking_period = null;
    int booking_id = 0;
    String note = null;
    String customer_email = request.getParameter("customer_email");
    String onPage = "";
    
    String sqlStatement = 
            "SELECT DISTINCT booking_date, booking_period "
            +"FROM booking "
            +"WHERE customer_email = '" + customer_email + "';";
   
    rs = dba.query(sqlStatement);
    
    while(rs.next()){
        booking_date = rs.getString("booking_date");
        booking_period = rs.getString("booking_period");
        
        //Get todays date and parse booking_date to Date objects
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date todaysDate = new Date();
        Date bookingDate = df.parse(booking_date);
        
        //If the booking is for today or later, it will be added to the page
        //content, otherwise, it will not.
        if(bookingDate.after(todaysDate) || bookingDate.equals(todaysDate)){
            
            //Generate a form passing the booking date and period to PaymentForm.jsp,
            //which is dynamically generated.
            
            onPage += 
                    "\n<div>"
                    +"\n    Date: " + booking_date + "<br>"
                    +"\n    Time: " + booking_period + "<br>"
                    +"\n    <form action=\"PaymentForm.jsp\">"
                    +"\n        <input type=\"hidden\""
                    +"\n               name=\"customer_email\""
                    +"\n               value=\"" + request.getParameter("customer_email") +"\">"
                    +"\n        <input type=\"hidden\""
                    +"\n               name=\"booking_date\""
                    +"\n               value=\"" + booking_date + "\">"
                    +"\n        <input type=\"hidden\""
                    +"\n               name=\"booking_period\""
                    +"\n               value=\"" + booking_period + "\""
                    +"\n        <input type=\"submit\">"
                    +"\n        <button type=\"submit\">"
                    +"\n            View this booking"
                    +"\n        </button>"
                    +"\n    </form>"
                    +"\n<div>";
            }
    }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

<head>
	<!-- Page Title -->
	<title>Select Booking | Anglian Bike Hire</title>
	<link rel="stylesheet" media="only screen and (min-width: 601px)" href="css/desktop.css" />
	<link rel="stylesheet" media="only screen and (max-width: 600px)" href="css/mobile.css" />
</head>

<body>
	<!-- Content DIV -->
	<div class="content">
		<!-- Header DIV -->
 		<div class="header">
	 		<div class="logo">
                            <a href="index.html"><img src="resources/images/graphics/abh_logo_small.png" alt="Logo image" /></a>
 			</div>
 		</div>

 		<div class="menu">
 			<a href="index.jsp">Home</a> |
 			<a href="routes.html">Local Routes</a> |
 			<a href="contact.html">Contact</a>
		</div>
		
		<div class="menu_buttons">
                    <%=uafg.generateHTML(request, response, "index.jsp")%>
		</div>	
				
		<div class="pagecontent">
		
		<!-- Page specific content here-->			
                <h3>Select the booking you would like to pay for:</h3>
        
                <%=onPage%>        
        
                <div class="spacer"></div>
                <form action="index.jsp">
                    <input type="hidden"
                           name="customer_email"
                           value="<%=customer_email%>">
                    <button type="submit">
                        Continue browsing
                    </button>
                </form>
		
		<!-- Page specific content ends-->
                
                <div class="icons_line">
                    <span class="icons">
                        <a href="https://twitter.com/anglianbikehire">	
                            <img src="resources/images/graphics/twitter.png" alt="Twitter image"></a>
                    </span>
                    <span class="icons">
                        <a href="https://www.facebook.com/anglianbikehireltd">
                            <img src="resources/images/graphics/facebook.png" alt="Facebook icon"></a>
                    </span>
                </div>
                </div>
                
                <!-- End of page content-->
		
 		<!-- Footer DIV -->
 		<div class="footer"> 
 			<a href="mailto:info@anglian-bike-hire.co.uk">info@anglian-bike-hire.co.uk</a>
 		</div>
		
	</div> 
    <!-- End of content DIV -->
    </body>
</html>