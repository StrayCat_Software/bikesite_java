<%-- 
    Document   : ProcessBooking
    Created on : 05-Dec-2014, 21:47:54
    Author     : Dan
--%>

<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="DatabaseTools.DBAccessor"%>
<%@page import="JSPTools.CheckBikeBookingPROCEEDURAL_1"%>
<%@page import="UserAccountTools.UserAccountFormGenerator"%>

<%
    DBAccessor dba = new DBAccessor();
    ResultSet rs = null;
    int i = 0;
    
    String queryStatement = null;
    String updateStatement = null;
    
    try{
        //Check whether the customer already exists on the database
        queryStatement =
                "SELECT * FROM customer WHERE customer_email = '" + request.getParameter("customer_email") + "';";
    
        rs = dba.query(queryStatement);

        //Count number of entries returned under that email (primary key)
        while(rs.next()){
            i++;
        }
        
        //If the user does not exist on the database, add them to it
        if(i == 0){
            updateStatement =
                    "INSERT INTO customer "
                    +"(customer_email, customer_name) "
                    +"VALUES( '" + request.getParameter("customer_email") + "', 'anonymous')";
            
            dba.update(updateStatement);
        }
        
    } catch (SQLException ex){
        
    }
    
    CheckBikeBookingPROCEEDURAL_1 cbb = new CheckBikeBookingPROCEEDURAL_1();
    UserAccountFormGenerator uafg = new UserAccountFormGenerator();    
%>

<%--<%=queryStatement%><br><br>
<%=i%><br><br>
<%=updateStatement%><br><br>--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

<head>
	<!-- Page Title -->
	<title>Bookings | Anglian Bike Hire</title>
	<link rel="stylesheet" media="only screen and (min-width: 601px)" href="css/desktop.css" />
	<link rel="stylesheet" media="only screen and (max-width: 600px)" href="css/mobile.css" />
</head>

<body>
    
	<!-- Content DIV -->
	<div class="content">
		<!-- Header DIV -->
 		<div class="header">
	 		<div class="logo">
                            <a href="index.html"><img src="resources/images/graphics/abh_logo_small.png" alt="Logo image" /></a>
 			</div>
 		</div>

 		<div class="menu">
 			<a href="index.jsp">Home</a> |
 			<a href="routes.html">Local Routes</a> |
 			<a href="contact.html">Contact</a>
		</div>
		
		<div class="menu_buttons">
                    <%=uafg.generateHTML(request, response, "index.jsp")%>
		</div>	
				
		<div class="pagecontent">
		
		<!-- Page specific content here-->			
                <div class="spacer"></div>
            
           
                <div class="pagecontent">
	
                    <!-- Page specific content here-->
                    
                    
                    
                    <div>
                        <%=cbb.processBooking(request, response)%>
                        <%--<%=mb.checkAndNotify(request, response)%>--%>

                        <!--Proceed to checkout button-->
                        <div id="checkoutButton">
                            <form action="SelectBookingDateAndTime.jsp"  method="GET">

                                <input type="hidden"
                                       name="customer_email"
                                       value="<%=request.getParameter("customer_email")%>">
                                <input type="hidden"
                                       name="booking_date"
                                       value="<%=request.getParameter("booking_date")%>">
                                <input type="hidden"
                                       name="booking_period"
                                       value="<%=request.getParameter("booking_period")%>">
                                <button type="submit"
                                        name="ProceedToCheckout">
                                    Proceed to Checkout
                                </button>
                            </form>
                        </div>
                    </div>
            </div>
		
		<!-- Page specific content ends-->
                
                <div class="icons_line">
                    <span class="icons">
                        <a href="https://twitter.com/anglianbikehire">	
                            <img src="resources/images/graphics/twitter.png" alt="Twitter image"></a>
                    </span>
                    <span class="icons">
                        <a href="https://www.facebook.com/anglianbikehireltd">
                            <img src="resources/images/graphics/facebook.png" alt="Facebook icon"></a>
                    </span>
                </div>
                </div>
                
                <!-- End of page content-->
		
 		<!-- Footer DIV -->
 		<div class="footer"> 
 			<a href="mailto:info@anglian-bike-hire.co.uk">info@anglian-bike-hire.co.uk</a>
 		</div>
		
	</div> 
    <!-- End of content DIV -->
    </body>
</html>
















            