package UserAccountTools;

import DatabaseTools.DBAccessor;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Dan
 */
public class UserAccountFormGenerator {
    
   
    /**The following method checks to see if it can retrieve a customer name and 
     * email added through a previous page (such as the sign in page). If it 
     * cannot, the null pointer exception is caught, and a sign in link is 
     * created. Otherwise, a link to the user's account page is created.
     * @param request
     * @param response
     * @param pageName
     * @param PageName
     * @return 
     **/
    public String generateHTML(HttpServletRequest request, HttpServletResponse response, String pageName/*, String customer_email*/){
        DBAccessor dba = new DBAccessor();
        ResultSet rs = null;
        
        String email = request.getParameter("customer_email");
        String query = "SELECT customer_name FROM customer WHERE customer_email = '" + email + "';";
        
        //by default, the output is set as if there were no use signed in. The act of supplying
        //A username via the sign in form changes this.
        String customer_name = 
                        "<!--Below is the sign in button, which only appears if the user is not signed in-->"
                        +"\n<div class=\"DYNAMICALLY_GENERATED_SIGN_IN_BUTTON\">"
                        +"\n    <form action=\"SignIn.jsp\">"
                        +"\n        <input type=\"submit\" value=\"Sign in\">"
                        +"\n        <input type=\"hidden\" name =\"pageName\" value=\"" + pageName + "\">"
                        +"\n    </form>"
                        +"</div>"
                        +"\n<!--Below is the register button, which only appears if the user is not signed in-->"
                        +"\n<div class=\"DYNAMICALLY_GENERATED_REGISTER_BUTTON\">"
                        +"\n    <form class=\"THIS_IS_A_TEST\" action=\"Register.jsp\">"
                        +"\n        <input type=\"submit\" value=\"Register\">"
                        +"\n        <input type=\"hidden\" name = pageName value=\"HomePage.jsp\">"
                        +"\n    </form>"
                        +"\n</div>"
                        ;
        
        try{
            if((!email.equals(null) || !email.equals(""))){
                //Get the customer_name using a database query. This proves that the 
                //database connection is working, and that other customer attributes 
                //can be retrieved if needed.
                rs = dba.query(query);
                while(rs.next()){
                    customer_name = 
                            "<div class=\"DYNAMICALLY_GENERATED_USER_ACCOUNT_BUTTON\">"
                            +"\n    <form action=\"UserAccount.jsp\">"
                            +"\n        <input type=\"submit\" value=\"" + rs.getString("customer_name") + "\">"
                            +"\n        <input type=\"hidden\" name=\"pageName\" value=\"" + pageName + "\">"
                            +"\n    </form>"
                            +"\n</div>"
                            +"\n<div class=\"DYNAMICALLY_GENERATED_SIGN_OUT_BUTTON\">"
                            +"\n    <form action=\"HomePage.jsp\">"
                            +"\n        <input type=\"submit\" value=\"Sign out\">"
                            +"\n        <input type=\"hidden\" name=\"pageName\" value=\"" + pageName + "\">"
                            +"\n        <input type=\"hidden\" name=\"customer_email\" value=\"" + null +"\">"
                            +"\n    </form>"
                            +"</div>"
                            ;
                }
            }
        }catch (SQLException | ClassNotFoundException | NullPointerException ex){
            //NullPointerException is important to catch, because the customer_email
            //variable will always be null when the website is first visited by the
            //user.
        }
        return customer_name;
    }
}