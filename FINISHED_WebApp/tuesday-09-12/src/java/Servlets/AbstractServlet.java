/**
 * This class serves as an abstract template for all java servlets in this web
 * application. Its subclass servlets each have unique overided versions of the
 * generateHTML() method, so as to process results and generate HTML content
 * relevant to their respective functions.
 */

package Servlets;

import DatabaseTools.DBAccessor;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Dan
 */
public abstract class AbstractServlet extends HttpServlet {
	
    //class variables are protected so that they are accessible only to subclass
    //servlets.
    protected ResultSet rs = null;
    protected int check_booking_id = 0;
    protected String check_customer_email = "";
    protected String check_booking_date = "";
    protected String check_booking_period = "";
    protected float check_amount = 0;       
    protected String check_note = "";
    protected DBAccessor dba = new DBAccessor();
    protected int pKey = 0;
    
    public void generateHTML(HttpServletRequest request, HttpServletResponse response) 
            throws IOException, SQLException, ClassNotFoundException{
        //Each subclass servlet overrides this method.
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        
        //Execute the update using the DBAccessor class.
        DBAccessor dba = new DBAccessor();
        dba.update(insertQuery(request, response));
        
        //Finally, generate the HTML document.
        generateHTML(request,response);
    }
    
    /**
     * This is used to supply the insert query to the processRequest method. It
     * is overridden for each of this abstract servlet's subclasses.
     * @param request
     * @param response
     * @return 
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     */
    public String insertQuery(HttpServletRequest request, HttpServletResponse response)
            throws IOException, SQLException, ClassNotFoundException{
        //overridden and unique for each subclass.
        return null;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. 
    //Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                processRequest(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Servlet_AddBooking.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Servlet_AddBooking.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                processRequest(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Servlet_AddBooking.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Servlet_AddBooking.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Adds bookings to the booking table.";
    }// </editor-fold>
}