/**
 * This class serves as an abstract template for all java servlets in this web
 * application. Its subclass servlets each have unique overided versions of the
 * generateHTML() method, so as to process results and generate HTML content
 * relevant to their respective functions.
 */

package Servlets;

import DatabaseTools.DBAccessor;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Dan
 */
public class Servlet_SignIn extends HttpServlet {
	
    //class variables are protected so that they are accessible only to subclass
    //servlets.
    protected ResultSet rs = null;
    protected String customer_email = "";
    protected String check_customer_email = "";
    protected String password;
    protected String check_password = "";
    protected String previousPage = null;
    
    /**
     * This method is called to call the next page to be displayed, which is either
     * the last page visited before the sign in attempt, or the sign in page itself
     * i.e. failing a sign in will return the user to the sign in page.
     * @param request
     * @param response
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    public void callJSP(HttpServletRequest request, HttpServletResponse response) 
            throws IOException, SQLException, ClassNotFoundException{

        try {
            //The following statements reset the HttpServletRequest object's previousPage attribute
            //as what it was before this servlet was accessed, which will help a successfull sign in
            //attempt result in redirection to the page the user visited immediatly before attempting
            //to sign in.
            String previousPage = request.getParameter("previousPage");
            request.setAttribute("previousPage", request.getParameter("previousPage"));
            
            String destinationPage = null;
            
            customer_email = request.getParameter("customer_email");
            password = request.getParameter("password");
            
            //First, the true customer email and password are determined
            while(rs.next()){
                check_customer_email = rs.getString("customer_email");
                check_password = rs. getString("password");
            }
            
            //If the email field is empty, the user is sent back to the sign in page
            if(customer_email.equals("") || customer_email.equals(null)){
            //if(request.getParameter("customer_email").equals("") || request.getParameter("customer_email").equals(null)){
                destinationPage = "SignIn.jsp";
            }
            
            //Next, it is checked whether the supplied email and password match both
            //the real email and password found in the result set. Any other outcome
            //results in a failed sign in attempt, and the supplied email being nullified.
            if(customer_email.equals(check_customer_email) && password.equals(check_password)){
                
                destinationPage = previousPage;
            } else {
                customer_email = "";
                destinationPage = "SignIn.jsp";
            }
            
            //The following statement involves adding an attribute to the HttpServletRequest object,
            //which is used by SignIn.jsp to determine that the previous sign in attempt failed, so
            //that an error message can be displayed on the page.
            request.setAttribute("failedAttempt", "true");
            
            //The following statement calls the decided destination JSP page.
            getServletContext().getRequestDispatcher("/" + destinationPage).forward(request, response);
        
        } catch (ServletException ex) {
            Logger.getLogger(Servlet_SignIn.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        
        //Execute the update using the DBAccessor class.
        DBAccessor dba = new DBAccessor();
        rs = dba.query(insertQuery(request, response));
        
        //Finally, generate the HTML document.
        callJSP(request,response);
    }
    
    /**
     * This is used to supply the insert query to the processRequest method. It
     * is overridden for each of this abstract servlet's subclasses.
     * @param request
     * @param response
     * @return 
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     */
    public String insertQuery(HttpServletRequest request, HttpServletResponse response)
            throws IOException, SQLException, ClassNotFoundException{
        String customer_email = request.getParameter("customer_email");
        return "SELECT customer_email, password FROM customer WHERE customer_email = '" + customer_email + "';";
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. 
    //Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                processRequest(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Servlet_AddBooking.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Servlet_AddBooking.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                processRequest(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(Servlet_AddBooking.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Servlet_AddBooking.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Adds bookings to the booking table.";
    }// </editor-fold>
}