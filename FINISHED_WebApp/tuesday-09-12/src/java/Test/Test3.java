/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import DatabaseTools.DBAccessor;
import JSPTools.NEW;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Dan
 */
public class Test3 {
    public static void main (String[] args){
        String htmlOutputBean = "";
        try {
            String customer_email = "D.pawsey@uea.ac.uk";//request.getParameter("customer_email");
            String booking_date = "11/11/2014";//request.getParameter("booking_date");
            String booking_period = "ALL";//request.getParameter("booking_period");
            String amount = "1";//request.getParameter("amount");
            String note = "BOOKING FORM TEST";//request.getParameter("note");
            String model = "Kona Lisa HT";//request.getParameter("model");
            
            boolean updateSuccessful = true;
            
            String bikeBike_id = "";
            String booked_bikeBike_id = "";
            
            DBAccessor dba = new DBAccessor();
            String query1 = "SELECT bike_id FROM bike WHERE model = '" + model + "';";
            ResultSet rs = dba.query(query1);
            
            //Check through all of the bike_ids for bikes of the model specified
            while(rs.next()){
                bikeBike_id = rs.getString("bike_id");                
                String query2 = "SELECT bike_id FROM booked_bike WHERE bike_id = '" + bikeBike_id + "';";
                ResultSet rs2 = dba.query(query2);
                
                //Check whether the same bike_id appears in any record in booked_bike
                int i = 0;
                while(rs2.next()){
                    booked_bikeBike_id = rs2.getString("bike_id");
                    i++;
                }
                
                //If the bike is found in the booked bike table, updateSuccessful
                //is set to false
                if(bikeBike_id.equals(booked_bikeBike_id)){
                    updateSuccessful = false;
                }
            }
            
            //If updateSuccessful is true, the SQL statement is created, an the
            //booking confirmation HTML is pushed to the document. Otherwise, no
            //command is set to the DBAccessor object, and the appology
            //and suggestion page is pushed to the document.
            if (updateSuccessful == true){
                
                dba.update(
                    "INSERT INTO booking "            
                        +"(booking_id, customer_email, booking_date, booking_period, amount, note) "
                        +"VALUES((SELECT max(booking_id) + 1 FROM booking), '" + customer_email + "', '" + booking_date + "', '" + booking_period + "', " + amount + ", '" + note + "');"
                    +"INSERT INTO booked_bike"
                        +"(booking_id, bike_id, note)"
                        +"VALUES(SELECT max(booking_id) FROM booking), '" + bikeBike_id + "', '" + note + "');"
                );
                
                //Query the database for the most recently added booking from the booking and booked_bike
                //tables.
                ResultSet rs3 = dba.query(
                        "SELECT booking.booking_id, bike.bike_id, model, booking_date, booking_period, amount, booking.note, day "
                        +"FROM booking "
                        +"LEFT JOIN booked_bike ON booked_bike.booking_id = booking.booking_id "
                        +"LEFT JOIN bike ON bike.bike_id = booked_bike.bike_id "
                        +"LEFT JOIN rates ON rates.bike_type = bike.bike_type "
                        +"WHERE booking.booking_id = (SELECT max(booking_id) FROM booking);"
                );
                
                String check_booking_id = "";
                String check_bike_id = "";
                String check_model = "";
                String check_booking_date = "";
                String check_booking_period = "";
                float check_amount = 0;
                String check_note = "";
                float check_price = 0;
                
                while(rs3.next()){
                    check_booking_id = rs3.getString("booking_id");
                    check_bike_id = rs3.getString("bike_id");
                    check_model = rs3.getString("model");
                    check_booking_date = rs3.getString("booking_date");
                    check_booking_period = rs3.getString("booking_period");
                    check_amount = rs3.getFloat("amount");
                    check_note = rs3.getString("note");
                    check_price = rs3.getFloat("day") * check_amount;
                }
                
                //If the the returned period is all day, change it so that it is
                //more human-readable. Otherwise, change the price so that they
                //are only charged for a half day.
                if(check_booking_period.equals("ALL")){
                    check_booking_period = "All day";
                }
                else{
                    ResultSet rs4 = dba.query("SELECT half FROM rates WHERE bike_type = "
                            + "(SELECT bike_type FROM bike WHERE bike_id = '" + check_bike_id + "';");
                    while(rs4.next()){
                        check_price = rs4.getFloat("half") * check_amount;
                    }
                }
               
                //Generate output HTML.
                htmlOutputBean = 
                        "<h1>Your booking of " + model + " has been processed</h1>"
                        +"\n<h2>Order details:</h2>"
            
                        //Booking details table, displaying the order placed by the user in
                        //a recepit style list.
                        +"\n<div>"
                        +"\n     <div>"
                        +"\n        booking reference number:"
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n        " + check_booking_id
                        +"\n     </div>"
                        +"\n     <div>" 
                        +"\n        Bike reference number:" 
                        +"\n     </div>" 
                        +"\n     <div>" 
                        +"\n       " + check_bike_id 
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n        Bike model:"
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n       " + check_model
                        +"\n     </div>"
                        +"\n     <div>" 
                        +"\n         Date:"
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n        " + check_booking_date
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n         Time:"
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n       " + check_booking_period
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n         Number of this model booked:"
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n       " + check_amount
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n       " + check_price
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n         Notes"
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n       " + check_note
                        +"\n     </div>"
                        +"\n</div>"
                        +"\n<a href=\"index.html\">Continue browsing</a><br>"
                        +"\n<a href=\"CheckoutForm.jsp\">Proceed to checkout</a><br>"
                ;
            }
            else{
                htmlOutputBean = "This is the appology and suggestion output.";
            }

        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(NEW.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(htmlOutputBean);
    }
}
