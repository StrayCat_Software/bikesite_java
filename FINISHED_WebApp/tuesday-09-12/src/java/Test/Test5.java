/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import DatabaseTools.DBAccessor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dan
 */
public class Test5 {
    public static void main(String[] args){
        DBAccessor dba = new DBAccessor();
        ResultSet rs = null;
        String query = null;
        String htmlOutputBean = "";
        ArrayList<String> BikeList = new ArrayList<String>();
        ArrayList<String> ConfirmedBikeList = new ArrayList<String>();
        
        ArrayList<String> CheckBikeID = new ArrayList<String>();
        String check_booking_id = null;
        String check_model = null;
        String check_booking_date = null;
        String check_booking_period = null;
        String check_note = null;
        float check_price = 0;
            
        String customer_email = "D.Pawsey@uea.ac.uk"; //request.getParameter("customer_email");
        String booking_date = "15/12/2015"; //request.getParameter("booking_date");
        String booking_period = "AM"; //request.getParameter("booking_period");
        int amount = 3; //request.getParameter("amount");
        String note = "BOOKING FORM TEST"; //request.getParameter("note");
        String model = "Kona Lisa HT"; //request.getParameter("model");
        int booking_id = 0;

        try {
            //First check whether there are any bookings in the booking table.
            //If there are none, set the booking_id being added as 1.
            query = "SELECT max(booking_id) AS max FROM booking;\n";
            rs = dba.query(query);
                
            while(rs.next()){
                int maxBooking_id = rs.getInt("max");
                htmlOutputBean += query + " Result: " + maxBooking_id + "\n";
               
                
                //If the max booking_id found in the booking table is 0, it
                //means that there are currently no bookings in the table.
                //In this case, the booking id is set to 1.
                if(maxBooking_id == 0){
                    booking_id = 1;
                }
                else{
                    booking_id = maxBooking_id;
                }
                htmlOutputBean += "booking_id has been set to " + booking_id + "\n";
                
            }
            
            
            //Get all bike_id values of the model from table bike.
            query = "SELECT bike_id FROM bike WHERE model = '" + model + "';";
            htmlOutputBean += "\n" + query;
            rs = dba.query(query);
     
            //Add all bike_id attributes found to BikeList.
            while(rs.next()){
                htmlOutputBean += "\nFound bike_id: " + rs.getString("bike_id") + "\n";
                BikeList.add(rs.getString("bike_id"));
            }
            
            //Iterate through BikeList, removing any values which correspond to
            //bike_id attributes on the booked_bike table.
            query = "SELECT bike_id FROM booked_bike";
            rs = dba.query(query);
            htmlOutputBean += "\n" + query;
        
            int j = BikeList.size()-1;
            while(rs.next()){
                htmlOutputBean += "\nLoop";
                if(BikeList.get(j).equals(rs.getString("bike_id"))){
                    htmlOutputBean += "Removing " + BikeList.get(j) + " from BikeList";
                    BikeList.remove(j);
                    j--;
                }
            }
            
            //Add the last of the bike_id values to ConfirmedBikeList, and remove it from BikeList
            //Do this for as many times as the customer has requested bikes.
            for(int i = 0; i != amount; i++){
                ConfirmedBikeList.add(BikeList.get(BikeList.size()-1));
                BikeList.remove(BikeList.size()-1);
                htmlOutputBean += "\nAdded " + ConfirmedBikeList.get(ConfirmedBikeList.size()-1) + " to ConfirmedBikeList";
            }
            
            //All elements in confirmedBikeList are used to book bikes, therefore,
            //as many bikes as can be found from confirmedBikeList (corrisponding
            //to a maximum of the amount the user specified) are booked.
            //Insert the booking
            query =
                    "INSERT INTO booking"
                    +"(booking_id, customer_email, booking_date, booking_period, amount, note)"
                    +"VALUES((" + booking_id + " + 1), '" + customer_email + "', '" + booking_date + "', '" + booking_period + "', " + amount + ", '" + note + "');"   
            ;
            dba.update(query);
            htmlOutputBean += "\n" + query;
            
            //Add the bikes to the booking table
            for(int k = 0; k != ConfirmedBikeList.size(); k++){
                query =
                        "INSERT INTO booked_bike"
                        +"(booking_id, bike_id, note)"
                        +"VALUES((SELECT max(booking_id) FROM booking), '" + ConfirmedBikeList.get(k) + "', '" + note + "');"
                ;
                dba.update(query);
                htmlOutputBean += "\n" + query;
            }
            
            //Query the database for the most recently added booking from the booking and booked_bike
            //tables.                
            query =
                        "SELECT booking.booking_id, bike.bike_id, model, booking_date, booking_period, amount, booking.note, day "
                        +"FROM booking "
                        +"LEFT JOIN booked_bike ON booked_bike.booking_id = booking.booking_id "
                        +"LEFT JOIN bike ON bike.bike_id = booked_bike.bike_id "
                        +"LEFT JOIN rates ON rates.bike_type = bike.bike_type "
                        +"WHERE booking.booking_id = (SELECT max(booking_id) FROM booking) AND model = '" + model + "';"             
            ;
            
            rs = dba.query(query);
                
            htmlOutputBean += "\n" + query;
            
            while(rs.next()){
                
                //All bike_id attributes are added to ArrayList CheckBikeID, as
                //there may be many of these, where as each of the other
                //attributes will be the same for each record.
                CheckBikeID.add(rs.getString("bike_id"));
                
                check_booking_id = rs.getString("booking_id");
                check_model = rs.getString("model");
                check_booking_date = rs.getString("booking_date");
                check_booking_period = rs.getString("booking_period");
                check_note = rs.getString("note");
                check_price = rs.getFloat("day") * amount;
            }
            
            htmlOutputBean +=
                    "\n\nBooking reference:"
                    + check_booking_id + "\n"
                    +"Bike reference numbers: ";
            for(int k = 0; k != CheckBikeID.size(); k++){
                htmlOutputBean += "\n" + CheckBikeID.get(k);
            }
            htmlOutputBean +=
                    "\nBike model:"
                    +"\n" + model
                    +"\nBooking date:"
                    +"\n" + check_booking_date
                    +"\nTime"
                    +"\n" + check_booking_period
                    +"\nNote"
                    +"\n" + check_note
                    +"\nPrice"
                    +"\n" + check_price;
            
        } catch (SQLException ex) {
            Logger.getLogger(Test5.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Test5.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(htmlOutputBean);
    }
}
