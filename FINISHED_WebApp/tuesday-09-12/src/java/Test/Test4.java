/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import DatabaseTools.DBAccessor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dan
 */
public class Test4 {
    
    public static void main (String[] args){
        String query = null;
        String htmlOutputBean = "";
        
        String check_booking_id = "";
        String check_booking_date = "";
        String check_booking_period = "";
        String check_note = "";
        float check_price = 0;
        int bookingID = 0;
        
        DBAccessor dba = new DBAccessor();
        ResultSet rs = null;
        String check_model = null;
        String bike_id = "";
        ArrayList<String> BikeList = new ArrayList<String>();
        ArrayList<String> ConfirmedBikeList = new ArrayList<String>();
        ArrayList<String> CheckBikeList = new ArrayList<String>();
        ArrayList<String> CheckBikeID = new ArrayList<String>();
        
        String customer_email = "D.Pawsey@uea.ac.uk"; //request.getParameter("customer_email");
        String booking_date = "15/12/2015"; //request.getParameter("booking_date");
        String booking_period = "AM"; //request.getParameter("booking_period");
        int amount = 3; //request.getParameter("amount");
        String note = "BOOKING FORM TEST"; //request.getParameter("note");
        String model = "Kona Lisa HT"; //request.getParameter("model");
        
        int counter = 0;

        try{
            
            //First check whether there are any bookings in the booking table.
            //If there are none, set the booking_id being added as 1.
            query = "SELECT max(booking_id) AS max FROM booking;\n";
            rs = dba.query(query);
                
            while(rs.next()){
                int maxBooking_id = rs.getInt("max");
                htmlOutputBean += query + " Result: " + maxBooking_id + "\n";
               
                
                //If the max booking_id found in the booking table is 0, it
                //means that there are currently no bookings in the table.
                //In this case, the booking id is set to 1.
                if(maxBooking_id == 0){
                    bookingID = 1;
                }
                else{
                    bookingID = maxBooking_id;
                }
                htmlOutputBean += "booking_id has been set to " + bookingID + "\n";
                
            }
                
            //The sequence will loop for as many times as bikes have been requested 
            //(the "amount" entered)
            for(int i = 0; i != amount; i++){
                htmlOutputBean += "\n\nConfirmedBikeList contains " + ConfirmedBikeList.size() + " bikes\n";
                htmlOutputBean += "Booking bike " + (i + 1) + " of " + amount + "...\n";
                
                //Get all bike_id attributes that match the selected bike model.
                query = "SELECT bike_id FROM bike WHERE model = '" + model + "';";
                rs = dba.query(query);
                htmlOutputBean += "query1 is: " + query + "\n";
                
                //Add all bike_id attributes found to an ArrayList.
                while(rs.next()){
                    htmlOutputBean += "Found bike_id: " + rs.getString("bike_id") + "\n";
                    //bike_id = rs.getString("bike_id");
                    BikeList.add(rs.getString("bike_id"));
                    //htmlOutputBean += rs.getString("bike_id") + " is in BikeList" + "\n";
                }
                
                //If there are still bike_id strings in BikeList (i.e. there were
                //some found, and they haven't all been removed due to finding
                //that they match bike_id attributes in the booked_bike table),
                //then the first element in BikeList is added to ArrayList
                //ConfirmedBikeList.
                if(BikeList.isEmpty()){
                    //SAB
                }
                else {
                    /*
                    //Ensure that the ConfirmedBikeList does not already contain
                    //the most recently added item.
                    ConfirmedBikeList.add(BikeList.get(BikeList.size()-1));
                    for(int index = BikeList.size()-1; index >= 0; index--){
                        if(!ConfirmedBikeList.isEmpty() && ConfirmedBikeList.contains(BikeList.get(index))){
                            ConfirmedBikeList.remove(ConfirmedBikeList.size() - 1);
                        }
                    }
                    */
                    
                    //Ensure that the most recently added item in ConfirmedBikeList
                    //is not on the booked_bike table.
                    if(!ConfirmedBikeList.isEmpty()){
                        query = "SELECT bike_id FROM booked_bike WHERE bike_id = '" + ConfirmedBikeList.get(ConfirmedBikeList.size()-1) + "';";
                        rs = dba.query(query);
                        while(rs.next()){
                            if(!rs.getString("bike_id").isEmpty()){
                                ConfirmedBikeList.add(BikeList.get(BikeList.size()-1));
                            }
                        }
                    }
                    BikeList.clear();
                }
            }
            
            //All elements in confirmedBikeList are used to book bikes, therefore,
            //as many bikes as can be found from confirmedBikeList (corrisponding
            //to a maximum of the amount the user specified) are booked.
            //Insert the booking
            query =
                    "INSERT INTO booking"
                    +"(booking_id, customer_email, booking_date, booking_period, amount, note)"
                    +"VALUES((" + bookingID + " + 1), '" + customer_email + "', '" + booking_date + "', '" + booking_period + "', " + amount + ", '" + note + "');"   
            ;
            
            dba.update(query);
  
            htmlOutputBean += "\n" + query;
            
            htmlOutputBean += "\n" + "ConfirmedBikeList has " + ConfirmedBikeList.size() + " entries.";
            
            //Book the bikes.
            for(int j = 0; j != ConfirmedBikeList.size()-1; j++){
                             
                query =
                        "INSERT INTO booked_bike"
                        +"(booking_id, bike_id, note)"
                        +"VALUES((SELECT max(booking_id) FROM booking), '" + ConfirmedBikeList.get(j) + "', '" + note + "');"
                ;
                
                dba.update(query);

               //CheckBikeList.add(ConfirmedBikeList.get(0));
               //ConfirmedBikeList.remove(0);
            
                htmlOutputBean += "\n" + query;
            }
            
            //bookingID++;
            //Query the database for the most recently added booking from the booking and booked_bike
            //tables.                
            query =
                        "SELECT booking.booking_id, bike.bike_id, model, booking_date, booking_period, amount, booking.note, day "
                        +"FROM booking "
                        +"LEFT JOIN booked_bike ON booked_bike.booking_id = booking.booking_id "
                        +"LEFT JOIN bike ON bike.bike_id = booked_bike.bike_id "
                        +"LEFT JOIN rates ON rates.bike_type = bike.bike_type "
                        +"WHERE booking.booking_id = (SELECT max(booking_id) FROM booking) AND model = '" + model + "';"             
            ;
            
            rs = dba.query(query);
                
            htmlOutputBean += "\n" + query;
            
            while(rs.next()){
                
                //All bike_id attributes are added to ArrayList CheckBikeID, as
                //there may be many of these, where as each of the other
                //attributes will be the same for each record.
                CheckBikeID.add(rs.getString("bike_id"));
                
                check_booking_id = rs.getString("booking_id");
                check_model = rs.getString("model");
                check_booking_date = rs.getString("booking_date");
                check_booking_period = rs.getString("booking_period");
                check_note = rs.getString("note");
                check_price = rs.getFloat("day") * amount;
            }
            
            htmlOutputBean +=
                    "\n\nBooking reference:"
                    + check_booking_id + "\n"
                    +"Bike reference numbers: ";
            for(int k = 0; k != CheckBikeID.size(); k++){
                htmlOutputBean += "\n" + CheckBikeID.get(k);
            }
            htmlOutputBean +=
                    "\nBike model:"
                    +"\n" + model
                    +"\nBooking date:"
                    +"\n" + check_booking_date
                    +"\nTime"
                    +"\n" + check_booking_period
                    +"\nNote"
                    +"\n" + check_note
                    +"\nPrice"
                    +"\n" + check_price;

        } catch (SQLException ex) {
                Logger.getLogger(Test4.class.getName()).log(Level.SEVERE, null, ex);
                htmlOutputBean += "SQLException occured"; 
        } catch (ClassNotFoundException ex) {
                Logger.getLogger(Test4.class.getName()).log(Level.SEVERE, null, ex);
                htmlOutputBean += "ClassNotFoundException occured";
        }
        System.out.println(htmlOutputBean);
    }
}
