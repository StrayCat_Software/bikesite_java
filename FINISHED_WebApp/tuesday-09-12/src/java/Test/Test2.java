/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wxu13keu
 */
public class Test2 {
    public static void main(String[] args){
    
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost/IMT_Assignment2_Prototype_Database", "postgres", "querty_1324");
            
            //Create the statement
            Statement stmt = conn.createStatement();
            String sql = "INSERT INTO booking"
                         +"(booking_id, customer_email, booking_date, booking_period, amount, note)"
                         +"VALUES((SELECT max(booking_id) + 1 FROM booking), 'D.Pawsey@uea.ac.uk', '11/11/2014', 'ALL', 1, 'BOOKING FORM TEST');";
            
            stmt.executeUpdate(sql);
            
            ResultSet rs = stmt.executeQuery("SELECT (SELECT max(booking_id)) AS booking_id FROM booking;");
            while(rs.next()){
                System.out.println(rs.getString("booking_id"));
            }
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Test2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
