/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Test;

import DatabaseTools.DBAccessor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author wxu13keu
 */
public class Test {
    public static void main (String[] args){   
        String insertStatement = null;
        try{
             
        //The input fields from BookingForm.jsp are collected into strings.
        //First, the booking_id value is auto-generated during the SQL insert.
        boolean updateSucessful = true;
        String customer_email = "D.Pawsey@uea.ac.uk";//request.getParameter("customer_email");                  
        String booking_date = "10/12/2014";//request.getParameter("booking_date");                   
        String booking_period = "AM";//request.getParameter("booking_period");         
        String amount = "1";//request.getParameter("amount");         
        String note = "BOOKING FORM TEST";//request.getParameter("note");
        String model = "Kona Lisa HT";//request.getParameter("model");
        
        DBAccessor dba = new DBAccessor();
        ArrayList<String> BikeList = new ArrayList<String>();
        String AvailableBikeID = null;
        boolean bikeAvailable;
        
        
        String query1 = 
                "SELECT bike_id FROM bike WHERE model = '" + model + "';";
        System.out.println(query1);
        
        ResultSet rs2 = dba.query(query1);        
        String bike_bike_id = "";
        String booked_bike_bike_id = "";
                
        //Check through all of the bike_ids for bikes of the model specified
        while(rs2.next()){
            bike_bike_id = rs2.getString("bike_id");
                    
            String query2 = "SELECT bike_id FROM booked_bike WHERE bike_id = '" + bike_bike_id + "';";
            System.out.println(query2);
                    
            ResultSet rs3 = dba.query(query2);
                    
            //Each time a bike_id of the model specified is found, check
            //if it has a matching entry in table booked_bike
            while(rs3.next()){
                booked_bike_bike_id = rs3.getString("bike_id");
                System.out.println("Found booked_bike.bike_id: " + booked_bike_bike_id);

                //If the bike is not booked...
                if(bike_bike_id.equals(booked_bike_bike_id)){
                    System.out.println(bike_bike_id + " is already booked...");
                } else {   
                    //...Add it to the ArrayList
                    BikeList.add(bike_bike_id);
                    System.out.println("Added string \"" + bike_bike_id + " to BikeList.");
                }
            }
        }
                
        if(BikeList.isEmpty()){
            bikeAvailable = false;
            System.out.println("BikeList is empty.");
        } else {
            bikeAvailable = true;
            AvailableBikeID = BikeList.get(0);
            for(int i = 0; i == BikeList.size(); i++)
            {
                System.out.println(BikeList.get(i));
            }
        }
                
        //If a bike of the model is found, and it isnt already in the booked_bike
        //table, it is here added to the booked_bike table, and the booking is added
        //to the booking table
                        
        //if(bikeAvailable == true){
            insertStatement = 
                    "INSERT INTO booking "
                        +"(booking_id, customer_email, booking_date, booking_period, amount, note) "
                        +"VALUES((SELECT max(booking_id) + 1 FROM booking), '" + customer_email + "', '" + booking_date + "', '" + booking_period + "', " + amount + ", '" + note + "');"
                    +"INSERT INTO booked_bike"
                        +"(booking_id, bike_id, note)"
                        +"VALUES(SELECT max(booking_id) FROM booking), '" + AvailableBikeID + "', '" + note + "');";
            updateSucessful = true;
        //} else {
                //Supplying the string CANCEL to DBAccessor.update() 
                //cancels it's call to update the database.
        //        insertStatement = "CANCEL";
        //        updateSucessful = false;
        //}
       
        }catch(NullPointerException | SQLException | ClassNotFoundException ex){
            
        }
        System.out.println(insertStatement);
    }
}