/*
As database connections and statements are regularly made within this web app,
this class is designed to encapsulate the JDBC connection-statement-close
process, so that an SQL update or query can be performed using a single
statement, with the SQL command supplied as a parameter to it.
 */

package DatabaseTools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wxu13keu
 */
public class DBAccessor {
    
    
    private String databaseURL;
    private String databaseUser;
    private String databasePassword;
    private String sqlStatement;
    private Connection conn = null;
    private Statement stmt = null;
    private ResultSet rs = null;
    
    public DBAccessor(){
        DBAccessorKey dbak = new DBAccessorKey();
        databaseURL = dbak.getURL();
        databaseUser = dbak.getUser();
        databasePassword = dbak.getPassword();
    }
    /*
    This is where the database credentials are accessed from
    NOTE: Alter databse name, username and password here
    */
    
    /*public void setCredentials(){
        databaseURL = "jdbc:postgresql://localhost/IMT_Assignment2_Prototype_Database";
        databaseUser = "postgres";
        databasePassword = "querty_1324";
    }*/
    
    
    /*
    Whether the SQL command is an update or query, connect is called from within
    the respective method first.
    */
    public void connect() throws SQLException, ClassNotFoundException{
        
        //Connect to the databse
        Class.forName("org.postgresql.Driver");
        conn = DriverManager.getConnection(databaseURL, databaseUser, databasePassword);
           
        //Create the statement
        stmt = conn.createStatement();
    }
    
    /*
    This is called at the end of both the update and query functions
    */
    public void disconnect() throws SQLException, ClassNotFoundException{
        //rs.close();
        //stmt.close();
        conn.close();
    }

    /*
    Update statement method
    */
    public void update(String sqlStatement) throws SQLException{
        
        try {
        
            if(sqlStatement.equals("CANCEL")){
                //Supplying the string CANCEL to DBAccessor.update() cancels 
                //it's call to update the database. This is included to prevent
                //bikes from being booked when they are not available, when
                //booked via Servlet_AddBooking.java.
            } else {
                //Connect to the database, and create the statement
                this.connect();
        
                //Execute the update
                stmt.executeUpdate(sqlStatement);
            
                //Disconnect from the database
                this.disconnect();
            }
        } catch (SQLException | ClassNotFoundException | NullPointerException ex) {
            Logger.getLogger(DBAccessor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Statement getStatement() throws SQLException, ClassNotFoundException{
            //Connect to the database, and create the statement
            this.connect();
            return stmt;
    }
    
    public ResultSet query(String sqlStatement) throws SQLException, ClassNotFoundException{
        this.connect();
        rs = stmt.executeQuery(sqlStatement);
        this.disconnect();
        return rs;
    }
}