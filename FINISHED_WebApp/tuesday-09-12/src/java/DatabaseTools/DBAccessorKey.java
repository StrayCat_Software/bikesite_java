/**This file is written by the CredentialSetter program, which provides it 
*with user supplied database access credentials. Upon compilation of the 
*web application, these credentials are read by class DBAccessor, which 
*provides all servlets and JSP pages in the application with database access
*/

package DatabaseTools;

/**
*
* @author Dan
*/

public class DBAccessorKey {

    private final String databaseURL = "jdbc:postgresql://cmp-14jboss01.cmp.uea.ac.uk/";
    private final String databaseUser = "group05";
    private final String databasePassword = "group05";

    protected String getURL(){
        return databaseURL;
    }

    protected String getUser(){
        return databaseUser;
    }

    protected String getPassword(){
        return databasePassword;
    }
}