package Test;

import DatabaseTools.DBAccessor;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wxu13keu
 */
public class DEPRECIATED_MergeBooking {
    
    public String checkAndNotify(HttpServletRequest request, HttpServletResponse response){
        
        DBAccessor dba = new DBAccessor();
        ResultSet rs = null;
        
        String bike_id = null;
        String booking_id = null;
        String customer_email = request.getParameter("customer_email");//D.Pawsey@uea.ac.uk";
        String booking_date = request.getParameter("booking_date");//"11/11/2014";
        String booking_period = request.getParameter("booking_period");//PM";
        String check_booking_id = null;
        int counter = 0;
        String output = null;
        
        try{
            
            String sqlStatement = 
                    "SELECT booked_bike.bike_id, booking.booking_id, customer_email, booking_date, booking_period "
                    +"FROM booked_bike "
                    +"LEFT JOIN booking ON booked_bike.booking_id = booking.booking_id "
                    +"WHERE customer_email = '"+ customer_email + "' AND booking_date = '" + booking_date + "' AND booking_period = '" + booking_period + "';";
        
            //output = "\n"+sqlStatement+"\n";
        
            output ="<h1>Booking created for " + booking_date + " (" + booking_period + ")</h1>"; 
            
            rs = dba.query(sqlStatement);
        
            while(rs.next()){
                
                
                bike_id = rs.getString("bike_id");
                booking_id = rs.getString("booking_id");
                customer_email = rs.getString("customer_email");
                booking_date = rs.getString("booking_date");
                booking_period = rs.getString("booking_period");
                counter++;
                
                //output+="\n<br><br>Counter = " + counter;
            }
            //output += "<br><br>booking_date is " + booking_date;
            
            //The following SQL commands are only executed if the query returns
            //more than 1 row, i.e. there is only one booking for the customer
            //per date and booking period.
            if(counter > 1){
                
                //Delete the record with the highest booking_id from the booking
                //table, leaving only one booking per date abd time. 
                sqlStatement =
                        "DELETE FROM booking "
                        +"WHERE booking_id = (SELECT max(booking_id) FROM booking WHERE customer_email = '" + customer_email + "');";
                            
                dba.update(sqlStatement);
                
                //output +="<br><br>" + sqlStatement;
                
                //update all of the records that were associated with the now
                //deleted booking as having the same booking_id as the highest
                //booking_id valued record in the booking table, associating 
                //them with it.
                sqlStatement =
                "UPDATE booked_bike "
                +"SET booking_id = (SELECT max(booking_id) FROM booking WHERE customer_email = '" + customer_email+ "' AND booking_date = '" + booking_date + "' AND booking_period = '" + booking_period + "') "
                +"WHERE booking_id = (SELECT max(booking_id) FROM booking WHERE customer_email = '" + customer_email + "');";
                dba.update(sqlStatement);
                
                output = "<br><br>" + sqlStatement;
                
                //Run an SQL query to get the booking reference number, in order
                //display it in the page title.
                sqlStatement = 
                        "SELECT booking_id FROM booking "
                        +"WHERE customer_email = '" + customer_email 
                            +"' AND booking_date = '" + booking_date 
                            +"' AND booking_period = '" + booking_period + "';";
                
                //output += "<br><br>" + sqlStatement;
                
                rs = dba.query(sqlStatement);

                while(rs.next()){
                    //get the booking_id for the output statement, which will serve as the page title.
                    check_booking_id = rs.getString("booking_id");
                }
                
                //create an on-page notification that this is a booking update, not a new booking.
                output += "<h1>This order has been added to your booking for " + booking_date + " (" + booking_period + ")</h1> "
                        +"\nBooking reference number:<br>"
                        +"\n" + booking_id + "<br>";
            }
            else{
                
                sqlStatement = 
                        "SELECT booking_id FROM booking"
                        +"WHERE customer_email = '" + customer_email 
                            +"' AND booking_date = '" + booking_date 
                            +"' AND booking_period = '" + booking_period + "';";
                rs = dba.query(sqlStatement);
                
                while(rs.next()){
                    //get the booking_id for the output statement, which will serve as the page title.
                    check_booking_id = rs.getString("booking_id");
                }
                
                output = "<h1>Your booking has been created</h1>"
                        +"\nBooking reference number:<br>"
                        +"\n" + booking_id + "<br>";
            }
        } catch(SQLException ex){
            
        } catch(ClassNotFoundException ex){
            
        }
        return output;
    }
}