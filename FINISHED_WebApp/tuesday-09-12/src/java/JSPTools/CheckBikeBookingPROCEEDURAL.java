package JSPTools;

import DatabaseTools.DBAccessor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CheckBikeBookingPROCEEDURAL {
    
    public String processBooking (HttpServletRequest request, HttpServletResponse response){
        DBAccessor dba = new DBAccessor();
        ResultSet rs = null;
        String query = null;
        String output = "";
        ArrayList<String> BikeList = new ArrayList<>();
        ArrayList<String> ConfirmedBikeList = new ArrayList<>();
        
        ArrayList<String> CheckBikeID = new ArrayList<>();
        String check_booking_id = null;
        String check_model = null;
        String check_booking_date = null;
        String check_booking_period = null;
        String check_note = null;
        float check_price = 0;
        float check_half = 0;
        
        String customer_email = request.getParameter("customer_email");
        String booking_date = request.getParameter("booking_date");
        String booking_period = request.getParameter("booking_period");
        String amountString = request.getParameter("amount");
        int amount = Integer.parseInt(amountString);
        String note = request.getParameter("note");
        String model = request.getParameter("model");
        
        int booking_id = 0;
        

        try {
            //First check whether there are any bookings in the booking table.
            //If there are none, set the booking_id being added as 1.
            query = "SELECT max(booking_id) AS max FROM booking;\n";
            rs = dba.query(query);
                
            while(rs.next()){
                int maxBooking_id = rs.getInt("max");
                
                //If the max booking_id found in the booking table is 0, it
                //means that there are currently no bookings in the table.
                //In this case, the booking id is set to 1.
                if(maxBooking_id == 0){
                    booking_id = 1;
                }
                else{
                    booking_id = maxBooking_id;
                }
            }
            
            //Get all bike_id values of the model from table bike.
            query = "SELECT bike_id FROM bike WHERE model = '" + model + "';";
            rs = dba.query(query);
     
            //Add all bike_id attributes found to BikeList.
            while(rs.next()){
                BikeList.add(rs.getString("bike_id"));
            }
            
            //Iterate through BikeList, removing any values which correspond to
            //bike_id attributes on the booked_bike table.
            query = "SELECT bike_id FROM booked_bike";
            rs = dba.query(query);
        
            int j = BikeList.size()-1;
            while(rs.next()){
                if(BikeList.get(j).equals(rs.getString("bike_id"))){
                    BikeList.remove(j);
                    j--;
                }
            }
            
            //Add the last of the bike_id values to ConfirmedBikeList, and remove it from BikeList
            //Do this for as many times as the customer has requested bikes.
            for(int i = 0; i != amount; i++){
                ConfirmedBikeList.add(BikeList.get(BikeList.size()-1));
                BikeList.remove(BikeList.size()-1);
            }
            
            //All elements in confirmedBikeList are used to book bikes, therefore,
            //as many bikes as can be found from confirmedBikeList (corrisponding
            //to a maximum of the amount the user specified) are booked.
            //Insert the booking
            query =
                    "INSERT INTO booking"
                    +"(booking_id, customer_email, booking_date, booking_period, amount, note)"
                    +"VALUES((" + booking_id + " + 1), '" + customer_email + "', '" + booking_date + "', '" + booking_period + "', " + amount + ", '" + note + "');"   
            ;
            dba.update(query);
            
            //Add the bikes to the booking table
            for(int k = 0; k != ConfirmedBikeList.size(); k++){
                query =
                        "INSERT INTO booked_bike"
                        +"(booking_id, bike_id, note)"
                        +"VALUES((SELECT max(booking_id) FROM booking), '" + ConfirmedBikeList.get(k) + "', '" + note + "');"
                ;
                dba.update(query);
            }
            
            //Query the database for the most recently added booking from the booking and booked_bike
            //tables.                
            query =
                        "SELECT booking.booking_id, bike.bike_id, model, booking_date, booking_period, amount, booking.note, day, half "
                        +"FROM booking "
                        +"LEFT JOIN booked_bike ON booked_bike.booking_id = booking.booking_id "
                        +"LEFT JOIN bike ON bike.bike_id = booked_bike.bike_id "
                        +"LEFT JOIN rates ON rates.bike_type = bike.bike_type "
                        +"WHERE booking.booking_id = (SELECT max(booking_id) FROM booking) AND model = '" + model + "';"             
            ;
            
            rs = dba.query(query);
            
            while(rs.next()){
                
                //All bike_id attributes are added to ArrayList CheckBikeID, as
                //there may be many of these, where as each of the other
                //attributes will be the same for each record.
                CheckBikeID.add(rs.getString("bike_id"));
                
                check_booking_id = rs.getString("booking_id");
                check_model = rs.getString("model");
                check_booking_date = rs.getString("booking_date");
                check_booking_period = rs.getString("booking_period");
                check_note = rs.getString("note");
                
                //If the period is "ALL", then this text is converted to read as
                //"all day" on the booking confirmation, and the price is
                //calculated as being equal to the "day" rate multiplied by the 
                //number of bikes booked.
                //Otherwise, the amount of bikes is multiplied by the "half"
                //rate to get the price.
                if(check_booking_period.equals("ALL")){
                    check_price = rs.getFloat("day");// * amount;
                    check_booking_period = "All Day";
                }
                else{
                    check_price = rs.getFloat("half");// * amount;
                }
            }
            
            output +=
                    "\n<div>"
                    + "\n   Booking reference:"
                    + check_booking_id + "\n"
                    +"\n    <br>Bike reference numbers: ";
            for(int k = 0; k != CheckBikeID.size(); k++){
                output += "\n   <br>" + CheckBikeID.get(k);
            }
            output +=
                    "\n    <br>Bike model:"
                    +"\n    <br>" + model
                    +"\n    <br>Booking date:"
                    +"\n    <br>" + check_booking_date
                    +"\n    <br>Time:"
                    +"\n    <br>" + check_booking_period
                    +"\n    <br>Note:"
                    +"\n    <br>" + check_note
                    +"\n    <br>Cost per bike:"
                    +"\n    <br>£" + check_price
                    +"\n    <br>Total cost:"
                    +"\n    <br>£" + (check_price * amount)
                    +"\n</div>"
                    +"\n<div id=\"checkoutButton\">"
                    +"\n    <form action=\"index.jsp\"  method=\"GET\">"
                    +"\n        <input type=\"hidden\""
                    +"\n               name=\"customer_email\""
                    +"\n               value=\"" + customer_email + "\">"
                    +"\n        <button type=\"submit\""
                    +"\n            Continue browsing"
                    +"\n        </button>"
                    +"\n    </form>"
                    +"\n</div>";
            
            //If there is a discrepency between ConfirmedBikeList.size(), and the
            //amount of bikes to reserve entered by the user, it means that
            //there weren't enough free bikes to meet the users order.
            //In this scenario, as many bikes as possible of the model requested
            //are reserved, and the SuggestAlternativeBike object is called
            //to display alternatives to the user.
            if(ConfirmedBikeList.size() < amount){
                SuggestAlternativeBike sab = new SuggestAlternativeBike();
                output += "\n" + sab.makeSuggestion(request, response)
                    +"\n<div id=\"checkoutButton\">"
                    +"\n    <form action=\"index.jsp\"  method=\"GET\">"
                    +"\n        <input type=\"hidden\""
                    +"\n               name=\"customer_email\""
                    +"\n               value=\"" + customer_email + "\">"
                    +"\n        <button type=\"submit\""
                    +"\n            Continue browsing"
                    +"\n        </button>"
                    +"\n    </form>"
                    +"\n</div>";
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(CheckBikeBooking.class.getName()).log(Level.SEVERE, null, ex);
            output += "\n<div>"
                    +"\n   SQL exception occured"
                    +"\n</div>"
                    +"\n<div id=\"checkoutButton\">"
                    +"\n    <form action=\"index.jsp\"  method=\"GET\">"
                    +"\n        <input type=\"hidden\""
                    +"\n               name=\"customer_email\""
                    +"\n               value=\"" + customer_email + "\">"
                    +"\n        <button type=\"submit\""
                    +"\n            Continue browsing"
                    +"\n        </button>"
                    +"\n    </form>"
                    +"\n</div>";
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CheckBikeBooking.class.getName()).log(Level.SEVERE, null, ex);
            output += "\n<div>"
                    +"\n   Class not found"
                    +"\n</div>"
                    +"\n<div id=\"checkoutButton\">"
                    +"\n    <form action=\"index.jsp\"  method=\"GET\">"
                    +"\n        <input type=\"hidden\""
                    +"\n               name=\"customer_email\""
                    +"\n               value=\"" + customer_email + "\">"
                    +"\n        <button type=\"submit\""
                    +"\n            Continue browsing"
                    +"\n        </button>"
                    +"\n    </form>"
                    +"\n</div>";
        }           
        return output;
    }
}
