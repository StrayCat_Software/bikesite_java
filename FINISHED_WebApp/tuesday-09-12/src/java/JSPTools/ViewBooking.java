package JSPTools;

import DatabaseTools.DBAccessor;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wxu13keu
 */

public class ViewBooking {
    
    private DBAccessor dba = new DBAccessor();
    private ResultSet rs = null;
    private String sqlStatement = null;
    private String listOutput = "";
    private int numberOutput = 0;
    
    /*
    public int getNumberOfItems(HttpServletRequest request, HttpServletResponse response){
        try{
            sqlStatement = 
                    "SELECT count(booked_bike.bike_id), AS num "
                    +"FROM booked_bike "
                    +"LEFT JOIN booking ON booked_bike.booking_id = booking.booking_id "
                    +"WHERE booking.customer_email = '" + request.getParameter("customer_email") + "'; ";
            rs = dba.query(sqlStatement);
            
            while(rs.next()){
                numberOutput = rs.getInt("num");
            }
            
        } catch(SQLException ex){
            
        } catch(ClassNotFoundException ex){
            
        }
        return numberOutput;
    }
    */
    
    public String generateHTML(HttpServletRequest request, HttpServletResponse response){
        
        String bike_id = null;
        String imagepath = null;
        String description = null;
        String model = null;
        int counter = 1;
        
        try{
            sqlStatement = 
                    "SELECT booked_bike.bike_id, bike.model, description, biketype.imagepath, booking_date, booking_period "
                    +"FROM booked_bike "
                    +"LEFT JOIN bike on booked_bike.bike_id = bike.bike_id "
                    +"LEFT JOIN biketype ON bike.model = biketype.model "
                    +"LEFT JOIN booking ON booked_bike.booking_id = booking.booking_id "
                    +"WHERE booking.customer_email = 'D.Pawsey@uea.ac.uk' "
                    //    +"AND booking.booking_date = '" + booking_date 
                    //    +"' AND booking_period = '" + booking_period + "';"
                    ;
            rs = dba.query(sqlStatement);
            
            while(rs.next()){
                bike_id = rs.getString("bike_id");
                model = rs.getString("model");
                imagepath = rs.getString("imagepath");
                description = rs.getString("description");
                
                //The html output below includes a call to a JSP which handles
                //the deletion of the bike from the booked_bike table when
                //clicked. The correct bike to delete is passed to this jsp
                //by way of a hidden input containing it's bike_id value
                //within this form.
                listOutput += 
                        "\n<div>"
                        +"\n    <h4>" + counter + ") " + model + " (" + bike_id + ")</h4>"
                        +"\n    <div>"
                        +"\n        " + description
                        +"\n    </div>"
                        +"\n    <form action =\"DeleteBooking.jsp\" method=\"GET\">"
                        +"\n        <input type=\"hidden\""
                        +"\n               name=\"bike_id\""
                        +"\n               value=\"" + bike_id + "\">"
                        +"\n        <button type=\"submit\">"
                        +"\n            Remove this booking"
                        +"\n        </button>"
                        +"\n    </form>"
                        +"\n</div>";
                
                counter++;
            }
        } catch(SQLException ex){
            
        } catch(ClassNotFoundException ex){
            
        }
        return listOutput;
    }
}
