package JSPTools;

import DatabaseTools.DBAccessor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SuggestAlternativeBike {
    
    DBAccessor dba = new DBAccessor();
    
    /**
     * This method employs SQL pattern matching to search for alternative bikes
     * if the chosen bike is unavailable.
     * 
     * It will first attempt to find another bike of the same bike_type under
     * the biketype table and suggest that to them.
     * 
     * If one is not found, a fitting bike of a different bike type is searched
     * for. In this case, The pattern matching is by bike_type, searching for 
     * character strings "mens", "womens", and "child". this is because, for 
     * example, if a man has chosen a mens' mountain bike, but one is not 
     * available, a mens' hybrid bike would be a  better alternative than a 
     * womans' or childs' bike.
     * @param request
     * @param response
     * @return 
     */
    public String makeSuggestion(HttpServletRequest request, HttpServletResponse response){
        String outputHTML = "";
        try {
            String model = request.getParameter("model");
            String bike_type = "";
            
            ResultSet rs = dba.query("SELECT bike_type FROM biketype WHERE model = '" + model + "';");
            
            while(rs.next()){
                bike_type = rs.getString("bike_type");
            }
            
            //Search for alternative bikes of the same bike_type.
            rs = dba.query("SELECT model, imagepath, description FROM biketype WHERE bike_type = '" + bike_type + "';");
            
            //if any bikes are found that don't have the same model name as the
            //requested model, they are added to the outpuHTML string.
            while(rs.next()){
                if(!rs.getString("model").equals(model)){
                    outputHTML += 
                            "<!--This is a single dynamically generated bike model of the previously selected bike type-->"
                            +"<div class=\"DYNAMICALLY_GENERATED_BIKE_MODEL_LIST_ELEMENT\">"
                            +"<form action=\"BookingForm.jsp\">"
                            +"\n    <input type=\"hidden\" name=\"pageName\" value=\"HomePage.jsp\">"
                            +"\n    <input type=\"hidden\" name=\"model\" value=\"" + model + "\">"
                            +"\n    <button type=\"submit\""
                            +"\n            name=\"" + rs.getString("model") + "\""
                            +"\n            value=\"" + rs.getString("model") + "\">"
                            +"\n       " + rs.getString("model")
                            +"\n        <img src=\"" + rs.getString("imagePath") + "\">"
                            +"\n        <p>"
                            +"\n            " + rs.getString("description")
                            +"\n        </p>"
                            +"\n    </button>"
                            +"\n</div>";
                }
            }
            
            //If no other bikes of the same bike_type are found, other bikes aimed
            //at either the same gender, or also aimed at children (if the previously
            //selected model is aimed at children are searched for.
            //
            //The need to perform this search is performed by checking whether
            //the ouputHTML string has yet been populated; If it has not, the search
            //is caried out, if not, this step is skipped
            if(outputHTML.equals("")){
                
                //Check wheteher the bike_type of the selected model contains the
                //string "women":
                if(bike_type.contains("women")){
                    
                    //populate outputHTML with other bike models where bike_type
                    //contains "women":
                    rs = dba.query("SELECT model, description, imagepath FROM biketype WHERE bike_type LIKE '%women%';");
                    
                    while(rs.next()){
                        if(!rs.getString("model").equals(model)){
                          
                            outputHTML += 
                                    "<!--This is a single dynamically generated bike model of the previously selected bike type-->"
                                    +"<div class=\"DYNAMICALLY_GENERATED_BIKE_MODEL_LIST_ELEMENT\">"
                                    +"<form action=\"BookingForm.jsp\">"
                                    +"\n    <input type=\"hidden\" name=\"pageName\" value=\"HomePage.jsp\">"
                                    +"\n    <input type=\"hidden\" name=\"model\" value=\"" + model + "\">"
                                    +"\n    <button type=\"submit\""
                                    +"\n            name=\"" + rs.getString("model") + "\""
                                    +"\n            value=\"" + rs.getString("model") + "\">"
                                    +"\n       " + rs.getString("model")
                                    +"\n        <img src=\"" + rs.getString("imagePath") + "\">"
                                    +"\n        <p>"
                                    +"\n            " + rs.getString("description")
                                    +"\n        </p>"
                                    +"\n    </button>"
                                    +"\n</div>";
                        }
                    }
                }
                
                //Check wheteher the bike_type of the selected model contains the
                //string "child":
                if(bike_type.contains("child")){
                    
                    //populate outputHTML with other bike models where bike_type
                    //contains "women":
                    rs = dba.query("SELECT model, description, imagepath FROM biketype WHERE bike_type LIKE '%child%';");
                    
                    while(rs.next()){
                        if(!rs.getString("model").equals(model)){
                          
                            outputHTML += 
                                    "<!--This is a single dynamically generated bike model of the previously selected bike type-->"
                                    +"<div class=\"DYNAMICALLY_GENERATED_BIKE_MODEL_LIST_ELEMENT\">"
                                    +"<form action=\"BookingForm.jsp\">"
                                    +"\n    <input type=\"hidden\" name=\"pageName\" value=\"HomePage.jsp\">"
                                    +"\n    <input type=\"hidden\" name=\"model\" value=\"" + model + "\">"
                                    +"\n    <button type=\"submit\""
                                    +"\n            name=\"" + rs.getString("model") + "\""
                                    +"\n            value=\"" + rs.getString("model") + "\">"
                                    +"\n       " + rs.getString("model")
                                    +"\n        <img src=\"" + rs.getString("imagePath") + "\">"
                                    +"\n        <p>"
                                    +"\n            " + rs.getString("description")
                                    +"\n        </p>"
                                    +"\n    </button>"
                                    +"\n</div>";
                        }
                    }
                }
                
                //Check wheteher the bike_type of the selected model contains the
                //string "men":
                if(!bike_type.contains("women") && !bike_type.contains("child") && !bike_type.equals("tandem")){
                    
                    //populate outputHTML with other bike models where bike_type
                    //contains "women":
                    rs = dba.query("SELECT model, description, imagepath FROM biketype WHERE bike_type LIKE 'men%';");
                    
                    while(rs.next()){
                        if(!rs.getString("model").equals(model)){
                          
                            outputHTML += 
                                    "<!--This is a single dynamically generated bike model of the previously selected bike type-->"
                                    +"<div class=\"DYNAMICALLY_GENERATED_BIKE_MODEL_LIST_ELEMENT\">"
                                    +"<form action=\"BookingForm.jsp\">"
                                    +"\n    <input type=\"hidden\" name=\"pageName\" value=\"HomePage.jsp\">"
                                    +"\n    <input type=\"hidden\" name=\"model\" value=\"" + model + "\">"
                                    +"\n    <button type=\"submit\""
                                    +"\n            name=\"" + rs.getString("model") + "\""
                                    +"\n            value=\"" + rs.getString("model") + "\">"
                                    +"\n       " + rs.getString("model")
                                    +"\n        <img src=\"" + rs.getString("imagePath") + "\">"
                                    +"\n        <p>"
                                    +"\n            " + rs.getString("description")
                                    +"\n        </p>"
                                    +"\n    </button>"
                                    +"\n</div>";
                        }
                    }
                }
                
                //For the sake of completeness, a simmilar process is included for tandem bikes
                if(bike_type.equals("tandem")){
                    
                    //populate outputHTML with other bike models where bike_type
                    //contains "women":
                    rs = dba.query("SELECT model, description, imagepath FROM biketype WHERE bike_type LIKE 'tandem';");
                    
                    while(rs.next()){
                        if(!rs.getString("model").equals(model)){
                          
                            outputHTML += 
                                    "<!--This is a single dynamically generated bike model of the previously selected bike type-->"
                                    +"<div class=\"DYNAMICALLY_GENERATED_BIKE_MODEL_LIST_ELEMENT\">"
                                    +"<form action=\"BookingForm.jsp\">"
                                    +"\n    <input type=\"hidden\" name=\"pageName\" value=\"HomePage.jsp\">"
                                    +"\n    <input type=\"hidden\" name=\"model\" value=\"" + model + "\">"
                                    +"\n    <button type=\"submit\""
                                    +"\n            name=\"" + rs.getString("model") + "\""
                                    +"\n            value=\"" + rs.getString("model") + "\">"
                                    +"\n       " + rs.getString("model")
                                    +"\n        <img src=\"" + rs.getString("imagePath") + "\">"
                                    +"\n        <p>"
                                    +"\n            " + rs.getString("description")
                                    +"\n        </p>"
                                    +"\n    </button>"
                                    +"\n</div>";
                        }
                    }
                }
                
                //If no other bikes are available, generate an appology message.
                else {
                    outputHTML +=
                            "<h1>We're sorry...</h1>"
                            +"\n<p>"
                            +"\n    It seems that we don't have any of that model, or anything simmilar available.<br>"
                            +"\n    Please check back regularly for other bikes and offers."
                            +"\n</p>";
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(SuggestAlternativeBike.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SuggestAlternativeBike.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "<form action=\"BookingForm.jsp\" method=\"GET\">\n" + outputHTML +"\n</form>";
        
    }
}
              