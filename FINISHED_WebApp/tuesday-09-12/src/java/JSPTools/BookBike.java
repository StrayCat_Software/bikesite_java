/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JSPTools;

import DatabaseTools.DBAccessor;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BookBike {
    public String book(
            HttpServletRequest request, HttpServletResponse response,
            DBAccessor dba, String bookingID, String customer_email, 
            String booking_date, String booking_period, int amount, 
            String note, String output) 
            throws SQLException, ClassNotFoundException{
        dba.update(
                        "INSERT INTO booking"
                        +"(booking_id, customer_email, booking_date, booking_period, amount, note)"
                        +"VALUES((" + bookingID + " + 1), '" + customer_email + "', '" + booking_date + "', '" + booking_period + "', " + amount + ", '" + note + "');"
                );
                
                output += 
                        "<br><strong>INSERT INTO booking"
                        +"<br>(booking_id, customer_email, booking_date, booking_period, amount, note)"
                        +"<br>VALUES((" + bookingID + " + 1), '" + customer_email + "', '" + booking_date + "', '" + booking_period + "', " + amount + ", '" + note + "');<strong><br>";
                
                dba.update(
                        "INSERT INTO booked_bike"
                        +"(booking_id, bike_id, note)"
                        +"VALUES((SELECT max(booking_id) FROM booking), '" + bikeBike_id + "', '" + note + "');"
                );

                output += 
                        "<br><strong>INSERT INTO booked_bike"
                        +"<br>(booking_id, bike_id, note)"
                        +"<br>VALUES((SELECT max(booking_id) FROM booking), '" + bikeBike_id + "', '" + note + "');<strong><br>";
                        
                //Query the database for the most recently added booking from the booking and booked_bike
                //tables.
                ResultSet rs3 = dba.query(
                        "SELECT booking.booking_id, bike.bike_id, model, booking_date, booking_period, amount, booking.note, day "
                        +"FROM booking "
                        +"LEFT JOIN booked_bike ON booked_bike.booking_id = booking.booking_id "
                        +"LEFT JOIN bike ON bike.bike_id = booked_bike.bike_id "
                        +"LEFT JOIN rates ON rates.bike_type = bike.bike_type "
                        +"WHERE booking.booking_id = (SELECT max(booking_id) FROM booking);"
                );

                output += 
                        "<br><strong>SELECT booking.booking_id, bike.bike_id, model, booking_date, booking_period, amount, booking.note, day "
                        +"<br>FROM booking "
                        +"<br>LEFT JOIN booked_bike ON booked_bike.booking_id = booking.booking_id "
                        +"<br>LEFT JOIN bike ON bike.bike_id = booked_bike.bike_id "
                        +"<br>LEFT JOIN rates ON rates.bike_type = bike.bike_type "
                        +"<br>WHERE booking.booking_id = (SELECT max(booking_id) FROM booking);<strong><br>";

                while(rs3.next()){
                    check_booking_id = rs3.getString("booking_id");
                    check_bike_id = rs3.getString("bike_id");
                    check_model = rs3.getString("model");
                    check_booking_date = rs3.getString("booking_date");
                    check_booking_period = rs3.getString("booking_period");
                    check_amount = rs3.getFloat("amount");
                    check_note = rs3.getString("note");
                    check_price = rs3.getFloat("day") * check_amount;
                }
                
                //If the the returned period is all day, change it so that it is
                //more human-readable. Otherwise, change the price so that they
                //are only charged for a half day.
                if(check_booking_period.equals("ALL")){
                    check_booking_period = "All day";
                }
                /*else{
                    ResultSet rs4 = dba.query("SELECT half FROM rates WHERE bike_type = "
                            + "(SELECT bike_type FROM bike WHERE bike_id = '" + check_bike_id + "');");
                    while(rs4.next()){
                        check_price = rs4.getFloat("half") * check_amount;
                    }
                }*/
               
                //Generate output HTML.
                output += 
                        "<h1>Your booking of " + model + " has been processed</h1>"
                        +"\n<h2>Order details:</h2>"
            
                        //Booking details table, displaying the order placed by the user in
                        //a recepit style list.
                        +"\n<div>"
                        +"\n     <div>"
                        +"\n        booking reference number:"
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n        " + check_booking_id
                        +"\n     </div>"
                        +"\n     <div>" 
                        +"\n        Bike reference number:" 
                        +"\n     </div>" 
                        +"\n     <div>" 
                        +"\n       " + check_bike_id 
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n        Bike model:"
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n       " + check_model
                        +"\n     </div>"
                        +"\n     <div>" 
                        +"\n         Date:"
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n        " + check_booking_date
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n         Time:"
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n       " + check_booking_period
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n         Number of this model booked:"
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n       " + check_amount
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n         Price:"
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n       £" + check_price
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n         Notes"
                        +"\n     </div>"
                        +"\n     <div>"
                        +"\n       " + check_note
                        +"\n     </div>"
                        +"\n</div>"
                        +"\n\n<!--Continue browsing button directs the user back to the home page -->"
                        +"\n<div id=\"ContinueBrowsingButton\">"
                        +"\n    <form action=\"index.jsp\"  method=\"GET\">"
                        +"\n        <input type=\"hidden\""
                        +"\n               name=\"customer_email\""
                        +"\n               value=\"" + request.getParameter("customer_email") + "\">"
                        +"\n        <button type=\"submit\" value=\"continue browsing\">"
                        +"\n            Continue browsing"
                        +"\n        </input>"
                        +"\n    </form>"
                        +"\n</div>"
                ;
    }
}
