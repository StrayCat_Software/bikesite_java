<%-- 
    Document   : BookingForm
    Created on : 23-Nov-2014, 19:29:32
    Author     : Dan
--%>

<%
String model = request.getParameter("model");
String customer_email = request.getParameter("customer_email");
String emailLabel = "";
String emailInputType = "hidden";

//If the customer_email parameter retrieved from the HttpServletRequest object is
//null (i.e. the user hasn't signed in), they can add there email address here.
//Their email address being null causes the hidden field which would have taken
//their email as passed between the JSP forms and converts it into a text field,
//requesting their email
if (customer_email.equals("null") || customer_email.equals(null) || customer_email.equals("") || customer_email.isEmpty()){
    customer_email = "";
    emailLabel = "Email Address: ";
    emailInputType = "email";
}

%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Booking <%=model%></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1>Booking of <%=model%></h1>
        
        <form action="ProcessBooking.jsp" method="GET">
            
            <!--The hidden field adds the users email to the booking. If the user
            is not signed in, then this becomes an email text entry field.-->
            <%=emailLabel%>
            <input type="<%=emailInputType%>" 
                   name="customer_email"
                   value="<%=customer_email%>"><br>
            
            <!--The bike model is passed as a hidden variable, retrieved from the previous page-->
            <input type="hidden" name="model" value="<%=model%>">
            
            Date:    
            <input type="date"    
                   name ="booking_date"><br>    
      
            Period:
            <input type="radio" name="booking_period" value="ALL">All day
            <input type="radio" name="booking_period" value="AM" checked>Morning
            <input type="radio" name="booking_period" value="PM" checked>Afternoon<br>
            
            How many?:
            <input type="number"
                   name="amount"><br>
            
            Note:    
            <input type="text"    
                   name="note"><br>
                
            <input type ="submit">
                
            <input type="reset">
        </form>
        <a href="HomePage.jsp">JUMP TO HOME PAGE</a>
    </body>
</html>