<%-- 
    Document   : Sign-in_Register
    Created on : 30-Nov-2014, 15:12:12
    Author     : Dan
--%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="DatabaseTools.DBAccessor"%>

<%
    //The previous page's name is called, so that the on page form can redirect
    //back to it once the user has signed in
    String previousPage = request.getParameter("pageName");
    String errorMessage = "";
    
    //This checks to see if the failedAttempt variable with value "true" has been
    //passed from Servlet_SignIn. Finding this means that the log in has been failed
    //at least once, and that the error message telling the user that the log in has
    //failed should be displayed using a JavaBean.
    try{
        String failedAttempt = (String)request.getAttribute("failedAttempt");
        if(failedAttempt.equals("true")){
            errorMessage = "Incorrect email address or password.";
            previousPage = request.getParameter("previousPage");
        }
    } catch (NullPointerException ex){
        //Exception handling
    }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sign In | Anglian Bike Hire</title>
    </head>
    <body>
        <h1>Sign in</h1>
        <form class="loginPanel" action="Servlet_SignIn">
            Email address: 
            <input type="email"
                   name ="customer_email"><br>
            
            Password:
            <input type="password"
                   name="password"><br>
            
            <input id="Button_SignIn"
                   type="submit" 
                   value="Sign in">
            
            <input type="hidden"
                   name="previousPage"
                   value="<%=previousPage%>">
        </form>
        
        <form action="Register.jsp" method="GET">
               
            <input id="Button_Register"
                   type="submit"
                   value="Register">
        </form>
        
        <!--This is where "Invalid username" message will appear-->
        <div id="loginFailedText">
            <%=errorMessage%>
        </div>
    </body>
</html>