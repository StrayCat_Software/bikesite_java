<%-- 
    Document   : SelectBookingDateAndTime
    Created on : 09-Dec-2014, 14:51:01
    Author     : wxu13keu
--%>
<%@page import="java.sql.ResultSet"%>
<%@page import="DatabaseTools.DBAccessor"%>
<%
    //This page allows the user to select the date and time that they want to make
    //a payment for, allowing them to handle multiple bookings.
    
    DBAccessor dba = new DBAccessor();
    ResultSet rs = null;
    String booking_date = null;
    String booking_period = null;
    String customer_email = request.getParameter("customer_email");
    String onPage = "";
    
    String sqlStatement = 
            "SELECT DISTINCT booking_date, booking_period "
            +"FROM booking "
            //+"WHERE customer_email = '" + customer_email + "';";
            +"WHERE customer_email = 'D.Pawsey@uea.ac.uk';";
    rs = dba.query(sqlStatement);
    
    while(rs.next()){
        booking_date = rs.getString("booking_date");
        booking_period = rs.getString("booking_period");
        
        onPage +=
                "booking date:" + booking_date
                +"booking_period: " + booking_period;
                
        
        onPage += 
                "\n<div>"
                +"\n    <form action=\"PaymentForm.jsp\" method=\"GET\">"
                +"\n        <input type=\"hidden\""
                +"\n                name=\"customer_email"
                +"\n                value=\"" + request.getParameter("customer_email") +"\">" //Is customer email persistent? This may be unnessicary...
                +"\n        <input type=\"hidden\""
                +"\n                name=\"booking_date\""
                +"\n                value=\"" + booking_date + "\">"
                +"\n        <input type=\"hidden\""
                +"\n                name=\"booking_period\""
                +"\n                value=\"" + booking_period + "\""
                +"\n        <input type=\"submit\" value=\"FUFF\">"
                +"\n        <button type=\"submit\">"
                +"\n            " + booking_date + " (" + booking_period + ")"
                +"\n        </button>"
                +"\n    </form>"
                +"\n<div>";
    }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--This page allows the user to select the date and time that they want to make
    a payment for, allowing them to handle multiple bookings-->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%=sqlStatement%>
        <h1>Select the booking you would like to pay for:</h1>
        <%=onPage%>
    </body>
</html>
