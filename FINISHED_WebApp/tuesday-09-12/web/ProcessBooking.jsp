<%-- 
    Document   : ProcessBooking
    Created on : 05-Dec-2014, 21:47:54
    Author     : Dan
--%>

<%@page import="JSPTools.CheckBikeBookingPROCEEDURAL"%>
<%@page import="UserAccountTools.UserAccountFormGenerator"%>
<%@page import="JSPTools.CheckBikeBooking"%>

<%
    //CheckBikeBooking cbb = new CheckBikeBooking(request, response);
    CheckBikeBookingPROCEEDURAL cbb = new CheckBikeBookingPROCEEDURAL();
    UserAccountFormGenerator uafg = new UserAccountFormGenerator();
    //MergeBooking mb = new MergeBooking();
            
    String pageName = "ProcessBooking.jsp";
    
    /*String booking_id = request.getParameter("booking_id");
    String booking_date = request.getParameter("booking_date");
    String booking_period = request.getParameter("booking_period");

    //If a previous booking has been made on the account, and
    if (booking_id.equals("null") || booking_id.equals(null) || booking_id.equals("") || booking_id.isEmpty()){
        booking_id = "";
    }*/
    
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%="SELECT booking_date, booking_period FROM booking WHERE customer_email = '" + request.getParameter("customer_email") + "';"%>
        <%=uafg.generateHTML(request, response, pageName)%>
        
        <%=cbb.processBooking(request, response)%>
        <%--<%=mb.checkAndNotify(request, response)%>--%>
        
        <!--Proceed to checkout button-->
        <div id="checkoutButton">
            <form action="SelectBookingDateAndTime.jsp"  method="GET">
                <input type="hidden"
                       class="customer_email"
                       value="<%=request.getParameter("customer_email")%>">
                <button type="submit"
                        name="ProceedToCheckout">
                    Proceed to Checkout
                </button>
            </form>
        </div>
    </body>
</html>
