<%-- 
    Document   : PaymentForm
    Created on : 08-Dec-2014, 11:53:46
    Author     : wxu13keu
--%>
<%@page import="JSPTools.ViewBooking"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="DatabaseTools.DBAccessor"%>
<%
    ViewBooking vb = new ViewBooking();
    
    //This JSP will check the customer table to see if the user has a card 
    //number. If they do not, it will generate a form which will request their 
    //card details
    String generateForm = "";
    String cardno = "";
    
    try{
        DBAccessor dba = new DBAccessor();
    
        String query = "SELECT cardno FROM customer WHERE customer_email = '" 
                + request.getParameter("customer_email") + "';";
    
        ResultSet rs = dba.query(query);
    
        while(rs.next()){
            cardno = request.getParameter("cardno");
        }
        
        if(cardno.isEmpty() || cardno.equals("") || cardno.equals(null)){
            generateForm += 
                    "Card number"
                    +"\n<input type=\"number\""
                    +"\n    name=\"card_number\">"
                    +"\nCard expiry (month)"
                    +"\n<input type=\"number\""
                    +"\n       name=\"card_exp_month\">"
                    +"\nCard expiry (year)"
                    +"\n<input type=\"number\""
                    +"\n       name=\"card_exp_year\">"
                    +"\nBilling address"
                    +"\n<input type=\"text\""
                    +"\n       name=\"Billing_Address\">"
                    +"\nCard type"
                    +"\n<input type=\"radio\""
                    +"\n       name=\"card_type\""
                    +"\n       value=\"V\" "
                    +"\n       checked>Visa"
                    +"\n<input type=\"radio\"" 
                    +"\n       name=\"card_type\""
                    +"\n       value=\"MC\">MasterCard"
                    +"\n<input type=\"radio\"" 
                    +"\n       name=\"card_type\" "
                    +"\n       value=\"AE\">American Express<br>";
        }
    } catch(SQLException ex){
        
    } catch(ClassNotFoundException ex){
               
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Checkout | Anglian Bike Hire</title>
    </head>
    <body>
        <h1>Checkout</h1>
        <div>
            <h2>Your basket</h2>
            <%=vb.generateHTML(request, response)%>
        </div>
        <div>
            <h2>Payment details</h2>
            <form action="ProcessPayment.jsp" method="GET">
                <input type="hidden"
                       name="customer_email"
                       value="<%=request.getParameter("customer_email")%>"
                <<%=generateForm%>
                <button type="submit">
                    Make payment
                </button>
            </form>
        </div>
        <div>
            <form action="index.jsp" method="GET">
                <input type="hidden"
                       name="customer_email"
                       value="<%=request.getParameter("customer_email")%>">
                <button type="submit">
                    Continue browsing
                </button>
            </form>
        </div>
    </body>
</html>
