<%-- 
    Document   : DeleteBooking
    Created on : 09-Dec-2014, 14:06:47
    Author     : wxu13keu
--%>
<%@page import="java.sql.SQLException"%>
<%@page import="DatabaseTools.DBAccessor"%>
<%
    //This page is intended specifically for the handling of the deletion of a
    //bike, and then redirecting the user back to PaymentForm.jsp
    
    DBAccessor dba = new DBAccessor();
    ResultSet rs = null;
    String bike_id = request.getParameter("bike_id");
    String sqlStatement = null;
    String onPage = "Redirecting...";
    
    
    try{
        
        
        sqlStatement = 
                "SELECT booking_id "
                +"FROM booking "
                +"LEFT JOIN booked_bike ON booking.bike_id = booked_bike.bike_id "
                +"WHERE bike.bike_id = '" + bike_id + "';";
        
        rs = dba.query(sqlStatement);
        
        while(rs.next()){
            String booking_id = rs.getString("booking_id");
            
            sqlStatement = "DELETE FROM booked_bike WHERE booking_id = '" + booking_id + "';";
        }
        
                
        sqlStatement = 
                "DELETE FROM booked_bike WHERE bike_id = '" + bike_id + "';";
        dba.update(sqlStatement);
        
        request.getRequestDispatcher("/PaymentForm.jsp").forward(request, response);
        
        
    } catch (SQLException ex){
        onPage = "SQL Exception occured.";
    }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Redirecting | Anglian Bike Hire</title>
    </head>
    <body>
        <h1><%=onPage%></h1>
    </body>
</html>
