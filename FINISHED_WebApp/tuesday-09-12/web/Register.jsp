<%-- 
    Document   : Register
    Created on : 30-Nov-2014, 16:18:16
    Author     : Dan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register</title>
    </head>
    <body>
        <h1>Register</h1>
        
        <form class="loginPanel" action="Servlet_AddCustomer" method="GET">
            
            Email address: 
            <input type="email"
                   name="customer_email"><br>
            
            Password:
            <input type="password"
                   name="password"><br>
            
            Confirm password:
            <input type="password"
                   name="confirm_password">

            <br>
            
            First name:
            <input type="text"
                   name="first_name">
            
            Last name:
            <input type="text"
                   name="last_name"><br>
            
            <div>Please enter payment details below:</div><br>
            
            Billing address:
            <input type="text"
                   name="billing_address"><br>
            
            Card number:
            <input type="numeric"
                   name="cardno"><br>
            
            Expiry date:
            <input type="numeric"
                   name="card_exp"><br>
            
            Card type:
            <input type="radio" name="card_type" value="V" checked>Visa
            <input type="radio" name="card_type" value="MC">MasterCard
            <input type="radio" name="card_type" value="AE">American Express<br>
            
            <input type="submit"
                   value="Register">
            
        </form>
    </body>
</html>
