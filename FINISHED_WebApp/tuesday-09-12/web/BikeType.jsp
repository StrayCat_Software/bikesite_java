<%-- 
    Document   : newjsp
    Created on : 24-Nov-2014, 15:48:33
    Author     : wxu13keu
--%>

<%@page import="UserAccountTools.UserAccountFormGenerator"%>
<%@page import="javax.servlet.http.HttpServletRequest"%>
<%@page import="DatabaseTools.DBAccessor"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>

<%
    /**
    * The code below is used to check if a user is signed in, and generate the 
    * appropriate buttons accordingly.
    */
    UserAccountFormGenerator uafg = new UserAccountFormGenerator();
    String NavPanelContent = uafg.generateHTML(request, response, "BikeType.jsp");
    
    
    ResultSet rs = null;
    String model = "";
    String imagePath = "";
    String description = "";
    String page_bike_type = request.getParameter("bike_type");
    
    //This is the page that will be called by the bike buttons on this page. If
    //the user is signed in, that will be the booking form, otherwise, if there
    //is a null value for email in the HttpServletRequest object (i.e. the user
    //has not signed in), it will be the sign in form.
    String callPage = "BookingForm.jsp";
    
    String customer_email = null;
    //try{
        customer_email = request.getParameter("customer_email");
        /*if(customer_email.equals(null) || customer_email.equals("") || customer_email.equals("null")){
                callPage = "SignIn.jsp";
        }
    }catch(NullPointerException ex){
        callPage = "SignIn.jsp";
    }*/

    //public String generateEntries(){
        String query = "SELECT * FROM biketype WHERE bike_type = '" + page_bike_type + "';";
        String entries = "";

        try {
            DBAccessor dba = new DBAccessor();
            rs = dba.query(query);
            
            //The following will retrieve the model and description of each bike
            //from the result set, and add them to a <div> tag HTML page element
            //with the class label "bike_type", allowing these elements to be
            //styled as individual on-page elements, but in a uniform style.
            //Furthermore, the name will be presented as a hyperlink. This will
            //All be generated as a string, called as a JavaBean on the page.
            int i = 0;
            while(rs.next()){
                /*String bike_type = rs.getString("bike_type");
                model = rs.getString("model");
                String modelLink = "<a href=\"" + model + ".html\">" + model + "</a><br>";
                //imagePath = "<a href=\"" + rs.getString("model") + "\".html><img src=\"" + rs.getString("imagePath") + "\"></a>";
                imagePath = "img src=\"resources\\images\\biketype\\childs_mtb.jpg\">";
                imagePath = "<img src=\"resources/images/biketype/" + rs.getString("imagePath") + ".jpg\">" + rs.getString("imagePath") + "</a><br>";
                description = rs.getString("description");*/
                
                
                String bike_type = rs.getString("bike_type");
                model = rs.getString("model");
                imagePath = rs.getString("imagePath");
                description = rs.getString("description");
                
                //For each bike, a form and submit button are included, which 
                //serve both as a link to BookingForm.jsp, and a means of passing
                //the bike model to that form.
                entries += 
                        "<!--This is a single dynamically generated bike model of the previously selected bike type-->"
                        +"<div class=\"DYNAMICALLY_GENERATED_BIKE_MODEL_LIST_ELEMENT\">"
                            +"<form action=\"" + callPage + "\">"
                                +"<input type=\"hidden\" name=\"pageName\" value=\"HomePage.jsp\">"
                                +"<input type=\"hidden\" name=\"model\" value=\"" + model + "\">"
                                + "<button type=\"submit\" name=\"" + rs.getString("bike_type") + "\" value=\"" + rs.getString("bike_type") + "\">"
                                    +"<h2>" + rs.getString("model") + "</h2>"
                                    +"<img src=\"" + rs.getString("imagePath") + "\">"
                                    +"<p>" + rs.getString("description") + "</p>"
                                    +"<p>Click to book a " + rs.getString("model") + ".</p>"
                                + "</button>"
                                + "<input type=\"hidden\" name=\"customer_email\" value=\"" + customer_email + "\">"
                            +"</form>"  
                        +"</div>"
                        ;
                
                rs.next();
                i++;               
            }
        } catch (SQLException ex) {
            //TODO exception handling
        } catch (ClassNotFoundException ex) {
            //TODO exception handling
        }
        //return entries;
    //}
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    
    <body>
        <div class="NavPanel">
            
            <!--Below is the nav bar, which can contain whatever. For now, it
            contains dynamically generated sign in, out etc buttons-->
            <nav>
                <%=NavPanelContent%>
            </nav>
        </div>
        
        <!--Below is a dynamically generated list of available bikes of the type
        selected on the previous page-->
        <div class="ContentTable">
           <%=entries%>
        </div>
        
    </body>
</html>