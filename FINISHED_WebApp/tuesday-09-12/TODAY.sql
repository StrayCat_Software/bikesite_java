﻿
--JAVA: if counter is greater than 1.

DELETE FROM booking WHERE booking_id = (SELECT max(booking_id) FROM booking WHERE customer_email = 'D.Pawsey@uea.ac.uk');

UPDATE booked_bike
SET booking_id = (
		SELECT max(booked_bike.booking_id) 
		FROM booked_bike 
		LEFT JOIN booking ON booking.booking_id = booked_bike.booking_id 
		WHERE customer_email = 'D.Pawsey@uea.ac.uk' AND booking_date = '11/11/2014' AND booking_period = 'PM'
		)
WHERE booking_id = (
		SELECT max(booked_bike.booking_id) 
		FROM booked_bike
		LEFT JOIN booking ON booking.booking_id = booked_bike.booking_id
		WHERE customer_email = 'D.Pawsey@uea.ac.uk');

SELECT * FROM booked_bike;