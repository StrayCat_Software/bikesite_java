﻿/**
* Modifications to the database were made in the development of this web
* application. To follow are a list of table DDL declairations taken from
* PGAdmin III, reflecting those changes.
*/

-- Table: bike

-- DROP TABLE bike;

CREATE TABLE bike
(
  bike_id character(6) NOT NULL,
  model character varying(32),
  bike_type character varying(16),
  imagepath character varying(200),
  damage character varying(100),
  CONSTRAINT bike_pkey PRIMARY KEY (bike_id)
)

-- Table: bikeservice

-- DROP TABLE bikeservice;

/**
 * Column damage is added so that the technician can log damage to an
 * individual bike. This is also recorded into the note column of the
 * bikeservice table for the bikeservice associated with that bike.
*/

CREATE TABLE bikeservice
(
  bike_id character(6) NOT NULL,
  service_start date NOT NULL,
  service_end date,
  note character varying(200),
  CONSTRAINT bikeservice_pkey PRIMARY KEY (bike_id, service_start)
)

-- Table: biketype

-- DROP TABLE biketype;

/**
* imagepath provides a file path to an image to be displayed on Biketype.jsp
*
* thumbnail provides a sile path to a thumbnail imaeg to be displayed on index.jsp
*
* imagepath_model was intended to provide image source file path links for individual
* bike models displayed on BikeType.jsp, but was never properly implemented due to time
* constraints limiting our ability to search for model-specific images.
*
* Formal_biketype_name is called for the bike type buttons on image.jsp, so as to display
* A more human-readable name for each one above each button.
*/
CREATE TABLE biketype
(
  model character varying(32) NOT NULL,
  bike_type character varying(16),
  description character varying(1000),
  imagepath character varying(200),
  imagepath_model character varying(200),
  formal_biketype_name character varying(100),
  thumbnail character varying(100),
  CONSTRAINT biketype_pkey PRIMARY KEY (model)
)


-- Table: booked_bike

-- DROP TABLE booked_bike;

CREATE TABLE booked_bike
(
  booking_id integer NOT NULL,
  bike_id character(6) NOT NULL,
  note character varying(200),
  CONSTRAINT booked_bike_pkey PRIMARY KEY (booking_id, bike_id)
)

-- Table: booking

-- DROP TABLE booking;

/**
* Boolean attribute was added to allow flagging of bookings as paid,
* however, time constraints prevented it's proper usage on the web 
* application
*/

CREATE TABLE booking
(
  booking_id integer NOT NULL,
  customer_email character varying(48),
  booking_date date NOT NULL,
  booking_period character(3) NOT NULL,
  amount numeric(6,2),
  note character varying(200),
  paid boolean,
  CONSTRAINT booking_pkey PRIMARY KEY (booking_id)
)

-- Table: customer

-- DROP TABLE customer;

/**
* Attribute staff was added so that customers can be treated as site
* users, and some of them can log in to access manager and technician
* pages. This attribute is set as 'CUSTOMER' when a customer registers
* using register.jsp to add their user and card details to the customer.
* 
* SignIn.jsp uses class UserAccountFormGenerator.java, which checks this 
* table for attributes 'CUSTOMER', 'MANAGER' or 'TECHNICIAN' in the field, 
* which it uses to generate buttons which redirect the user (with their 
* email address passed via the HttpServletRequest object) to the relevant
* pages for them; SelectBookingDateAndTime.jsp (order viewing),
* Adminstrator's home page (ManagerPage.jsp) or Technician's home page
* (TechnicianPage.jsp), shown on index.jsp.
*
* password provides a string the user must enter in SignIn.jsp to view
* previous bookings (for customers), or the technician pages or administrator
* pages. For customers, this can be added through Register.jsp, but for
* technicians and administrators, it is assumed that a database technician
* or specialised software client would be used to add them to the database,
* given their access to specialised information.
*/

CREATE TABLE customer
(
  customer_name character varying(48) NOT NULL,
  customer_email character varying(48) NOT NULL,
  billing_address character varying(200),
  card_type character varying(8),
  card_exp character varying(5),
  cardno character varying(16),
  password character varying(20),
  staff character varying(20),
  CONSTRAINT customer_pkey PRIMARY KEY (customer_email)
)

-- Table: rates

-- DROP TABLE rates;

CREATE TABLE rates
(
  bike_type character varying(32) NOT NULL,
  day numeric(6,2),
  half numeric(6,2),
  CONSTRAINT rates_pkey PRIMARY KEY (bike_type)
)

/**
* To follow is Dan Smith's dummy data, with a few alterations
* at the end. This should leave the database in the state it
* was by the end of the Friday presentation.
*/

insert into customer values ('Andrew Spalding ', 'Andrew.Spalding@yahoo.com', '30 Castle Precincts, Acle NR13 2AY', 'V', '12/14', '6101225280422702');
insert into customer values ('Ann Hinchcliffe ', 'Ann.Hinchcliffe@yahoo.com', '81 New Road, Acle NR13 7GH', 'V', '10/16', '8948106927123585');
insert into customer values ('Carol Pearson ', 'Carol.Pearson@gmail.com', '40 St Mark''s Road, London E23 5LW', 'V', '9/15', '9183767246807868');
insert into customer values ('Cherrill Fisher ', 'Cherrill.Fisher@yahoo.com', '14 Bernard Road, Norwich NR6 7LL', 'MC', '5/16', '5491775659602051');
insert into customer values ('Cherry Smith ', 'Cherry.Smith@yahoo.com', '15 The Hurst, Birmingham B17 5AH', 'V', '8/14', '5055718166088399');
insert into customer values ('Chris Holmes ', 'Chris.Holmes@gmail.com', '65 Sandridge Road St Albans AL1 8YH', 'MC', '8/14', '3758532795729252');
insert into customer values ('Craig Naylor ', 'Craig.Naylor@yahoo.com', '15 Park Street, London NW5 3RG', 'MC', '2/16', '7897976289709324');
insert into customer values ('Danny Keenan ', 'Danny.Keenan@gmail.com', '8 Clifford Street York Y2 7JV', 'V', '5/15', '3296158946356776');
insert into customer values ('Daphne Mann ', 'Daphne.Mann@yahoo.com', '10 Victoria Street, Norwich NR1 7RD', 'V', '5/16', '3041722074855055');
insert into customer values ('Darren Lee ', 'Darren.Lee@gmail.com', '40 Helena Road, Diss IP22 8UK', 'A', '1/16', '3206315976614144');
insert into customer values ('Debbie Davison ', 'Debbie.Davison@yahoo.com', '65 Windsor Road, Reading RG2 8AX', 'MC', '8/14', '9313717884645601');
insert into customer values ('Deborah Herring ', 'Deborah.Herring@gmail.com', '5 Peascod Street, Sheffield S5 8HB', 'MC', '11/14', '4595864641759032');
insert into customer values ('Denis Gower ', 'Denise.Gower@gmail.com', '5 Alma Road, Kings Lynn PE30 2 FN', 'V', '9/15', '3258301785786885');
insert into customer values ('Denise Follows ', 'Denise.Follows@gmail.com', '15 Frogmore Park, London NW4 3AQ', 'V', '2/15', '9124628930869373');
insert into customer values ('Denise Hunt ', 'Denise.Hunt@yahoo.com', '5 Spinners Walk, Ipswich IP1 3JN', 'MC', '4/15', '7824605040712906');
insert into customer values ('Dominic Earl ', 'Dominic.Earl@yahoo.com', '15 Bexley Street, Diss IP22 2ES', 'MC', '5/14', '9771589265721184');
insert into customer values ('Emma Seager ', 'Emma.Seager@gmail.com', '80 Devereux Road, Holt NR25 3EE', 'V', '11/15', '2571819110792660');
insert into customer values ('Ian Haden ', 'Ian.Haden@yahoo.com', '10 Frogmore, Bristol BS16 4AF', 'MC', '11/14', '3656758666526411');
insert into customer values ('Jennifer Gray ', 'Jennifer.Gray@yahoo.com', '10 Bexley Terrace, Great Yarmouth NR30 5TZ', 'V', '11/14', '7660503113099615');
insert into customer values ('Jodie Anderson ', 'Jodie.Anderson@yahoo.com', '5 Bridgewater Terrace, Little Melton NR18 4JB', 'MC', '4/15', '9093490787439584');
insert into customer values ('Joe Page ', 'Joe.Page@yahoo.com', '5 Spinners Walk, London N17 2GC', 'A', '4/16', '1231336923428654');
insert into customer values ('Joe Prentice ', 'Joe.Prentice@yahoo.com', '40 Helena Road, Aylsham NR11 2YB', 'MC', '2/14', '1608794797959268');
insert into customer values ('John Jones ', 'John.Jones@yahoo.com', '5 Dorset Road, Wymondham NR18 6QT', 'V', '12/15', '1956167460056484');
insert into customer values ('John Moore ', 'John.Moore@yahoo.com', '65 Arthur Road, Diss IP22 7YH', 'V', '4/16', '1460998871211523');
insert into customer values ('Jolanta Balaz ', 'Jolanta.Balaz@gmail.com', '25 Bachelors'' Acre, Cromer NR27 9JY', 'V', '9/14', '9645108040255342');
insert into customer values ('Joseph Zafor Kahn ', 'Joseph.ZaforKahn@gmail.com', '15 Park Avenue, Norwich NR7 1BL', 'MC', '7/16', '9263423347679293');
insert into customer values ('Keith Roberts ', 'Keith.Roberts@yahoo.com', '105 Oxford Road, Hethersett NR18 7UX', 'V', '8/16', '8451200084036386');
insert into customer values ('Linda Satt ', 'Linda.Satt@gmail.com', '5 Thames Side, Reading RG4 8KE', 'MC', '12/16', '3002553128235845');
insert into customer values ('Linda Smith ', 'Linda.Smith@gmail.com', '65 Barton Lodge Road, Birmingham B12 1KC', 'MC', '6/16', '4316155679735479');
insert into customer values ('Maria Deacon ', 'Maria.Deacon@yahoo.com', '20 Clewer Hill Road, Great Yarmouth NR30 2EV', 'MC', '10/16', '9377665278133681');
insert into customer values ('Mathew Ireland ', 'Mathew.Ireland@yahoo.com', '5 Love Lane, Ipswich IP1 4ED', 'V', '6/16', '5350948440312389');
insert into customer values ('Michael Harrison ', 'Michael.Harrison@yahoo.com', '5 Spinners Walk, Aylsham NR11 5TD', 'MC', '3/14', '1361876228505197');
insert into customer values ('Michelle Stafford ', 'Michelle.Stafford@gmail.com', '15 Home Park, Hethersett NR18 0RH', 'A', '6/15', '4447915332882744');
insert into customer values ('Monica Steele ', 'Monica.Steele@yahoo.com', '135 Grove Road, Norwich NR3 5LB', 'V', '1/14', '1284850699045096');
insert into customer values ('Nancy Chizarura ', 'Nancy.Chizarura@gmail.com', '25 St Leonard''s Road, Norwich NR2 2AX', 'V', '8/14', '3217358417874174');
insert into customer values ('Nigel Ainley ', 'Nigel.Ainley@yahoo.com', '60 Temple Road, Peterborough PE4 8QT', 'MC', '4/15', '6538837920765282');
insert into customer values ('Phillip Taylor ', 'Phillip.Taylor@yahoo.com', '40 Helena Road, Holt NR25 3AZ', 'V', '2/16', '3659676257717685');
insert into customer values ('Rob Thompson ', 'Rob.Thompson@gmail.com', '10 Spencergate York Y2 1SL', 'MC', '6/16', '4155755838256510');
insert into customer values ('Sally Rodgers ', 'Sally.Rodgers@yahoo.com', '28 St Anne''s Road, London SE23 1FT', 'V', '10/16', '6453483470978788');
insert into customer values ('Sheila Reynolds ', 'Sheila.Reynolds@yahoo.com', '25 Laundry Loke, North Walsham NR28 0DK', 'V', '3/14', '6842481611086565');
insert into customer values ('Stuart Reed ', 'Stuart.Reed@yahoo.com', '170 New Road, Kings Lynn PE30 0UJ', 'AE', '12/14', '6621132418881361');
insert into customer values ('Sue Gray ', 'Sue.Gray@yahoo.com', '20 Albert Street, Ipswich IP2 3AX', 'V', '8/14', '8947579422735172');
insert into customer values ('Sue Green ', 'Sue.Green@yahoo.com', '40 Helena Road, Cromer NR27 6TH', 'MC', '5/14', '6781813175933739');
insert into customer values ('Susan Connolly ', 'Susan.Connolly@gmail.com', '65 Victoria Road, Wymondham NR18 3RG', 'MC', '10/14', '9956366620035307');
insert into customer values ('Vanessa Burns ', 'Vanessa.Burns@yahoo.com', '60 Temple Road, Norwich NR7 8QT', 'A', '11/16', '7422266817013766');
insert into customer values ('Zoe Smallbone ', 'Zoe.Smallbone@yahoo.com', '5 Lime Tree Avanue, Hethersett NR18 1FK', 'V', '6/16', '8703274271879718');

INSERT INTO biketype (bike_type,model,description) VALUES 
('mens_hybrid','Kona Dew SE FRR','Our hybrid bikes are equipped with mudguards and a luggage rack suitable for panniers. If your route is mainly on country roads or good quality paths, this is a much faster, more efficient ride than our mountain bikes.'),
('womens_mtb','Kona Lisa HT','Our womens mountain bikes feature female-specific geometry to ensure new-to-the-sport female cyclists enter the world of the forest and trail with confidence and comfort.'),
('mens_mtb','Kona Nunu','Named for a God that personifies the vast freedom of the ocean, this is a solid bike with features designed to take a bite out of the trail.'),
('childs_mtb','Specialized Hotrock 24','This bike is an XC inspired bike designed to be fun, safe and reliable'),
('tandem','Dawes Duet','Our touring tandems will ensure you have a safe and comfortable trip and leave you hungry for your next tandem touring adventure. All tandems are fitted with a rear rack to give you the option of carrying luggage as well as mudguards to help keep you dry.'),
('womens_hybrid','Ridgeback Velocity','Ridgeback Velocity 2010 Womens Hybrid Bike comes complete with a 6061 Double butted Aluminium frame, cro moly Forks and shimano gearing'),
('mens_hybrid','Scott Sportster 60','The Scott Sportster 60 2011 Hybrid Bike is designed to emphasize comfort and convenience for touring and transportation. With a frame design that offers the user a smooth ride with plenty of standover clearance.'),
('womens_hybrid','Pinnacle Lithium One','The Lithium One Womens hybrid is a fast and capable 700c flatbar bike with a nice upright riding position and built-in comfort thanks to the skinny tapered steel fork and Kenda Kwest 38c tyres. A more compact frame geometry with low slung top tube combined with shorter crank arm length, stem, narrower handlebars and female specific saddle makes the Lithium One Womens as comfortable for female riders as possible. A 6sp 14-34T freewheel combined with 28-38-48T Suntour chainset make for a great urban gear range. Perfect for city commuting duties or weekend jaunts along the canal towpath. Ergonomic bar-ends add comfort and power on ascents and offer an additional hand position.'),
('childs_mtb','Specialized Hotrock 20','Inspired by the adult XC hardrock bike, the Specialized Hotrock 20 Inch 2012 Kids Bike (20 Inch Wheel) is a fun, safe and reliable bike featuring a lightweight aluminium frame with a low standover height for care-free starts and stops, easy to use adjustable V-brakes, 7 speed gearing offering an easy introduction to using gears and 40mm suspension forks giving more control over bumps.'),
('childs_mtb','Jamie X.24','The Jamis X.24 Kids Bike (24 inch Wheel) uses size-specific items at every contact point, with grips, saddles and cranks that are scaled appropriately for each frame size to maximize comfort and control. We sweat the details, too, with features like high-quality polyethylene and polyurethane foam saddles that won''t pack down and bottom out, grippy saddle top surfaces with slick sides that won''t chafe young thighs, and we use tough handlebar grips with enough give for comfort, but afford a secure grip for small fingers.');

INSERT INTO bike (model,bike_type,bike_id) VALUES
('Kona Dew SE FRR','mens_hybrid','NCH180'),
('Kona Dew SE FRR','mens_hybrid','NCH248'),
('Kona Dew SE FRR','mens_hybrid','NCH178'),
('Kona Dew SE FRR','mens_hybrid','NCH150'),
('Kona Dew SE FRR','mens_hybrid','NCH103'),
('Kona Dew SE FRR','mens_hybrid','NCH224'),
('Kona Dew SE FRR','mens_hybrid','NCH264'),
('Kona Dew SE FRR','mens_hybrid','NCH170'),
('Kona Dew SE FRR','mens_hybrid','NCH206'),
('Kona Dew SE FRR','mens_hybrid','NCH279'),
('Kona Dew SE FRR','mens_hybrid','NCH281'),
('Kona Dew SE FRR','mens_hybrid','NCH257'),
('Kona Dew SE FRR','mens_hybrid','NCH225'),
('Kona Dew SE FRR','mens_hybrid','NCH297'),
('Kona Dew SE FRR','mens_hybrid','NCH153'),
('Kona Dew SE FRR','mens_hybrid','NCH293'),
('Kona Dew SE FRR','mens_hybrid','NCH142'),
('Kona Dew SE FRR','mens_hybrid','NCH107'),
('Kona Dew SE FRR','mens_hybrid','NCH162'),
('Kona Dew SE FRR','mens_hybrid','NCH283'),
('Kona Dew SE FRR','mens_hybrid','NCH196'),
('Kona Dew SE FRR','mens_hybrid','NCH135'),
('Kona Dew SE FRR','mens_hybrid','NCH156'),
('Scott Sportster 60','mens_hybrid','NCH148'),
('Scott Sportster 60','mens_hybrid','NCH101'),
('Scott Sportster 60','mens_hybrid','NCH120'),
('Scott Sportster 60','mens_hybrid','NCH252'),
('Ridgeback Velocity','womens_hybrid','NCH111'),
('Ridgeback Velocity','womens_hybrid','NCH209'),
('Ridgeback Velocity','womens_hybrid','NCH291'),
('Ridgeback Velocity','womens_hybrid','NCH247'),
('Ridgeback Velocity','womens_hybrid','NCH163'),
('Ridgeback Velocity','womens_hybrid','NCH260'),
('Ridgeback Velocity','womens_hybrid','NCH108'),
('Ridgeback Velocity','womens_hybrid','NCH245'),
('Ridgeback Velocity','womens_hybrid','NCH187'),
('Ridgeback Velocity','womens_hybrid','NCH134'),
('Ridgeback Velocity','womens_hybrid','NCH106'),
('Ridgeback Velocity','womens_hybrid','NCH167'),
('Ridgeback Velocity','womens_hybrid','NCH232'),
('Ridgeback Velocity','womens_hybrid','NCH139'),
('Ridgeback Velocity','womens_hybrid','NCH174'),
('Ridgeback Velocity','womens_hybrid','NCH173'),
('Ridgeback Velocity','womens_hybrid','NCH215'),
('Ridgeback Velocity','womens_hybrid','NCH202'),
('Ridgeback Velocity','womens_hybrid','NCH124'),
('Ridgeback Velocity','womens_hybrid','NCH292'),
('Ridgeback Velocity','womens_hybrid','NCH116'),
('Pinnacle Lithium One','womens_hybrid','NCH105'),
('Pinnacle Lithium One','womens_hybrid','NCH128'),
('Pinnacle Lithium One','womens_hybrid','NCH136'),
('Pinnacle Lithium One','womens_hybrid','NCH122'),
('Kona Nunu','mens_mtb','NCH284'),
('Kona Nunu','mens_mtb','NCH228'),
('Kona Nunu','mens_mtb','NCH164'),
('Kona Nunu','mens_mtb','NCH125'),
('Kona Nunu','mens_mtb','NCH212'),
('Kona Nunu','mens_mtb','NCH157'),
('Kona Nunu','mens_mtb','NCH276'),
('Kona Nunu','mens_mtb','NCH226'),
('Kona Nunu','mens_mtb','NCH242'),
('Kona Nunu','mens_mtb','NCH127'),
('Kona Nunu','mens_mtb','NCH102'),
('Kona Nunu','mens_mtb','NCH115'),
('Kona Nunu','mens_mtb','NCH261'),
('Kona Nunu','mens_mtb','NCH289'),
('Kona Nunu','mens_mtb','NCH234'),
('Kona Nunu','mens_mtb','NCH117'),
('Kona Nunu','mens_mtb','NCH182'),
('Kona Nunu','mens_mtb','NCH296'),
('Kona Nunu','mens_mtb','NCH259'),
('Kona Nunu','mens_mtb','NCH193'),
('Kona Nunu','mens_mtb','NCH294'),
('Kona Nunu','mens_mtb','NCH113'),
('Kona Nunu','mens_mtb','NCH217'),
('Kona Nunu','mens_mtb','NCH144'),
('Kona Nunu','mens_mtb','NCH274'),
('Kona Nunu','mens_mtb','NCH165'),
('Kona Nunu','mens_mtb','NCH256'),
('Kona Nunu','mens_mtb','NCH249'),
('Kona Lisa HT','womens_mtb','NCH208'),
('Kona Lisa HT','womens_mtb','NCH179'),
('Kona Lisa HT','womens_mtb','NCH141'),
('Kona Lisa HT','womens_mtb','NCH184'),
('Kona Lisa HT','womens_mtb','NCH177'),
('Kona Lisa HT','womens_mtb','NCH230'),
('Kona Lisa HT','womens_mtb','NCH140'),
('Kona Lisa HT','womens_mtb','NCH154'),
('Kona Lisa HT','womens_mtb','NCH110'),
('Kona Lisa HT','womens_mtb','NCH288'),
('Kona Lisa HT','womens_mtb','NCH251'),
('Kona Lisa HT','womens_mtb','NCH270'),
('Kona Lisa HT','womens_mtb','NCH219'),
('Kona Lisa HT','womens_mtb','NCH222'),
('Kona Lisa HT','womens_mtb','NCH205'),
('Kona Lisa HT','womens_mtb','NCH149'),
('Kona Lisa HT','womens_mtb','NCH181'),
('Kona Lisa HT','womens_mtb','NCH233'),
('Kona Lisa HT','womens_mtb','NCH204'),
('Kona Lisa HT','womens_mtb','NCH195'),
('Kona Lisa HT','womens_mtb','NCH171'),
('Kona Lisa HT','womens_mtb','NCH175'),
('Kona Lisa HT','womens_mtb','NCH112'),
('Kona Lisa HT','womens_mtb','NCH186'),
('Kona Lisa HT','womens_mtb','NCH216'),
('Kona Lisa HT','womens_mtb','NCH299'),
('Kona Lisa HT','womens_mtb','NCH172'),
('Kona Lisa HT','womens_mtb','NCH287'),
('Kona Lisa HT','womens_mtb','NCH155'),
('Kona Lisa HT','womens_mtb','NCH151'),
('Kona Lisa HT','womens_mtb','NCH189'),
('Kona Lisa HT','womens_mtb','NCH236'),
('Dawes Duet','tandem','NCH255'),
('Dawes Duet','tandem','NCH126'),
('Dawes Duet','tandem','NCH227'),
('Dawes Duet','tandem','NCH185'),
('Specialized Hotrock 24','childs_mtb','NCH324'),
('Specialized Hotrock 24','childs_mtb','NCH342'),
('Specialized Hotrock 24','childs_mtb','NCH359'),
('Specialized Hotrock 24','childs_mtb','NCH347'),
('Specialized Hotrock 24','childs_mtb','NCH351'),
('Specialized Hotrock 24','childs_mtb','NCH328'),
('Specialized Hotrock 24','childs_mtb','NCH343'),
('Specialized Hotrock 24','childs_mtb','NCH315'),
('Specialized Hotrock 24','childs_mtb','NCH336'),
('Specialized Hotrock 24','childs_mtb','NCH361'),
('Specialized Hotrock 24','childs_mtb','NCH381'),
('Specialized Hotrock 20','childs_mtb','NCH326'),
('Specialized Hotrock 20','childs_mtb','NCH341'),
('Specialized Hotrock 20','childs_mtb','NCH319'),
('Specialized Hotrock 20','childs_mtb','NCH360'),
('Specialized Hotrock 20','childs_mtb','NCH390'),
('Specialized Hotrock 20','childs_mtb','NCH334'),
('Specialized Hotrock 20','childs_mtb','NCH335'),
('Specialized Hotrock 20','childs_mtb','NCH318'),
('Specialized Hotrock 20','childs_mtb','NCH317'),
('Specialized Hotrock 20','childs_mtb','NCH311'),
('Specialized Hotrock 20','childs_mtb','NCH300'),
('Specialized Hotrock 20','childs_mtb','NCH363'),
('Specialized Hotrock 20','childs_mtb','NCH385'),
('Specialized Hotrock 20','childs_mtb','NCH308'),
('Jamie X.24','childs_mtb','NCH355'),
('Jamie X.24','childs_mtb','NCH373'),
('Jamie X.24','childs_mtb','NCH386'),
('Jamie X.24','childs_mtb','NCH312'),
('Jamie X.24','childs_mtb','NCH316'),
('Jamie X.24','childs_mtb','NCH362'),
('Jamie X.24','childs_mtb','NCH383'),
('Jamie X.24','childs_mtb','NCH382'),
('Jamie X.24','childs_mtb','NCH370'),
('Jamie X.24','childs_mtb','NCH321'),
('Jamie X.24','childs_mtb','NCH368'),
('Jamie X.24','childs_mtb','NCH309'),
('Jamie X.24','childs_mtb','NCH339');

insert into booking values ('1295', 'Denise.Follows@gmail.com', '28/8/2014', 'ALL');
insert into booking values ('1323', 'Dominic.Earl@yahoo.com', '7/5/2014', 'AM');
insert into booking values ('1342', 'Denise.Gower@gmail.com', '4/5/2014', 'ALL');
insert into booking values ('1359', 'Zoe.Smallbone@yahoo.com', '9/12/2014', 'AM');
insert into booking values ('1385', 'Nigel.Ainley@yahoo.com', '23/8/2014', 'PM');
insert into booking values ('1417', 'Debbie.Davison@yahoo.com', '20/6/2014', 'PM');
insert into booking values ('1522', 'Sheila.Reynolds@yahoo.com', '7/7/2014', 'AM');
insert into booking values ('1600', 'Stuart.Reed@yahoo.com', '30/3/2014', 'ALL');
insert into booking values ('1643', 'Danny.Keenan@gmail.com', '9/7/2014', 'PM');
insert into booking values ('1682', 'Deborah.Herring@gmail.com', '20/5/2014', 'PM');
insert into booking values ('1696', 'Susan.Connolly@gmail.com', '23/8/2014', 'ALL');
insert into booking values ('1717', 'Michelle.Stafford@gmail.com', '26/11/2014', 'AM');
insert into booking values ('1793', 'Joe.Page@yahoo.com', '9/8/2014', 'AM');
insert into booking values ('1821', 'Nancy.Chizarura@gmail.com', '28/4/2014', 'PM');
insert into booking values ('1830', 'Carol.Pearson@gmail.com', '4/7/2014', 'PM');
insert into booking values ('1878', 'Joseph.ZaforKahn@gmail.com', '2/12/2014', 'AM');
insert into booking values ('1893', 'Chris.Holmes@gmail.com', '3/6/2014', 'ALL');
insert into booking values ('1912', 'Jolanta.Balaz@gmail.com', '11/7/2014', 'PM');
insert into booking values ('1945', 'Phillip.Taylor@yahoo.com', '1/5/2014', 'ALL');
insert into booking values ('2111', 'Rob.Thompson@gmail.com', '5/4/2014', 'PM');
insert into booking values ('2323', 'Dominic.Earl@yahoo.com', '4/3/2014', 'ALL');
insert into booking values ('2390', 'Mathew.Ireland@yahoo.com', '9/11/2014', 'PM');
insert into booking values ('2843', 'Maria.Deacon@yahoo.com', '27/8/2014', 'ALL');
insert into booking values ('2884', 'Denise.Hunt@yahoo.com', '23/9/2014', 'ALL');
insert into booking values ('2905', 'Cherrill.Fisher@yahoo.com', '15/3/2014', 'PM');
insert into booking values ('2945', 'Phillip.Taylor@yahoo.com', '7/6/2014', 'PM');
insert into booking values ('2950', 'Ian.Haden@yahoo.com', '10/7/2014', 'ALL');
insert into booking values ('2968', 'Linda.Satt@gmail.com', '11/7/2014', 'PM');
insert into booking values ('3143', 'Sally.Rodgers@yahoo.com', '28/10/2014', 'AM');
insert into booking values ('3193', 'Sally.Rodgers@yahoo.com', '14/12/2014', 'PM');
insert into booking values ('3268', 'Sue.Gray@yahoo.com', '6/5/2014', 'PM');
insert into booking values ('3490', 'Vanessa.Burns@yahoo.com', '28/8/2014', 'ALL');
insert into booking values ('3788', 'Daphne.Mann@yahoo.com', '3/7/2014', 'AM');
insert into booking values ('3853', 'John.Moore@yahoo.com', '18/3/2014', 'PM');
insert into booking values ('3890', 'Vanessa.Burns@yahoo.com', '15/12/2014', 'PM');
insert into booking values ('3945', 'Phillip.Taylor@yahoo.com', '8/10/2014', 'ALL');
insert into booking values ('3968', 'Sue.Gray@yahoo.com', '3/6/2014', 'PM');
insert into booking values ('3971', 'Linda.Satt@gmail.com', '22/3/2014', 'AM');
insert into booking values ('3975', 'Cherrill.Fisher@yahoo.com', '10/6/2014', 'PM');
insert into booking values ('4197', 'Craig.Naylor@yahoo.com', '4/7/2014', 'ALL');
insert into booking values ('4457', 'Darren.Lee@gmail.com', '22/6/2014', 'ALL');
insert into booking values ('4843', 'Maria.Deacon@yahoo.com', '4/4/2014', 'PM');
insert into booking values ('4904', 'Andrew.Spalding@yahoo.com', '6/5/2014', 'AM');
insert into booking values ('4997', 'Craig.Naylor@yahoo.com', '26/8/2014', 'PM');
insert into booking values ('5280', 'Michael.Harrison@yahoo.com', '4/6/2014', 'PM');
insert into booking values ('5372', 'Keith.Roberts@yahoo.com', '13/6/2014', 'AM');
insert into booking values ('5600', 'Monica.Steele@yahoo.com', '10/8/2014', 'ALL');
insert into booking values ('5760', 'Emma.Seager@gmail.com', '13/5/2014', 'AM');
insert into booking values ('5772', 'Keith.Roberts@yahoo.com', '11/7/2014', 'PM');
insert into booking values ('5880', 'Michael.Harrison@yahoo.com', '12/7/2014', 'PM');
insert into booking values ('5904', 'Andrew.Spalding@yahoo.com', '2/8/2014', 'AM');
insert into booking values ('5914', 'Jolanta.Balaz@gmail.com', '4/3/2014', 'ALL');
insert into booking values ('6080', 'Sue.Gray@yahoo.com', '20/10/2014', 'PM');
insert into booking values ('6160', 'Emma.Seager@gmail.com', '11/7/2014', 'PM');
insert into booking values ('6417', 'Darren.Lee@gmail.com', '18/9/2014', 'ALL');
insert into booking values ('7080', 'Sue.Gray@yahoo.com', '15/11/2014', 'AM');
insert into booking values ('7236', 'Jennifer.Gray@yahoo.com', '3/5/2014', 'AM');
insert into booking values ('7425', 'Nancy.Chizarura@gmail.com', '5/12/2014', 'PM');
insert into booking values ('7463', 'Jodie.Anderson@yahoo.com', '2/10/2014', 'PM');
insert into booking values ('7600', 'Monica.Steele@yahoo.com', '20/10/2014', 'ALL');
insert into booking values ('7793', 'Joe.Page@yahoo.com', '7/3/2014', 'ALL');
insert into booking values ('7843', 'Maria.Deacon@yahoo.com', '18/10/2014', 'ALL');
insert into booking values ('7853', 'John.Moore@yahoo.com', '13/6/2014', 'ALL');
insert into booking values ('7988', 'Daphne.Mann@yahoo.com', '7/12/2014', 'AM');
insert into booking values ('8080', 'Sue.Gray@yahoo.com', '4/12/2014', 'PM');
insert into booking values ('8323', 'Dominic.Earl@yahoo.com', '30/11/2014', 'PM');

insert into booked_bike values (1295, 'NCH101');
insert into booked_bike values (1295, 'NCH204');
insert into booked_bike values (1295, 'NCH335');
insert into booked_bike values (1295, 'NCH359');
insert into booked_bike values (1342, 'NCH102');
insert into booked_bike values (1342, 'NCH205');
insert into booked_bike values (1342, 'NCH336');
insert into booked_bike values (1342, 'NCH360');
insert into booked_bike values (1359, 'NCH105');
insert into booked_bike values (1359, 'NCH206');
insert into booked_bike values (1359, 'NCH336');
insert into booked_bike values (1359, 'NCH362');
insert into booked_bike values (1385, 'NCH106');
insert into booked_bike values (1385, 'NCH208');
insert into booked_bike values (1385, 'NCH341');
insert into booked_bike values (1385, 'NCH362');
insert into booked_bike values (1417, 'NCH103');
insert into booked_bike values (1417, 'NCH209');
insert into booked_bike values (1417, 'NCH342');
insert into booked_bike values (1417, 'NCH363');
insert into booked_bike values (1522, 'NCH107');
insert into booked_bike values (1522, 'NCH212');
insert into booked_bike values (1522, 'NCH342');
insert into booked_bike values (1522, 'NCH368');
insert into booked_bike values (1600, 'NCH107');
insert into booked_bike values (1600, 'NCH215');
insert into booked_bike values (1600, 'NCH343');
insert into booked_bike values (1600, 'NCH370');
insert into booked_bike values (1643, 'NCH108');
insert into booked_bike values (1643, 'NCH216');
insert into booked_bike values (1643, 'NCH308');
insert into booked_bike values (1643, 'NCH347');
insert into booked_bike values (1682, 'NCH110');
insert into booked_bike values (1682, 'NCH217');
insert into booked_bike values (1682, 'NCH309');
insert into booked_bike values (1682, 'NCH351');
insert into booked_bike values (1696, 'NCH111');
insert into booked_bike values (1696, 'NCH219');
insert into booked_bike values (1696, 'NCH311');
insert into booked_bike values (1696, 'NCH355');
insert into booked_bike values (1717, 'NCH112');
insert into booked_bike values (1717, 'NCH222');
insert into booked_bike values (1717, 'NCH312');
insert into booked_bike values (1717, 'NCH355');
insert into booked_bike values (1821, 'NCH113');
insert into booked_bike values (1821, 'NCH224');
insert into booked_bike values (1821, 'NCH316');
insert into booked_bike values (1821, 'NCH359');
insert into booked_bike values (1830, 'NCH115');
insert into booked_bike values (1830, 'NCH225');
insert into booked_bike values (1830, 'NCH316');
insert into booked_bike values (1830, 'NCH360');
insert into booked_bike values (1878, 'NCH116');
insert into booked_bike values (1878, 'NCH226');
insert into booked_bike values (1878, 'NCH317');
insert into booked_bike values (1878, 'NCH362');
insert into booked_bike values (1893, 'NCH117');
insert into booked_bike values (1893, 'NCH228');
insert into booked_bike values (1893, 'NCH318');
insert into booked_bike values (1893, 'NCH362');
insert into booked_bike values (2111, 'NCH120');
insert into booked_bike values (2111, 'NCH228');
insert into booked_bike values (2111, 'NCH318');
insert into booked_bike values (2111, 'NCH363');
insert into booked_bike values (2390, 'NCH122');
insert into booked_bike values (2390, 'NCH230');
insert into booked_bike values (2390, 'NCH321');
insert into booked_bike values (2390, 'NCH368');
insert into booked_bike values (2843, 'NCH247');
insert into booked_bike values (2884, 'NCH124');
insert into booked_bike values (2884, 'NCH232');
insert into booked_bike values (2884, 'NCH324');
insert into booked_bike values (2884, 'NCH370');
insert into booked_bike values (2905, 'NCH125');
insert into booked_bike values (2945, 'NCH127');
insert into booked_bike values (2950, 'NCH126');
insert into booked_bike values (2950, 'NCH233');
insert into booked_bike values (2950, 'NCH326');
insert into booked_bike values (2950, 'NCH373');
insert into booked_bike values (2968, 'NCH141');
insert into booked_bike values (3143, 'NCH126');
insert into booked_bike values (3143, 'NCH186');
insert into booked_bike values (3143, 'NCH234');
insert into booked_bike values (3143, 'NCH316');
insert into booked_bike values (3143, 'NCH328');
insert into booked_bike values (3143, 'NCH347');
insert into booked_bike values (3143, 'NCH381');
insert into booked_bike values (3193, 'NCH184');
insert into booked_bike values (3268, 'NCH128');
insert into booked_bike values (3268, 'NCH186');
insert into booked_bike values (3268, 'NCH236');
insert into booked_bike values (3268, 'NCH317');
insert into booked_bike values (3268, 'NCH335');
insert into booked_bike values (3268, 'NCH351');
insert into booked_bike values (3268, 'NCH382');
insert into booked_bike values (3490, 'NCH128');
insert into booked_bike values (3490, 'NCH187');
insert into booked_bike values (3490, 'NCH242');
insert into booked_bike values (3490, 'NCH318');
insert into booked_bike values (3490, 'NCH336');
insert into booked_bike values (3490, 'NCH355');
insert into booked_bike values (3490, 'NCH383');
insert into booked_bike values (3788, 'NCH134');
insert into booked_bike values (3788, 'NCH189');
insert into booked_bike values (3788, 'NCH245');
insert into booked_bike values (3788, 'NCH318');
insert into booked_bike values (3788, 'NCH336');
insert into booked_bike values (3788, 'NCH355');
insert into booked_bike values (3788, 'NCH385');
insert into booked_bike values (3853, 'NCH299');
insert into booked_bike values (3890, 'NCH172');
insert into booked_bike values (3945, 'NCH135');
insert into booked_bike values (3945, 'NCH171');
insert into booked_bike values (3945, 'NCH193');
insert into booked_bike values (3945, 'NCH248');
insert into booked_bike values (3945, 'NCH292');
insert into booked_bike values (3945, 'NCH318');
insert into booked_bike values (3945, 'NCH321');
insert into booked_bike values (3945, 'NCH341');
insert into booked_bike values (3945, 'NCH351');
insert into booked_bike values (3945, 'NCH359');
insert into booked_bike values (3945, 'NCH386');
insert into booked_bike values (3968, 'NCH227');
insert into booked_bike values (3971, 'NCH315');
insert into booked_bike values (3975, 'NCH361');
insert into booked_bike values (1323, 'NCH136');
insert into booked_bike values (1323, 'NCH173');
insert into booked_bike values (1323, 'NCH195');
insert into booked_bike values (1323, 'NCH249');
insert into booked_bike values (1323, 'NCH293');
insert into booked_bike values (1323, 'NCH321');
insert into booked_bike values (1323, 'NCH324');
insert into booked_bike values (1323, 'NCH342');
insert into booked_bike values (1323, 'NCH355');
insert into booked_bike values (1323, 'NCH360');
insert into booked_bike values (1323, 'NCH390');
insert into booked_bike values (4197, 'NCH319');
insert into booked_bike values (1793, 'NCH139');
insert into booked_bike values (1793, 'NCH174');
insert into booked_bike values (1793, 'NCH196');
insert into booked_bike values (1793, 'NCH249');
insert into booked_bike values (1793, 'NCH294');
insert into booked_bike values (1793, 'NCH308');
insert into booked_bike values (1793, 'NCH324');
insert into booked_bike values (1793, 'NCH326');
insert into booked_bike values (1793, 'NCH342');
insert into booked_bike values (1793, 'NCH355');
insert into booked_bike values (1793, 'NCH362');
insert into booked_bike values (4457, 'NCH334');
insert into booked_bike values (4843, 'NCH140');
insert into booked_bike values (4843, 'NCH175');
insert into booked_bike values (4843, 'NCH202');
insert into booked_bike values (4843, 'NCH251');
insert into booked_bike values (4843, 'NCH296');
insert into booked_bike values (4843, 'NCH309');
insert into booked_bike values (4843, 'NCH326');
insert into booked_bike values (4843, 'NCH328');
insert into booked_bike values (4843, 'NCH343');
insert into booked_bike values (4843, 'NCH370');
insert into booked_bike values (4904, 'NCH140');
insert into booked_bike values (4904, 'NCH177');
insert into booked_bike values (4904, 'NCH252');
insert into booked_bike values (4904, 'NCH297');
insert into booked_bike values (4904, 'NCH311');
insert into booked_bike values (4904, 'NCH328');
insert into booked_bike values (4904, 'NCH347');
insert into booked_bike values (4997, 'NCH142');
insert into booked_bike values (4997, 'NCH178');
insert into booked_bike values (4997, 'NCH255');
insert into booked_bike values (4997, 'NCH300');
insert into booked_bike values (4997, 'NCH312');
insert into booked_bike values (4997, 'NCH335');
insert into booked_bike values (4997, 'NCH351');
insert into booked_bike values (5280, 'NCH144');
insert into booked_bike values (5280, 'NCH179');
insert into booked_bike values (5280, 'NCH256');
insert into booked_bike values (5280, 'NCH300');
insert into booked_bike values (5280, 'NCH316');
insert into booked_bike values (5280, 'NCH336');
insert into booked_bike values (5280, 'NCH355');
insert into booked_bike values (5372, 'NCH148');
insert into booked_bike values (5372, 'NCH179');
insert into booked_bike values (5372, 'NCH257');
insert into booked_bike values (5372, 'NCH310');
insert into booked_bike values (5372, 'NCH318');
insert into booked_bike values (5372, 'NCH337');
insert into booked_bike values (5372, 'NCH355');
insert into booked_bike values (5600, 'NCH149');
insert into booked_bike values (5600, 'NCH180');
insert into booked_bike values (5600, 'NCH259');
insert into booked_bike values (5600, 'NCH309');
insert into booked_bike values (5600, 'NCH317');
insert into booked_bike values (5600, 'NCH341');
insert into booked_bike values (5600, 'NCH359');
insert into booked_bike values (5760, 'NCH150');
insert into booked_bike values (5760, 'NCH181');
insert into booked_bike values (5760, 'NCH260');
insert into booked_bike values (5760, 'NCH311');
insert into booked_bike values (5760, 'NCH318');
insert into booked_bike values (5760, 'NCH342');
insert into booked_bike values (5760, 'NCH360');
insert into booked_bike values (5772, 'NCH339');
insert into booked_bike values (5880, 'NCH247');
insert into booked_bike values (5904, 'NCH125');
insert into booked_bike values (5912, 'NCH151');
insert into booked_bike values (5912, 'NCH182');
insert into booked_bike values (5912, 'NCH261');
insert into booked_bike values (5912, 'NCH312');
insert into booked_bike values (5912, 'NCH318');
insert into booked_bike values (5912, 'NCH342');
insert into booked_bike values (5912, 'NCH362');
insert into booked_bike values (5914, 'NCH127');
insert into booked_bike values (6080, 'NCH141');
insert into booked_bike values (6160, 'NCH184');
insert into booked_bike values (6417, 'NCH153');
insert into booked_bike values (6417, 'NCH185');
insert into booked_bike values (6417, 'NCH264');
insert into booked_bike values (6417, 'NCH316');
insert into booked_bike values (6417, 'NCH321');
insert into booked_bike values (6417, 'NCH343');
insert into booked_bike values (6417, 'NCH362');
insert into booked_bike values (7080, 'NCH299');
insert into booked_bike values (7236, 'NCH154');
insert into booked_bike values (7236, 'NCH270');
insert into booked_bike values (7236, 'NCH324');
insert into booked_bike values (7236, 'NCH363');
insert into booked_bike values (7425, 'NCH155');
insert into booked_bike values (7425, 'NCH274');
insert into booked_bike values (7425, 'NCH326');
insert into booked_bike values (7425, 'NCH368');
insert into booked_bike values (7463, 'NCH156');
insert into booked_bike values (7463, 'NCH276');
insert into booked_bike values (7463, 'NCH328');
insert into booked_bike values (7463, 'NCH370');
insert into booked_bike values (7600, 'NCH172');
insert into booked_bike values (7793, 'NCH156');
insert into booked_bike values (7793, 'NCH164');
insert into booked_bike values (7793, 'NCH279');
insert into booked_bike values (7793, 'NCH287');
insert into booked_bike values (7793, 'NCH308');
insert into booked_bike values (7793, 'NCH316');
insert into booked_bike values (7793, 'NCH335');
insert into booked_bike values (7793, 'NCH342');
insert into booked_bike values (7843, 'NCH227');
insert into booked_bike values (7853, 'NCH157');
insert into booked_bike values (7853, 'NCH165');
insert into booked_bike values (7853, 'NCH281');
insert into booked_bike values (7853, 'NCH288');
insert into booked_bike values (7853, 'NCH309');
insert into booked_bike values (7853, 'NCH316');
insert into booked_bike values (7853, 'NCH336');
insert into booked_bike values (7853, 'NCH342');
insert into booked_bike values (7905, 'NCH162');
insert into booked_bike values (7905, 'NCH167');
insert into booked_bike values (7905, 'NCH283');
insert into booked_bike values (7905, 'NCH289');
insert into booked_bike values (7905, 'NCH311');
insert into booked_bike values (7905, 'NCH317');
insert into booked_bike values (7905, 'NCH336');
insert into booked_bike values (7905, 'NCH343');
insert into booked_bike values (7968, 'NCH163');
insert into booked_bike values (7968, 'NCH170');
insert into booked_bike values (7968, 'NCH284');
insert into booked_bike values (7968, 'NCH291');
insert into booked_bike values (7968, 'NCH312');
insert into booked_bike values (7968, 'NCH318');
insert into booked_bike values (7968, 'NCH341');
insert into booked_bike values (7968, 'NCH347');
insert into booked_bike values (7988, 'NCH315');
insert into booked_bike values (8080, 'NCH361');
insert into booked_bike values (8323, 'NCH319');

INSERT INTO rates VALUES ('mens_hybrid', 15.00, 9.00);
INSERT INTO rates VALUES ('womens_hybrid', 15.00, 9.00);
INSERT INTO rates VALUES ('mens_mtb', 14.00, 8.00);
INSERT INTO rates VALUES ('womens_mtb', 14.00, 8.00);
INSERT INTO rates VALUES ('childs_mtb', 10.00, 5.00);
INSERT INTO rates VALUES ('tandem', 25.00, 13.00);

insert into bikeservice values ('NCH236','01/03/2013','02/03/2013','new front tyre');
insert into bikeservice values ('NCH156','03/03/2013','04/03/2013','new rear inner tube');
insert into bikeservice values ('NCH106','06/03/2013','07/03/2013','new rear inner tube');
insert into bikeservice values ('NCH117','06/03/2013','07/03/2013','front brakes changed');
insert into bikeservice values ('NCH259','06/03/2013','07/03/2013','front brakes changed');
insert into bikeservice values ('NCH288','06/03/2013','07/03/2013','front brakes changed');
insert into bikeservice values ('NCH139','07/03/2013','08/03/2013','front brakes changed');
insert into bikeservice values ('NCH204','07/03/2013','08/03/2013','rear brakes changed');
insert into bikeservice values ('NCH284','08/03/2013','09/03/2013','front brakes changed');
insert into bikeservice values ('NCH140','10/03/2013','11/03/2013','rear brakes changed');
insert into bikeservice values ('NCH219','10/03/2013','11/03/2013','rear brakes changed');
insert into bikeservice values ('NCH149','11/03/2013','12/03/2013','rear brakes changed');
insert into bikeservice values ('NCH164','12/03/2013','13/03/2013','rear brakes changed');
insert into bikeservice values ('NCH215','13/03/2013','14/03/2013','new front inner tube');
insert into bikeservice values ('NCH251','13/03/2013','14/03/2013','new front inner tube');
insert into bikeservice values ('NCH144','17/03/2013','18/03/2013','replaced saddle');
insert into bikeservice values ('NCH150','20/03/2013','21/03/2013','new rear inner tube');
insert into bikeservice values ('NCH264','20/03/2013','21/03/2013','new front inner tube');
insert into bikeservice values ('NCH157','20/03/2013','21/03/2013','new rear inner tube');
insert into bikeservice values ('NCH249','20/03/2013','21/03/2013','new handgrips');
insert into bikeservice values ('NCH234','22/03/2013','23/03/2013','front brakes changed');
insert into bikeservice values ('NCH193','24/03/2013','25/03/2013','rear brakes changed');
insert into bikeservice values ('NCH216','24/03/2013','26/03/2013','replaced front forks');
insert into bikeservice values ('NCH144','25/03/2013','26/03/2013','front brakes changed');
insert into bikeservice values ('NCH177','25/03/2013','26/03/2013','front brakes changed');
insert into bikeservice values ('NCH128','26/03/2013','27/03/2013','front brakes changed');
insert into bikeservice values ('NCH117','26/03/2013','27/03/2013','replaced front forks');
insert into bikeservice values ('NCH179','27/03/2013','28/03/2013','new front inner tube');
insert into bikeservice values ('NCH291','28/03/2013','29/03/2013','rear brakes changed');
insert into bikeservice values ('NCH165','28/03/2013','29/03/2013','new front inner tube');
insert into bikeservice values ('NCH276','28/03/2013','29/03/2013','rear brakes changed');
insert into bikeservice values ('NCH216','30/03/2013','31/03/2013','rear brakes changed');
insert into bikeservice values ('NCH294','01/04/2013','02/04/2013','rear brakes changed');
insert into bikeservice values ('NCH153','03/04/2013','04/04/2013','front brakes changed');
insert into bikeservice values ('NCH165','03/04/2013','04/04/2013','front wheel replaced');
insert into bikeservice values ('NCH126','05/04/2013','06/04/2013','front brakes changed');
insert into bikeservice values ('NCH106','05/04/2013','06/04/2013','front brakes changed');
insert into bikeservice values ('NCH102','05/04/2013','06/04/2013','rear brakes changed');
insert into bikeservice values ('NCH167','10/04/2013','11/04/2013','rear brakes changed');
insert into bikeservice values ('NCH204','10/04/2013','11/04/2013','new rear inner tube');
insert into bikeservice values ('NCH189','11/04/2013','12/04/2013','front brakes changed');
insert into bikeservice values ('NCH170','14/04/2013','15/04/2013','new rear inner tube');
insert into bikeservice values ('NCH288','15/04/2013','16/04/2013','front brakes changed');
insert into bikeservice values ('NCH261','15/04/2013','16/04/2013','front brakes changed');
insert into bikeservice values ('NCH179','15/04/2013','16/04/2013','front brakes changed');
insert into bikeservice values ('NCH177','15/04/2013','16/04/2013','rear brakes changed');
insert into bikeservice values ('NCH134','17/04/2013','18/04/2013','rear brakes changed');
insert into bikeservice values ('NCH287','18/04/2013','19/04/2013','front brakes changed');
insert into bikeservice values ('NCH230','20/04/2013','21/04/2013','new rear inner tube');
insert into bikeservice values ('NCH233','20/04/2013','21/04/2013','new front inner tube');
insert into bikeservice values ('NCH113','20/04/2013','21/04/2013','new handgrips');
insert into bikeservice values ('NCH175','21/04/2013','22/04/2013','new rear inner tube');
insert into bikeservice values ('NCH179','21/04/2013','22/04/2013','replaced front forks');
insert into bikeservice values ('NCH106','22/04/2013','23/04/2013','new rear inner tube');
insert into bikeservice values ('NCH249','22/04/2013','23/04/2013','new rear inner tube');
insert into bikeservice values ('NCH151','22/04/2013','23/04/2013','new rear inner tube');
insert into bikeservice values ('NCH297','23/04/2013','24/04/2013','front brakes changed');
insert into bikeservice values ('NCH230','24/04/2013','25/04/2013','rear brakes changed');
insert into bikeservice values ('NCH296','24/04/2013','25/04/2013','front wheel replaced');
insert into bikeservice values ('NCH257','26/04/2013','27/04/2013','new front inner tube');
insert into bikeservice values ('NCH186','26/04/2013','27/04/2013','new rear tyre');
insert into bikeservice values ('NCH219','27/04/2013','28/04/2013','new front inner tube');
insert into bikeservice values ('NCH113','29/04/2013','01/05/2013','front brakes changed');
insert into bikeservice values ('NCH300','30/04/2013','01/05/2013','new handgrips');
insert into bikeservice values ('NCH279','02/05/2013','03/05/2013','rear brakes changed');
insert into bikeservice values ('NCH217','02/05/2013','03/05/2013','replaced saddle');
insert into bikeservice values ('NCH175','03/05/2013','04/05/2013','rear brakes changed');
insert into bikeservice values ('NCH249','03/05/2013','04/05/2013','new rear inner tube');
insert into bikeservice values ('NCH248','04/05/2013','05/05/2013','front brakes changed');
insert into bikeservice values ('NCH260','05/05/2013','06/05/2013','new rear inner tube');
insert into bikeservice values ('NCH228','05/05/2013','06/05/2013','new front inner tube');
insert into bikeservice values ('NCH256','06/05/2013','07/05/2013','new rear inner tube');
insert into bikeservice values ('NCH208','07/05/2013','08/05/2013','rear brakes changed');
insert into bikeservice values ('NCH171','07/05/2013','08/05/2013','rear brakes changed');
insert into bikeservice values ('NCH287','07/05/2013','08/05/2013','new front inner tube');
insert into bikeservice values ('NCH293','08/05/2013','09/05/2013','new front inner tube');
insert into bikeservice values ('NCH179','11/05/2013','12/05/2013','rear brakes changed');
insert into bikeservice values ('NCH126','12/05/2013','13/05/2013','front brakes changed');
insert into bikeservice values ('NCH300','12/05/2013','14/05/2013','replaced front forks');
insert into bikeservice values ('NCH289','13/05/2013','14/05/2013','rear brakes changed');
insert into bikeservice values ('NCH270','14/05/2013','15/05/2013','new front inner tube');
insert into bikeservice values ('NCH256','16/05/2013','17/05/2013','new front tyre');
insert into bikeservice values ('NCH252','17/05/2013','18/05/2013','new rear inner tube');
insert into bikeservice values ('NCH142','17/05/2013','18/05/2013','new front inner tube');
insert into bikeservice values ('NCH224','18/05/2013','19/05/2013','new front inner tube');
insert into bikeservice values ('NCH289','18/05/2013','19/05/2013','new rear inner tube');
insert into bikeservice values ('NCH284','21/05/2013','22/05/2013','front brakes changed');
insert into bikeservice values ('NCH105','22/05/2013','23/05/2013','front brakes changed');
insert into bikeservice values ('NCH156','22/05/2013','23/05/2013','new rear inner tube');
insert into bikeservice values ('NCH148','24/05/2013','25/05/2013','new front inner tube');
insert into bikeservice values ('NCH296','25/05/2013','26/05/2013','rear brakes changed');
insert into bikeservice values ('NCH140','25/05/2013','26/05/2013','rear brakes changed');
insert into bikeservice values ('NCH232','26/05/2013','27/05/2013','new rear inner tube');
insert into bikeservice values ('NCH178','27/05/2013','28/05/2013','new rear inner tube');
insert into bikeservice values ('NCH259','02/06/2013','03/06/2013','new front inner tube');
insert into bikeservice values ('NCH287','03/06/2013','04/06/2013','new handgrips');
insert into bikeservice values ('NCH222','06/06/2013','07/06/2013','new rear inner tube');
insert into bikeservice values ('NCH120','07/06/2013','08/06/2013','front brakes changed');
insert into bikeservice values ('NCH225','10/06/2013','11/06/2013','rear brakes changed');
insert into bikeservice values ('NCH186','11/06/2013','12/06/2013','rear brakes changed');
insert into bikeservice values ('NCH236','11/06/2013','12/06/2013','front brakes changed');
insert into bikeservice values ('NCH182','12/06/2013','13/06/2013','front brakes changed');
insert into bikeservice values ('NCH140','12/06/2013','13/06/2013','front brakes changed');
insert into bikeservice values ('NCH151','12/06/2013','13/06/2013','new front tyre');
insert into bikeservice values ('NCH209','13/06/2013','14/06/2013','front brakes changed');
insert into bikeservice values ('NCH259','14/06/2013','15/06/2013','new handgrips');
insert into bikeservice values ('NCH297','17/06/2013','18/06/2013','new rear inner tube');
insert into bikeservice values ('NCH117','17/06/2013','18/06/2013','front wheel replaced');
insert into bikeservice values ('NCH116','23/06/2013','24/06/2013','new rear inner tube');
insert into bikeservice values ('NCH212','23/06/2013','24/06/2013','new rear inner tube');
insert into bikeservice values ('NCH283','24/06/2013','25/06/2013','rear brakes changed');
insert into bikeservice values ('NCH274','24/06/2013','25/06/2013','replaced saddle');
insert into bikeservice values ('NCH274','26/06/2013','27/06/2013','new front inner tube');
insert into bikeservice values ('NCH111','27/06/2013','28/06/2013','new front inner tube');
insert into bikeservice values ('NCH163','27/06/2013','28/06/2013','new rear inner tube');
insert into bikeservice values ('NCH170','28/06/2013','29/06/2013','new rear inner tube');
insert into bikeservice values ('NCH154','29/06/2013','30/06/2013','rear brakes changed');
insert into bikeservice values ('NCH189','30/06/2013','01/07/2013','replaced saddle');
insert into bikeservice values ('NCH236','30/06/2013','01/07/2013','new front tyre');
insert into bikeservice values ('NCH151','03/07/2013','04/07/2013','new handgrips');
insert into bikeservice values ('NCH270','04/07/2013','05/07/2013','front brakes changed');
insert into bikeservice values ('NCH249','04/07/2013','05/07/2013','front brakes changed');
insert into bikeservice values ('NCH274','04/07/2013','06/07/2013','replaced saddle');
insert into bikeservice values ('NCH202','06/07/2013','07/07/2013','front brakes changed');
insert into bikeservice values ('NCH177','06/07/2013','07/07/2013','new front inner tube');
insert into bikeservice values ('NCH115','06/07/2013','07/07/2013','front brakes changed');
insert into bikeservice values ('NCH182','07/07/2013','08/07/2013','front wheel replaced');
insert into bikeservice values ('NCH242','08/07/2013','09/07/2013','front brakes changed');
insert into bikeservice values ('NCH142','09/07/2013','10/07/2013','new front inner tube');
insert into bikeservice values ('NCH248','09/07/2013','10/07/2013','rear brakes changed');
insert into bikeservice values ('NCH224','09/07/2013','10/07/2013','new front inner tube');
insert into bikeservice values ('NCH249','10/07/2013','11/07/2013','new rear inner tube');
insert into bikeservice values ('NCH216','10/07/2013','12/07/2013','replaced front forks');
insert into bikeservice values ('NCH174','13/07/2013','14/07/2013','new rear inner tube');
insert into bikeservice values ('NCH113','13/07/2013','14/07/2013','front brakes changed');
insert into bikeservice values ('NCH140','13/07/2013','14/07/2013','new rear inner tube');
insert into bikeservice values ('NCH173','14/07/2013','15/07/2013','new front inner tube');
insert into bikeservice values ('NCH287','15/07/2013','16/07/2013','new handgrips');
insert into bikeservice values ('NCH165','18/07/2013','19/07/2013','new handgrips');
insert into bikeservice values ('NCH236','20/07/2013','21/07/2013','new rear inner tube');
insert into bikeservice values ('NCH140','20/07/2013','21/07/2013','front wheel replaced');
insert into bikeservice values ('NCH113','20/07/2013','21/07/2013','new handgrips');
insert into bikeservice values ('NCH226','21/07/2013','22/07/2013','new rear inner tube');
insert into bikeservice values ('NCH144','21/07/2013','22/07/2013','new front inner tube');
insert into bikeservice values ('NCH165','22/07/2013','23/07/2013','front wheel replaced');
insert into bikeservice values ('NCH264','23/07/2013','24/07/2013','new rear inner tube');
insert into bikeservice values ('NCH179','24/07/2013','25/07/2013','front brakes changed');
insert into bikeservice values ('NCH217','26/07/2013','27/07/2013','new rear inner tube');
insert into bikeservice values ('NCH150','27/07/2013','28/07/2013','front brakes changed');
insert into bikeservice values ('NCH181','29/07/2013','30/07/2013','new rear inner tube');
insert into bikeservice values ('NCH279','30/07/2013','31/07/2013','new front inner tube');
insert into bikeservice values ('NCH155','01/08/2013','02/08/2013','replaced saddle');
insert into bikeservice values ('NCH217','01/08/2013','02/08/2013','front wheel replaced');
insert into bikeservice values ('NCH293','04/08/2013','05/08/2013','rear brakes changed');
insert into bikeservice values ('NCH171','04/08/2013','05/08/2013','new front inner tube');
insert into bikeservice values ('NCH110','05/08/2013','06/08/2013','new front inner tube');
insert into bikeservice values ('NCH300','05/08/2013','06/08/2013','front brakes changed');
insert into bikeservice values ('NCH124','06/08/2013','07/08/2013','rear brakes changed');
insert into bikeservice values ('NCH195','07/08/2013','08/08/2013','new front inner tube');
insert into bikeservice values ('NCH182','07/08/2013','08/08/2013','front brakes changed');
insert into bikeservice values ('NCH281','08/08/2013','09/08/2013','new rear inner tube');
insert into bikeservice values ('NCH276','09/08/2013','10/08/2013','front brakes changed');
insert into bikeservice values ('NCH186','10/08/2013','11/08/2013','front brakes changed');
insert into bikeservice values ('NCH157','10/08/2013','11/08/2013','front brakes changed');
insert into bikeservice values ('NCH165','14/08/2013','15/08/2013','rear brakes changed');
insert into bikeservice values ('NCH189','14/08/2013','15/08/2013','new rear inner tube');
insert into bikeservice values ('NCH259','14/08/2013','15/08/2013','front wheel replaced');
insert into bikeservice values ('NCH206','15/08/2013','16/08/2013','front brakes changed');
insert into bikeservice values ('NCH296','15/08/2013','16/08/2013','rear brakes changed');
insert into bikeservice values ('NCH144','15/08/2013','16/08/2013','new handgrips');
insert into bikeservice values ('NCH140','16/08/2013','17/08/2013','new rear inner tube');
insert into bikeservice values ('NCH102','17/08/2013','18/08/2013','new rear inner tube');
insert into bikeservice values ('NCH257','18/08/2013','19/08/2013','new rear inner tube');
insert into bikeservice values ('NCH261','19/08/2013','20/08/2013','new rear inner tube');
insert into bikeservice values ('NCH274','19/08/2013','20/08/2013','rear brakes changed');
insert into bikeservice values ('NCH155','20/08/2013','21/08/2013','front wheel replaced');
insert into bikeservice values ('NCH256','20/08/2013','21/08/2013','new rear tyre');
insert into bikeservice values ('NCH225','21/08/2013','22/08/2013','front brakes changed');
insert into bikeservice values ('NCH217','21/08/2013','22/08/2013','new front inner tube');
insert into bikeservice values ('NCH230','21/08/2013','22/08/2013','new rear inner tube');
insert into bikeservice values ('NCH300','25/08/2013','26/08/2013','new front inner tube');
insert into bikeservice values ('NCH186','25/08/2013','26/08/2013','new handgrips');
insert into bikeservice values ('NCH216','26/08/2013','27/08/2013','front brakes changed');
insert into bikeservice values ('NCH126','29/08/2013','30/08/2013','rear brakes changed');
insert into bikeservice values ('NCH182','29/08/2013','30/08/2013','new front tyre');
insert into bikeservice values ('NCH208','04/09/2013','05/09/2013','front brakes changed');
insert into bikeservice values ('NCH149','05/09/2013','06/09/2013','new front inner tube');
insert into bikeservice values ('NCH242','06/09/2013','07/09/2013','rear brakes changed');
insert into bikeservice values ('NCH256','06/09/2013','07/09/2013','new rear tyre');
insert into bikeservice values ('NCH186','08/09/2013','09/09/2013','front brakes changed');
insert into bikeservice values ('NCH226','09/09/2013','10/09/2013','new rear inner tube');
insert into bikeservice values ('NCH144','09/09/2013','10/09/2013','new handgrips');
insert into bikeservice values ('NCH189','11/09/2013','12/09/2013','replaced saddle');
insert into bikeservice values ('NCH193','12/09/2013','13/09/2013','new front tyre');
insert into bikeservice values ('NCH233','13/09/2013','14/09/2013','new front inner tube');
insert into bikeservice values ('NCH187','14/09/2013','15/09/2013','new rear inner tube');
insert into bikeservice values ('NCH205','14/09/2013','15/09/2013','new rear inner tube');
insert into bikeservice values ('NCH251','15/09/2013','16/09/2013','rear brakes changed');
insert into bikeservice values ('NCH283','16/09/2013','17/09/2013','new front inner tube');
insert into bikeservice values ('NCH155','17/09/2013','18/09/2013','new rear inner tube');
insert into bikeservice values ('NCH228','17/09/2013','18/09/2013','front brakes changed');
insert into bikeservice values ('NCH101','18/09/2013','19/09/2013','front brakes changed');
insert into bikeservice values ('NCH153','19/09/2013','20/09/2013','front brakes changed');
insert into bikeservice values ('NCH112','20/09/2013','21/09/2013','new front inner tube');
insert into bikeservice values ('NCH186','22/09/2013','23/09/2013','new front inner tube');
insert into bikeservice values ('NCH217','22/09/2013','23/09/2013','new handgrips');
insert into bikeservice values ('NCH108','23/09/2013','24/09/2013','rear brakes changed');
insert into bikeservice values ('NCH222','24/09/2013','25/09/2013','rear brakes changed');
insert into bikeservice values ('NCH155','25/09/2013','26/09/2013','new rear inner tube');
insert into bikeservice values ('NCH205','26/09/2013','27/09/2013','rear brakes changed');
insert into bikeservice values ('NCH256','27/09/2013','28/09/2013','rear brakes changed');
insert into bikeservice values ('NCH107','29/09/2013','30/09/2013','front brakes changed');
insert into bikeservice values ('NCH212','29/09/2013','30/09/2013','rear brakes changed');
insert into bikeservice values ('NCH206','30/09/2013','01/10/2013','rear brakes changed');
insert into bikeservice values ('NCH281','01/10/2013','02/10/2013','front brakes changed');
insert into bikeservice values ('NCH292','01/10/2013','02/10/2013','new front inner tube');
insert into bikeservice values ('NCH193','02/10/2013','03/10/2013','new handgrips');
insert into bikeservice values ('NCH154','04/10/2013','05/10/2013','rear brakes changed');
insert into bikeservice values ('NCH117','06/10/2013','07/10/2013','new rear inner tube');
insert into bikeservice values ('NCH179','08/10/2013','09/10/2013','new handgrips');
insert into bikeservice values ('NCH112','09/10/2013','10/10/2013','new rear inner tube');
insert into bikeservice values ('NCH234','10/10/2013','11/10/2013','new rear inner tube');
insert into bikeservice values ('NCH296','10/10/2013','11/10/2013','replaced saddle');
insert into bikeservice values ('NCH107','11/10/2013','12/10/2013','new rear inner tube');
insert into bikeservice values ('NCH126','11/10/2013','12/10/2013','new front inner tube');
insert into bikeservice values ('NCH195','12/10/2013','13/10/2013','front brakes changed');
insert into bikeservice values ('NCH193','13/10/2013','14/10/2013','front brakes changed');
insert into bikeservice values ('NCH179','15/10/2013','16/10/2013','front brakes changed');
insert into bikeservice values ('NCH164','15/10/2013','16/10/2013','rear brakes changed');
insert into bikeservice values ('NCH178','17/10/2013','18/10/2013','front brakes changed');
insert into bikeservice values ('NCH128','20/10/2013','21/10/2013','rear brakes changed');
insert into bikeservice values ('NCH186','20/10/2013','21/10/2013','front brakes changed');
insert into bikeservice values ('NCH274','20/10/2013','21/10/2013','replaced saddle');
insert into bikeservice values ('NCH294','22/10/2013','23/10/2013','new rear inner tube');
insert into bikeservice values ('NCH110','22/10/2013','23/10/2013','new front inner tube');
insert into bikeservice values ('NCH180','23/10/2013','24/10/2013','new front inner tube');
insert into bikeservice values ('NCH162','24/10/2013','25/10/2013','new rear inner tube');
insert into bikeservice values ('NCH115','25/10/2013','26/10/2013','new rear inner tube');
insert into bikeservice values ('NCH181','26/10/2013','27/10/2013','new rear inner tube');
insert into bikeservice values ('NCH162','27/10/2013','28/10/2013','new front inner tube');
insert into bikeservice values ('NCH107','27/10/2013','28/10/2013','front brakes changed');
insert into bikeservice values ('NCH151','28/10/2013','29/10/2013','new front inner tube');
insert into bikeservice values ('NCH208','28/10/2013','29/10/2013','front brakes changed');
insert into bikeservice values ('NCH180','29/10/2013','30/10/2013','new front inner tube');
insert into bikeservice values ('NCH245','30/10/2013','01/11/2013','rear brakes changed');

/**
* My modifications to the data
*/

INSERT INTO customer
(customer_name, customer_email, billing_address, card_type, card_exp, cardno, staff)
VALUES('Daniel Pawsey', 'D.Pawsey@uea.ac.uk', 'V', '11/16', '1234567890');

UPDATE customer
SET staff = 'CUSTOMER';

INSERT INTO customer
(customer_name, customer_email, staff)
VALUES('John Doe', 'manager@abh.co.uk', 'MANAGER');

INSERT INTO customer
(customer_name, customer_email, staff)
VALUES('Kelly Jackson', 'technician@abh.co.uk', 'TECHNICIAN');

UPDATE customer
SET password = 'password';